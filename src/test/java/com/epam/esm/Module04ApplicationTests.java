package com.epam.esm;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("dev")
@SpringBootTest
class Module04ApplicationTests {

    @Test
    void contextLoads() {
    }

}
