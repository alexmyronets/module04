package com.epam.esm.data.impl;


import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.DATE_ASC_ORDER_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NAME_DESC_ORDER_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ALL_PARAMETERS_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_LAST_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_PREV_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.SORT_BY_DATE_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.SORT_BY_NAME_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_AND_TEXT_EMPTY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_AND_TEXT_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TEXT_ONLY_SEARCH_DTO;
import static com.epam.esm.DateConstants.ISO_8601_DATE_PATTERN;
import static com.epam.esm.DateConstants.ISO_8601_DATE_REGEX;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERTIFICATE_ENTITY_PATCHED;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_10_12_17_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_14_16_17_20_24_46_47_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_1_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_1_TO_5_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_3_13_23_30_TO_39_43_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_41_TO_45_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_46_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_6_TO_10_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_1;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_16;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_LIST_DATE_ASC_ORDER;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_LIST_NAME_DESC_ORDER;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.SAVED_GIFT_CERTIFICATE_ENTITY;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.getGiftCertificateEntityPatch;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.getGiftCertificateEntityToSave;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.epam.esm.data.GiftCertificateRepository;
import com.epam.esm.data.TagRepository;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.dto.GiftCertificateFilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchGiftCertificateException;
import jakarta.persistence.EntityManager;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@ActiveProfiles("dev")
@SpringBootTest
@Transactional
class GiftCertificateRepositoryImplTest {


    @Qualifier("giftCertificateRepositoryImpl")
    @Autowired
    GiftCertificateRepository giftCertificateRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    @DirtiesContext
    void create() {
        GiftCertificate actual = giftCertificateRepository.create(getGiftCertificateEntityToSave());
        assertAll(() -> assertEquals(SAVED_GIFT_CERTIFICATE_ENTITY.getId(), actual.getId()),
                  () -> assertEquals(SAVED_GIFT_CERTIFICATE_ENTITY.getName(), actual.getName()),
                  () -> assertEquals(SAVED_GIFT_CERTIFICATE_ENTITY.getDescription(), actual.getDescription()),
                  () -> assertEquals(SAVED_GIFT_CERTIFICATE_ENTITY.getPrice(), actual.getPrice()),
                  () -> assertEquals(SAVED_GIFT_CERTIFICATE_ENTITY.getDuration(), actual.getDuration()),
                  () -> assertThat(actual.getCreateDate()
                                         .format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertThat(actual.getLastUpdateDate()
                                         .format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertEquals(SAVED_GIFT_CERTIFICATE_ENTITY.getTags(), actual.getTags()));
    }

    @Test
    void readExistingGiftCertificate() {
        assertEquals(GIFT_CERT_ENTITY_1, giftCertificateRepository.read(1));
    }

    @Test
    void readNonExistingGiftCertificate() {
        assertThrows(NoSuchGiftCertificateException.class, () -> giftCertificateRepository.read(442));
    }

    @Test
    @DirtiesContext
    void updateExistingGiftCertificate() {
        GiftCertificate actual = giftCertificateRepository.update(getGiftCertificateEntityPatch(), 1);
        assertAll(() -> assertEquals(GIFT_CERTIFICATE_ENTITY_PATCHED.getId(), actual.getId()),
                  () -> assertEquals(GIFT_CERTIFICATE_ENTITY_PATCHED.getName(), actual.getName()),
                  () -> assertEquals(GIFT_CERTIFICATE_ENTITY_PATCHED.getDescription(), actual.getDescription()),
                  () -> assertEquals(GIFT_CERTIFICATE_ENTITY_PATCHED.getPrice(), actual.getPrice()),
                  () -> assertEquals(GIFT_CERTIFICATE_ENTITY_PATCHED.getDuration(), actual.getDuration()),
                  () -> assertEquals(GIFT_CERTIFICATE_ENTITY_PATCHED.getCreateDate(), actual.getCreateDate()),
                  () -> assertThat(actual.getLastUpdateDate()
                                         .format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertEquals(GIFT_CERTIFICATE_ENTITY_PATCHED.getTags(), actual.getTags()));
    }

    @Test
    void updateNonExistingGiftCertificate() {
        GiftCertificate patch = getGiftCertificateEntityPatch();

        assertThrows(NoSuchGiftCertificateException.class, () -> giftCertificateRepository.update(patch, 442));
    }

    @Test
    @DirtiesContext
    void deleteExistingGiftCertificate() {
        giftCertificateRepository.delete(25);
        assertNull(entityManager.find(GiftCertificate.class, 25));
    }

    @Test
    void deleteNonExistingGiftCertificate() {
        assertThrows(NoSuchGiftCertificateException.class, () -> giftCertificateRepository.delete(442));
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndResult")
    void list(GiftCertificateFilterParams giftCertificateFilterParams, List<GiftCertificate> giftCertificateList) {
        assertEquals(giftCertificateList, giftCertificateRepository.list(giftCertificateFilterParams));
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndPaging")
    void getNextAfterId(GiftCertificateFilterParams giftCertificateFilterParams,
                        List<GiftCertificate> giftCertificateList, PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getNextAfterId(),
                     giftCertificateRepository.getNextAfterId(giftCertificateFilterParams,
                                                              giftCertificateList.get(giftCertificateList.size() - 1)
                                                                                 .getId()));
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndPaging")
    void getPrevAfterId(GiftCertificateFilterParams giftCertificateFilterParams,
                        List<GiftCertificate> giftCertificateList, PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getPrevAfterId(),
                     giftCertificateRepository.getPrevAfterId(giftCertificateFilterParams,
                                                              giftCertificateList.get(0).getId()));
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndPaging")
    void getLastAfterId(GiftCertificateFilterParams giftCertificateFilterParams,
                        List<GiftCertificate> giftCertificateList, PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getLastAfterId(),
                     giftCertificateRepository.getLastAfterId(giftCertificateFilterParams));
    }

    private static Stream<Arguments> provideGiftCertificateSearchParamsAndResult() {
        return Stream.of(Arguments.of(ALL_PARAMETERS_SEARCH_DTO, GIFT_CERT_10_12_17_ENTITY_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO, GIFT_CERT_6_TO_10_ENTITY_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO, GIFT_CERT_1_TO_5_ENTITY_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_LAST_PAGE_DTO, GIFT_CERT_46_TO_50_ENTITY_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_PREV_PAGE_DTO, GIFT_CERT_41_TO_45_ENTITY_LIST),
                         Arguments.of(TAG_ONLY_SEARCH_DTO, GIFT_CERT_14_16_17_20_24_46_47_ENTITY_LIST),
                         Arguments.of(TEXT_ONLY_SEARCH_DTO, GIFT_CERT_3_13_23_30_TO_39_43_ENTITY_LIST),
                         Arguments.of(SORT_BY_NAME_ONLY_SEARCH_DTO, GIFT_CERT_ENTITY_LIST_NAME_DESC_ORDER),
                         Arguments.of(SORT_BY_DATE_ONLY_SEARCH_DTO, GIFT_CERT_ENTITY_LIST_DATE_ASC_ORDER),
                         Arguments.of(TAG_AND_TEXT_SEARCH_DTO, List.of(GIFT_CERT_ENTITY_16)),
                         Arguments.of(TAG_AND_TEXT_EMPTY_SEARCH_DTO, List.of()),
                         Arguments.of(EMPTY_SEARCH_DTO, GIFT_CERT_1_TO_50_ENTITY_LIST));
    }

    private static Stream<Arguments> provideGiftCertificateSearchParamsAndPaging() {
        return Stream.of(Arguments.of(ALL_PARAMETERS_SEARCH_DTO, GIFT_CERT_10_12_17_ENTITY_LIST, EMPTY_PAGING),
                         Arguments.of(NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO, GIFT_CERT_6_TO_10_ENTITY_LIST, NEXT_PAGING),
                         Arguments.of(NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO, GIFT_CERT_1_TO_5_ENTITY_LIST, FIRST_PAGING),
                         Arguments.of(NO_PARAMETERS_SEARCH_LAST_PAGE_DTO, GIFT_CERT_46_TO_50_ENTITY_LIST, LAST_PAGING),
                         Arguments.of(NO_PARAMETERS_SEARCH_PREV_PAGE_DTO, GIFT_CERT_41_TO_45_ENTITY_LIST, PREV_PAGING),
                         Arguments.of(TAG_ONLY_SEARCH_DTO, GIFT_CERT_14_16_17_20_24_46_47_ENTITY_LIST, EMPTY_PAGING),
                         Arguments.of(TEXT_ONLY_SEARCH_DTO, GIFT_CERT_3_13_23_30_TO_39_43_ENTITY_LIST, EMPTY_PAGING),
                         Arguments.of(SORT_BY_NAME_ONLY_SEARCH_DTO,
                                      GIFT_CERT_ENTITY_LIST_NAME_DESC_ORDER,
                                      NAME_DESC_ORDER_PAGING),
                         Arguments.of(SORT_BY_DATE_ONLY_SEARCH_DTO,
                                      GIFT_CERT_ENTITY_LIST_DATE_ASC_ORDER,
                                      DATE_ASC_ORDER_PAGING),
                         Arguments.of(TAG_AND_TEXT_SEARCH_DTO, List.of(GIFT_CERT_ENTITY_16), EMPTY_PAGING),
                         Arguments.of(EMPTY_SEARCH_DTO, GIFT_CERT_1_TO_50_ENTITY_LIST, EMPTY_PAGING));
    }
}

