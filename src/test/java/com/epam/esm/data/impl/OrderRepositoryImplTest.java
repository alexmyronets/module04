package com.epam.esm.data.impl;

import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.DateConstants.ISO_8601_DATE_PATTERN;
import static com.epam.esm.DateConstants.ISO_8601_DATE_REGEX;
import static com.epam.esm.EntityConstants.OrderEntityConstants.CREATED_ORDER_ENTITY;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_93_TO_103_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_93_TO_97_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_94_TO_98_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_98_TO_102_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_99_TO_103_ENTITY_LIST;
import static com.epam.esm.EntityConstants.OrderEntityConstants.ORDER_ENTITY_1;
import static com.epam.esm.EntityConstants.OrderEntityConstants.getOrderEntityToCreate;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.epam.esm.data.OrderRepository;
import com.epam.esm.domain.Order;
import com.epam.esm.dto.OrderFilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchOrderException;
import jakarta.persistence.EntityManager;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@ActiveProfiles("dev")
@SpringBootTest
@Transactional
class OrderRepositoryImplTest {

    @Autowired
    OrderRepository orderRepository;

    @Test
    @DirtiesContext
    @WithMockUser(roles = "ADMIN")
    void createByAdmin() {
        Order actual = orderRepository.create(getOrderEntityToCreate());
        assertAll(() -> assertEquals(CREATED_ORDER_ENTITY.getId(), actual.getId()),
                  () -> assertEquals(CREATED_ORDER_ENTITY.getUser(), actual.getUser()),
                  () -> assertEquals(CREATED_ORDER_ENTITY.getGiftCertificate(), actual.getGiftCertificate()),
                  () -> assertThat(actual.getOrderDate().format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertEquals(CREATED_ORDER_ENTITY.getOrderCost(), actual.getOrderCost()));

    }

    @Test
    @DirtiesContext
    @WithUserDetails("user1")
    void createByOwnerUser() {
        Order actual = orderRepository.create(getOrderEntityToCreate());
        assertAll(() -> assertEquals(CREATED_ORDER_ENTITY.getId(), actual.getId()),
                  () -> assertEquals(CREATED_ORDER_ENTITY.getUser(), actual.getUser()),
                  () -> assertEquals(CREATED_ORDER_ENTITY.getGiftCertificate(), actual.getGiftCertificate()),
                  () -> assertThat(actual.getOrderDate().format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertEquals(CREATED_ORDER_ENTITY.getOrderCost(), actual.getOrderCost()));

    }

    @Test
    @WithUserDetails("user2")
    void createByOtherUser() {
        var order = getOrderEntityToCreate();
        assertThrows(AccessDeniedException.class, () ->  orderRepository.create(order));
    }

    @Test
    void createNoAuth() {
        var order = getOrderEntityToCreate();
        assertThrows(AuthenticationException.class, () ->  orderRepository.create(order));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void readExistingOrderByAdmin() {
        assertEquals(ORDER_ENTITY_1, orderRepository.read(1));
    }

    @Test
    @WithUserDetails("user1")
    void readExistingOrderByOwner() {
        assertEquals(ORDER_ENTITY_1, orderRepository.read(1));
    }

    @Test
    @WithUserDetails("user2")
    void readExistingOrderByOtherUser() {
        assertThrows(AccessDeniedException.class, () ->  orderRepository.read(1));
    }

    @Test
    void readExistingOrderNoAuth() {
        assertThrows(AuthenticationException.class, () ->  orderRepository.read(1));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void readNonExistingOrder() {
        assertThrows(NoSuchOrderException.class, () -> orderRepository.read(442));
    }

    @ParameterizedTest
    @MethodSource("provideOrderPageParamsAndResult")
    @WithMockUser(roles = "ADMIN")
    void listByAdmin(OrderFilterParams orderFilterParams, List<Order> orderEntityList) {
        assertEquals(orderEntityList, orderRepository.list(orderFilterParams));
    }

    @ParameterizedTest
    @MethodSource("provideOrderPageParamsAndResult")
    @WithUserDetails("user17")
    void listByOwner(OrderFilterParams orderFilterParams, List<Order> orderEntityList) {
        assertEquals(orderEntityList, orderRepository.list(orderFilterParams));
    }

    @Test
    @WithUserDetails("user1")
    void listByOtherUser() {
        assertThrows(AccessDeniedException.class, () -> orderRepository.list(ORDER_FIRST_PAGE_FILTER_PARAMS));
    }

    @Test
    void listNoAuth() {
        assertThrows(AuthenticationException.class, () -> orderRepository.list(ORDER_FIRST_PAGE_FILTER_PARAMS));
    }

    @ParameterizedTest
    @MethodSource("provideOrderPageParamsAndResultAndPaging")
    @WithMockUser(roles = "ADMIN")
    void getNextAfterId(OrderFilterParams orderFilterParams, List<Order> orderEntityList,
                        PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getNextAfterId(),
                     orderRepository.getNextAfterId(orderFilterParams,
                                                    orderEntityList.get(orderEntityList.size() - 1).getId()));
    }

    @ParameterizedTest
    @MethodSource("provideOrderPageParamsAndResultAndPaging")
    @WithMockUser(roles = "ADMIN")
    void getPrevAfterId(OrderFilterParams orderFilterParams, List<Order> orderEntityList,
                        PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getPrevAfterId(),
                     orderRepository.getPrevAfterId(orderFilterParams,
                                                    orderEntityList.get(0).getId()));
    }

    @ParameterizedTest
    @MethodSource("provideOrderPageParamsAndResultAndPaging")
    @WithMockUser(roles = "ADMIN")
    void getLastAfterId(OrderFilterParams orderFilterParams, List<Order> orderEntityList,
                        PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getLastAfterId(),
                     orderRepository.getLastAfterId(orderFilterParams));
    }

    private static Stream<Arguments> provideOrderPageParamsAndResult() {
        return Stream.of(Arguments.of(ORDER_NEXT_PAGE_FILTER_PARAMS, ORDER_98_TO_102_ENTITY_LIST),
                         Arguments.of(ORDER_FIRST_PAGE_FILTER_PARAMS, ORDER_93_TO_97_ENTITY_LIST),
                         Arguments.of(ORDER_LAST_PAGE_FILTER_PARAMS, ORDER_99_TO_103_ENTITY_LIST),
                         Arguments.of(ORDER_PREV_PAGE_FILTER_PARAMS, ORDER_94_TO_98_ENTITY_LIST),
                         Arguments.of(ORDER_EMPTY_PAGE_FILTER_PARAMS, ORDER_93_TO_103_ENTITY_LIST));
    }

    private static Stream<Arguments> provideOrderPageParamsAndResultAndPaging() {
        return Stream.of(Arguments.of(ORDER_NEXT_PAGE_FILTER_PARAMS,
                                      ORDER_98_TO_102_ENTITY_LIST,
                                      ORDER_NEXT_PAGING),
                         Arguments.of(ORDER_FIRST_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_97_ENTITY_LIST,
                                      ORDER_FIRST_PAGING),
                         Arguments.of(ORDER_LAST_PAGE_FILTER_PARAMS,
                                      ORDER_99_TO_103_ENTITY_LIST,
                                      ORDER_LAST_PAGING),
                         Arguments.of(ORDER_PREV_PAGE_FILTER_PARAMS,
                                      ORDER_94_TO_98_ENTITY_LIST,
                                      ORDER_PREV_PAGING),
                         Arguments.of(ORDER_EMPTY_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_103_ENTITY_LIST,
                                      EMPTY_PAGING));
    }
}