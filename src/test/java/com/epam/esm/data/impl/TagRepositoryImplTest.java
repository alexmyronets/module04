package com.epam.esm.data.impl;

import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.EntityConstants.TagEntityConstants.SAVED_TAG;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_1_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_1_TO_5_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_41_TO_55_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_46_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_6_TO_10_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_ENTITY_1;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_ENTITY_EXISTS_TO_CREATE;
import static com.epam.esm.EntityConstants.TagEntityConstants.getTagEntityToSave;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.epam.esm.data.TagRepository;
import com.epam.esm.domain.Tag;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchTagException;
import com.epam.esm.exception.TagAlreadyExistsException;
import com.epam.esm.exception.TagIsInUseException;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@ActiveProfiles("dev")
@SpringBootTest
class TagRepositoryImplTest {


    @Qualifier("tagRepositoryImpl")
    @Autowired
    TagRepository tagRepository;
    @Autowired
    EntityManager entityManager;

    @Test
    @DirtiesContext
    void createTag() {
        assertEquals(SAVED_TAG, tagRepository.create(getTagEntityToSave()));
    }

    @Test
    void createExistingTag() {
        assertThrows(TagAlreadyExistsException.class, () -> tagRepository.create(TAG_ENTITY_EXISTS_TO_CREATE));
    }

    @Test
    void readExistingTag() {
        assertEquals(TAG_ENTITY_1, tagRepository.read(1));
    }

    @Test
    void readNonExistingTag() {
        assertThrows(NoSuchTagException.class, () -> tagRepository.read(442));
    }

    @Test
    @DirtiesContext
    void deleteTag() {
        tagRepository.delete(9);
        assertNull(entityManager.find(Tag.class, 9));
    }

    @Test
    void deleteUsedTag() {
        assertThrows(TagIsInUseException.class, () -> tagRepository.delete(1));
    }

    @Test
    void deleteNonExistingTag() {
        assertThrows(NoSuchTagException.class, () -> tagRepository.delete(442));
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResult")
    void list(FilterParams filterParams, List<Tag> tagList) {
        assertEquals(tagList, tagRepository.list(filterParams));
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResultAndPaging")
    void getNextAfterId(FilterParams filterParams, List<Tag> tagList, PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getNextAfterId(),
                     tagRepository.getNextAfterId(filterParams, tagList.get(tagList.size() - 1).getId()));
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResultAndPaging")
    void getPrevAfterId(FilterParams filterParams, List<Tag> tagList, PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getPrevAfterId(),
                     tagRepository.getPrevAfterId(filterParams, tagList.get(0).getId()));
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResultAndPaging")
    void getLastAfterId(FilterParams filterParams, List<Tag> tagList, PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO.getLastAfterId(),
                     tagRepository.getLastAfterId(filterParams));
    }

    private static Stream<Arguments> provideTagPageParamsAndResult() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS, TAG_6_TO_10_ENTITY_LIST),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS, TAG_1_TO_5_ENTITY_LIST),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS, TAG_46_TO_50_ENTITY_LIST),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS, TAG_41_TO_55_ENTITY_LIST),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS, TAG_1_TO_50_ENTITY_LIST));
    }

    private static Stream<Arguments> provideTagPageParamsAndResultAndPaging() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS, TAG_6_TO_10_ENTITY_LIST, NEXT_PAGING),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS, TAG_1_TO_5_ENTITY_LIST, FIRST_PAGING),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS, TAG_46_TO_50_ENTITY_LIST, LAST_PAGING),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS, TAG_41_TO_55_ENTITY_LIST, PREV_PAGING),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS, TAG_1_TO_50_ENTITY_LIST, EMPTY_PAGING));
    }
}