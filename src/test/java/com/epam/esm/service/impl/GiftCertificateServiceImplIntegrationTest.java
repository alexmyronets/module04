package com.epam.esm.service.impl;

import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_10_12_17_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_14_16_17_20_24_46_47_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_1_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_1_TO_5_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_3_13_23_30_TO_39_43_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_41_TO_45_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_46_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_6_TO_10_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_1;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_16;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_LIST_DATE_ASC_ORDER;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_LIST_NAME_DESC_ORDER;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_PATCHED;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.SAVED_GIFT_CERT_DTO;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.getGiftCertificateDTOToSave;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.getGiftCertificateDtoPatch;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.DATE_ASC_ORDER_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NAME_DESC_ORDER_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ALL_PARAMETERS_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_LAST_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_PREV_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.SORT_BY_DATE_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.SORT_BY_NAME_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_AND_TEXT_EMPTY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_AND_TEXT_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TEXT_ONLY_SEARCH_DTO;
import static com.epam.esm.DateConstants.ISO_8601_DATE_PATTERN;
import static com.epam.esm.DateConstants.ISO_8601_DATE_REGEX;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.epam.esm.data.GiftCertificateRepository;
import com.epam.esm.data.TagJPARepository;
import com.epam.esm.data.TagRepository;
import com.epam.esm.data.impl.GiftCertificateRepositoryImpl;
import com.epam.esm.data.impl.TagRepositoryImpl;
import com.epam.esm.dto.GiftCertificateDTO;
import com.epam.esm.dto.GiftCertificateFilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchGiftCertificateException;
import com.epam.esm.service.GiftCertificateService;
import jakarta.persistence.EntityManager;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;

@DataJpaTest
class GiftCertificateServiceImplIntegrationTest {


    @Autowired
    private GiftCertificateService giftCertificateService;

    @Test
    @DirtiesContext
    void createGiftCertificate() {
        GiftCertificateDTO actual = giftCertificateService.create(getGiftCertificateDTOToSave());
        assertAll(() -> assertEquals(SAVED_GIFT_CERT_DTO.getId(), actual.getId()),
                  () -> assertEquals(SAVED_GIFT_CERT_DTO.getName(), actual.getName()),
                  () -> assertEquals(SAVED_GIFT_CERT_DTO.getDescription(), actual.getDescription()),
                  () -> assertEquals(SAVED_GIFT_CERT_DTO.getPrice(), actual.getPrice()),
                  () -> assertEquals(SAVED_GIFT_CERT_DTO.getDuration(), actual.getDuration()),
                  () -> assertThat(actual.getCreateDate().format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertThat(actual.getLastUpdateDate()
                                         .format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertEquals(SAVED_GIFT_CERT_DTO.getTags(), actual.getTags()));
    }

    @Test
    void readGiftCertificate() {
        assertEquals(GIFT_CERT_DTO_1, giftCertificateService.read(1));
    }

    @Test
    @DirtiesContext
    void updateGiftCertificate() {
        GiftCertificateDTO actual = giftCertificateService.update(getGiftCertificateDtoPatch(), 1);
        assertAll(() -> assertEquals(GIFT_CERT_DTO_PATCHED.getId(), actual.getId()),
                  () -> assertEquals(GIFT_CERT_DTO_PATCHED.getName(), actual.getName()),
                  () -> assertEquals(GIFT_CERT_DTO_PATCHED.getDescription(), actual.getDescription()),
                  () -> assertEquals(GIFT_CERT_DTO_PATCHED.getPrice(), actual.getPrice()),
                  () -> assertEquals(GIFT_CERT_DTO_PATCHED.getDuration(), actual.getDuration()),
                  () -> assertThat(actual.getCreateDate().format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertThat(actual.getLastUpdateDate()
                                         .format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertEquals(GIFT_CERT_DTO_PATCHED.getTags(), actual.getTags()));
    }

    @Test
    @DirtiesContext
    void deleteGiftCertificate() {
        giftCertificateService.delete(25);
        assertThrows(NoSuchGiftCertificateException.class, () -> giftCertificateService.read(25));
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndResult")
    void getAllGiftCertificates(GiftCertificateFilterParams giftCertificateFilterParams,
                                List<GiftCertificateDTO> giftCertificateDTOS) {
        assertEquals(giftCertificateDTOS, giftCertificateService.list(giftCertificateFilterParams));
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndPagingAndResultList")
    void getPagingParams(GiftCertificateFilterParams giftCertificateFilterParams,
                         List<GiftCertificateDTO> giftCertificateDTOList, PagingParamsDTO pagingParamsDTO) {

        assertEquals(pagingParamsDTO,
                     giftCertificateService.getPagingParams(giftCertificateFilterParams,
                                                            giftCertificateDTOList.get(
                                                                                      giftCertificateDTOList.size() - 1)
                                                                                  .getId(),
                                                            giftCertificateDTOList.get(0).getId()));
    }

    private static Stream<Arguments> provideGiftCertificateSearchParamsAndResult() {
        return Stream.of(Arguments.of(ALL_PARAMETERS_SEARCH_DTO,
                                      GIFT_CERT_10_12_17_DTO_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO,
                                      GIFT_CERT_6_TO_10_DTO_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO,
                                      GIFT_CERT_1_TO_5_DTO_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_LAST_PAGE_DTO,
                                      GIFT_CERT_46_TO_50_DTO_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_PREV_PAGE_DTO,
                                      GIFT_CERT_41_TO_45_DTO_LIST),
                         Arguments.of(TAG_ONLY_SEARCH_DTO,
                                      GIFT_CERT_14_16_17_20_24_46_47_DTO_LIST),
                         Arguments.of(TEXT_ONLY_SEARCH_DTO,
                                      GIFT_CERT_3_13_23_30_TO_39_43_DTO_LIST),
                         Arguments.of(SORT_BY_NAME_ONLY_SEARCH_DTO,
                                      GIFT_CERT_DTO_LIST_NAME_DESC_ORDER),
                         Arguments.of(SORT_BY_DATE_ONLY_SEARCH_DTO,
                                      GIFT_CERT_DTO_LIST_DATE_ASC_ORDER),
                         Arguments.of(TAG_AND_TEXT_SEARCH_DTO,
                                      List.of(GIFT_CERT_DTO_16)),
                         Arguments.of(TAG_AND_TEXT_EMPTY_SEARCH_DTO,
                                      List.of()),
                         Arguments.of(EMPTY_SEARCH_DTO,
                                      GIFT_CERT_1_TO_50_DTO_LIST));
    }

    private static Stream<Arguments> provideGiftCertificateSearchParamsAndPagingAndResultList() {
        return Stream.of(Arguments.of(ALL_PARAMETERS_SEARCH_DTO,
                                      GIFT_CERT_10_12_17_DTO_LIST,
                                      EMPTY_PAGING),
                         Arguments.of(NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO,
                                      GIFT_CERT_6_TO_10_DTO_LIST,
                                      NEXT_PAGING),
                         Arguments.of(NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO,
                                      GIFT_CERT_1_TO_5_DTO_LIST,
                                      FIRST_PAGING),
                         Arguments.of(NO_PARAMETERS_SEARCH_LAST_PAGE_DTO,
                                      GIFT_CERT_46_TO_50_DTO_LIST,
                                      LAST_PAGING),
                         Arguments.of(NO_PARAMETERS_SEARCH_PREV_PAGE_DTO,
                                      GIFT_CERT_41_TO_45_DTO_LIST,
                                      PREV_PAGING),
                         Arguments.of(TAG_ONLY_SEARCH_DTO,
                                      GIFT_CERT_14_16_17_20_24_46_47_DTO_LIST,
                                      EMPTY_PAGING),
                         Arguments.of(TEXT_ONLY_SEARCH_DTO,
                                      GIFT_CERT_3_13_23_30_TO_39_43_DTO_LIST,
                                      EMPTY_PAGING),
                         Arguments.of(SORT_BY_NAME_ONLY_SEARCH_DTO,
                                      GIFT_CERT_DTO_LIST_NAME_DESC_ORDER,
                                      NAME_DESC_ORDER_PAGING),
                         Arguments.of(SORT_BY_DATE_ONLY_SEARCH_DTO,
                                      GIFT_CERT_DTO_LIST_DATE_ASC_ORDER,
                                      DATE_ASC_ORDER_PAGING),
                         Arguments.of(TAG_AND_TEXT_SEARCH_DTO,
                                      List.of(GIFT_CERT_DTO_16),
                                      EMPTY_PAGING),
                         Arguments.of(EMPTY_SEARCH_DTO,
                                      GIFT_CERT_1_TO_50_DTO_LIST,
                                      EMPTY_PAGING));
    }

    @TestConfiguration
    static class GiftCertificateServiceTestContextConfiguration {

        @Bean
        public GiftCertificateService giftCertificateService(GiftCertificateRepository giftCertificateRepository,
                                                             ModelMapper modelMapper) {
            return new GiftCertificateServiceImpl(giftCertificateRepository, modelMapper);
        }

        @Bean
        public GiftCertificateRepository giftCertRepo(TagRepository tagRepository, EntityManager entityManager) {
            return new GiftCertificateRepositoryImpl(tagRepository, entityManager);
        }

        @Bean
        public TagRepository tagRepo(EntityManager entityManager, TagJPARepository tagJPARepository) {
            return new TagRepositoryImpl(entityManager, tagJPARepository);
        }

    }


}