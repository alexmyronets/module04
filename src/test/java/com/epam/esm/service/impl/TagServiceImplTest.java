package com.epam.esm.service.impl;

import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.TagDTOConstants.SAVED_TAG_DTO;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_1_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_1_TO_5_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_41_TO_55_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_46_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_6_TO_10_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_1;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_3;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_TO_SAVE;
import static com.epam.esm.EntityConstants.TagEntityConstants.SAVED_TAG;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_1_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_1_TO_5_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_41_TO_55_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_46_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_6_TO_10_ENTITY_LIST;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_ENTITY_1;
import static com.epam.esm.EntityConstants.TagEntityConstants.TAG_ENTITY_3;
import static com.epam.esm.EntityConstants.TagEntityConstants.getTagEntityToSave;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.epam.esm.data.TagRepository;
import com.epam.esm.domain.Tag;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.TagDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.service.TagService;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class TagServiceImplTest {

    @Autowired
    private TagService tagService;
    @MockBean
    private TagRepository tagRepository;

    @Test
    void createTag() {
        given(tagRepository.create(getTagEntityToSave())).willReturn(SAVED_TAG);
        assertEquals(SAVED_TAG_DTO, tagService.create(TAG_DTO_TO_SAVE));
    }

    @Test
    void readTag() {
        given(tagRepository.read(1)).willReturn(TAG_ENTITY_1);
        assertEquals(TAG_DTO_1, tagService.read(1));
    }

    @Test
    void deleteTag() {
        tagService.delete(3);
        verify(tagRepository, times(1)).delete(3);
    }

    @Test
    void getMostUsedTagByMostSpentUserOrders() {
        given(tagRepository.readMostUsedTagByMostSpendUser()).willReturn(TAG_ENTITY_3);
        assertEquals(TAG_DTO_3, tagService.readMostUsedTagByMostSpendUser());
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResult")
    void getAllTags(FilterParams filterParams, List<TagDTO> tagDTOList, List<Tag> tagList) {
        given(tagRepository.list(filterParams)).willReturn(tagList);
        assertEquals(tagDTOList, tagService.list(filterParams));
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResultAndPaging")
    void getPagingParams(FilterParams filterParams, List<TagDTO> tagDTOList, PagingParamsDTO pagingParamsDTO) {
        given(tagRepository.getNextAfterId(filterParams, tagDTOList.get(tagDTOList.size() - 1).getId())).willReturn(
            pagingParamsDTO.getNextAfterId());
        given(tagRepository.getPrevAfterId(filterParams, tagDTOList.get(0).getId())).willReturn(
            pagingParamsDTO.getPrevAfterId());
        given(tagRepository.getLastAfterId(filterParams)).willReturn(
            pagingParamsDTO.getLastAfterId());

        assertEquals(pagingParamsDTO,
                     tagService.getPagingParams(filterParams,
                                                tagDTOList.get(tagDTOList.size() - 1).getId(),
                                                tagDTOList.get(0).getId()));
    }

    private static Stream<Arguments> provideTagPageParamsAndResult() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS,
                                      TAG_6_TO_10_DTO_LIST, TAG_6_TO_10_ENTITY_LIST),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_5_DTO_LIST, TAG_1_TO_5_ENTITY_LIST),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS,
                                      TAG_46_TO_50_DTO_LIST, TAG_46_TO_50_ENTITY_LIST),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS,
                                      TAG_41_TO_55_DTO_LIST, TAG_41_TO_55_ENTITY_LIST),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_50_DTO_LIST, TAG_1_TO_50_ENTITY_LIST));
    }

    private static Stream<Arguments> provideTagPageParamsAndResultAndPaging() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS,
                                      TAG_6_TO_10_DTO_LIST,
                                      NEXT_PAGING),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_5_DTO_LIST,
                                      FIRST_PAGING),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS,
                                      TAG_46_TO_50_DTO_LIST,
                                      LAST_PAGING),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS,
                                      TAG_41_TO_55_DTO_LIST,
                                      PREV_PAGING),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_50_DTO_LIST,
                                      EMPTY_PAGING));
    }

    @TestConfiguration
    static class TagServiceTestContextConfiguration {

        @Bean
        public ModelMapper modelMapper() {
            return new ModelMapper();
        }

        @Bean
        TagService tagService(TagRepository tagRepository, ModelMapper modelMapper) {
            return new TagServiceImpl(modelMapper, tagRepository);
        }

    }
}