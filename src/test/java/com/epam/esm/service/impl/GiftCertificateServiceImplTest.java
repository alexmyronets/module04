package com.epam.esm.service.impl;

import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_10_12_17_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_14_16_17_20_24_46_47_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_1_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_1_TO_5_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_3_13_23_30_TO_39_43_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_41_TO_45_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_46_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_6_TO_10_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_1;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_16;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_LIST_DATE_ASC_ORDER;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_LIST_NAME_DESC_ORDER;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_PATCHED;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.SAVED_GIFT_CERT_DTO;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.getGiftCertificateDTOToSave;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.getGiftCertificateDtoPatch;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.DATE_ASC_ORDER_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NAME_DESC_ORDER_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ALL_PARAMETERS_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_LAST_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_PREV_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.SORT_BY_DATE_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.SORT_BY_NAME_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_AND_TEXT_EMPTY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_AND_TEXT_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TEXT_ONLY_SEARCH_DTO;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERTIFICATE_ENTITY_PATCHED;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_10_12_17_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_14_16_17_20_24_46_47_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_1_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_1_TO_5_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_3_13_23_30_TO_39_43_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_41_TO_45_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_46_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_6_TO_10_ENTITY_LIST;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_1;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_16;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_LIST_DATE_ASC_ORDER;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.GIFT_CERT_ENTITY_LIST_NAME_DESC_ORDER;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.SAVED_GIFT_CERTIFICATE_ENTITY;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.getGiftCertificateEntityPatch;
import static com.epam.esm.EntityConstants.GiftCertificateEntityConstants.getGiftCertificateEntityToSave;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.epam.esm.data.GiftCertificateRepository;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.dto.GiftCertificateDTO;
import com.epam.esm.dto.GiftCertificateFilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.service.GiftCertificateService;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class GiftCertificateServiceImplTest {

    @MockBean
    GiftCertificateRepository giftCertificateRepository;

    @Autowired
    private GiftCertificateService giftCertificateService;

    @Test
    void createGiftCertificate() {
        given(giftCertificateRepository.create(getGiftCertificateEntityToSave())).willReturn(
            SAVED_GIFT_CERTIFICATE_ENTITY);

        assertEquals(SAVED_GIFT_CERT_DTO, giftCertificateService.create(getGiftCertificateDTOToSave()));
        verify(giftCertificateRepository, times(1)).create(getGiftCertificateEntityToSave());
    }

    @Test
    void readGiftCertificate() {
        given(giftCertificateRepository.read(1)).willReturn(GIFT_CERT_ENTITY_1);
        assertEquals(GIFT_CERT_DTO_1, giftCertificateService.read(1));
        verify(giftCertificateRepository, times(1)).read(1);
    }

    @Test
    void updateGiftCertificate() {
        given(giftCertificateRepository.update(getGiftCertificateEntityPatch(), 1)).willReturn(
            GIFT_CERTIFICATE_ENTITY_PATCHED);

        assertEquals(GIFT_CERT_DTO_PATCHED,
                     giftCertificateService.update(getGiftCertificateDtoPatch(), 1));
        verify(giftCertificateRepository, times(1)).update(getGiftCertificateEntityPatch(), 1);
    }

    @Test
    void deleteGiftCertificate() {
        giftCertificateService.delete(1);
        verify(giftCertificateRepository, times(1)).delete(1);
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndResult")
    void getAllGiftCertificates(GiftCertificateFilterParams giftCertificateFilterParams,
                                List<GiftCertificateDTO> giftCertificateDTOList,
                                List<GiftCertificate> giftCertificateList) {
        given(giftCertificateRepository.list(giftCertificateFilterParams)).willReturn(giftCertificateList);
        assertEquals(giftCertificateDTOList,
                     giftCertificateService.list(giftCertificateFilterParams));
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndPagingAndResultList")
    void getPagingParams(GiftCertificateFilterParams giftCertificateFilterParams,
                         List<GiftCertificateDTO> giftCertificateDTOList, PagingParamsDTO pagingParamsDTO) {

        given(giftCertificateRepository.getNextAfterId(giftCertificateFilterParams,
                                                       giftCertificateDTOList.get(giftCertificateDTOList.size() - 1)
                                                                             .getId())).willReturn(pagingParamsDTO.getNextAfterId());
        given(giftCertificateRepository.getPrevAfterId(giftCertificateFilterParams,
                                                       giftCertificateDTOList.get(0)
                                                                             .getId())).willReturn(pagingParamsDTO.getPrevAfterId());
        given(giftCertificateRepository.getLastAfterId(giftCertificateFilterParams)).willReturn(pagingParamsDTO.getLastAfterId());

        assertEquals(pagingParamsDTO,
                     giftCertificateService.getPagingParams(giftCertificateFilterParams,
                                                            giftCertificateDTOList.get(
                                                                                      giftCertificateDTOList.size() - 1)
                                                                                  .getId(),
                                                            giftCertificateDTOList.get(0).getId()));
    }

    private static Stream<Arguments> provideGiftCertificateSearchParamsAndResult() {
        return Stream.of(Arguments.of(ALL_PARAMETERS_SEARCH_DTO,
                                      GIFT_CERT_10_12_17_DTO_LIST,
                                      GIFT_CERT_10_12_17_ENTITY_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO,
                                      GIFT_CERT_6_TO_10_DTO_LIST,
                                      GIFT_CERT_6_TO_10_ENTITY_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO,
                                      GIFT_CERT_1_TO_5_DTO_LIST,
                                      GIFT_CERT_1_TO_5_ENTITY_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_LAST_PAGE_DTO,
                                      GIFT_CERT_46_TO_50_DTO_LIST,
                                      GIFT_CERT_46_TO_50_ENTITY_LIST),
                         Arguments.of(NO_PARAMETERS_SEARCH_PREV_PAGE_DTO,
                                      GIFT_CERT_41_TO_45_DTO_LIST,
                                      GIFT_CERT_41_TO_45_ENTITY_LIST),
                         Arguments.of(TAG_ONLY_SEARCH_DTO,
                                      GIFT_CERT_14_16_17_20_24_46_47_DTO_LIST,
                                      GIFT_CERT_14_16_17_20_24_46_47_ENTITY_LIST),
                         Arguments.of(TEXT_ONLY_SEARCH_DTO,
                                      GIFT_CERT_3_13_23_30_TO_39_43_DTO_LIST,
                                      GIFT_CERT_3_13_23_30_TO_39_43_ENTITY_LIST),
                         Arguments.of(SORT_BY_NAME_ONLY_SEARCH_DTO,
                                      GIFT_CERT_DTO_LIST_NAME_DESC_ORDER,
                                      GIFT_CERT_ENTITY_LIST_NAME_DESC_ORDER),
                         Arguments.of(SORT_BY_DATE_ONLY_SEARCH_DTO,
                                      GIFT_CERT_DTO_LIST_DATE_ASC_ORDER,
                                      GIFT_CERT_ENTITY_LIST_DATE_ASC_ORDER),
                         Arguments.of(TAG_AND_TEXT_SEARCH_DTO,
                                      List.of(GIFT_CERT_DTO_16),
                                      List.of(GIFT_CERT_ENTITY_16)),
                         Arguments.of(TAG_AND_TEXT_EMPTY_SEARCH_DTO,
                                      List.of(),
                                      List.of()),
                         Arguments.of(EMPTY_SEARCH_DTO,
                                      GIFT_CERT_1_TO_50_DTO_LIST,
                                      GIFT_CERT_1_TO_50_ENTITY_LIST));
    }

    private static Stream<Arguments> provideGiftCertificateSearchParamsAndPagingAndResultList() {
        return Stream.of(
            Arguments.of(ALL_PARAMETERS_SEARCH_DTO, GIFT_CERT_10_12_17_DTO_LIST, EMPTY_PAGING),
            Arguments.of(NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO, GIFT_CERT_6_TO_10_DTO_LIST, NEXT_PAGING),
            Arguments.of(NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO, GIFT_CERT_1_TO_5_DTO_LIST, FIRST_PAGING),
            Arguments.of(NO_PARAMETERS_SEARCH_LAST_PAGE_DTO, GIFT_CERT_46_TO_50_DTO_LIST, LAST_PAGING),
            Arguments.of(NO_PARAMETERS_SEARCH_PREV_PAGE_DTO, GIFT_CERT_41_TO_45_DTO_LIST, PREV_PAGING),
            Arguments.of(TAG_ONLY_SEARCH_DTO, GIFT_CERT_14_16_17_20_24_46_47_DTO_LIST, EMPTY_PAGING),
            Arguments.of(TEXT_ONLY_SEARCH_DTO, GIFT_CERT_3_13_23_30_TO_39_43_DTO_LIST, EMPTY_PAGING),
            Arguments.of(SORT_BY_NAME_ONLY_SEARCH_DTO,
                         GIFT_CERT_DTO_LIST_NAME_DESC_ORDER,
                         NAME_DESC_ORDER_PAGING),
            Arguments.of(SORT_BY_DATE_ONLY_SEARCH_DTO,
                         GIFT_CERT_DTO_LIST_DATE_ASC_ORDER,
                         DATE_ASC_ORDER_PAGING),
            Arguments.of(TAG_AND_TEXT_SEARCH_DTO, List.of(GIFT_CERT_DTO_16), EMPTY_PAGING),
            Arguments.of(EMPTY_SEARCH_DTO, GIFT_CERT_1_TO_50_DTO_LIST, EMPTY_PAGING));
    }

    @TestConfiguration
    static class GiftCertificateServiceImplTestContextConfiguration {

        @Bean
        public GiftCertificateService giftCertificateService(GiftCertificateRepository giftCertificateRepository,
                                                             ModelMapper modelMapper) {
            return new GiftCertificateServiceImpl(giftCertificateRepository, modelMapper);
        }

        @Bean
        public ModelMapper modelMapper() {
            return new ModelMapper();
        }

    }
}