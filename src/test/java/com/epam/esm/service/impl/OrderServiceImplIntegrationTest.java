package com.epam.esm.service.impl;

import static com.epam.esm.DTOConstants.OrderDTOConstants.CREATED_ORDER_DTO;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_93_TO_103_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_93_TO_97_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_94_TO_98_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_98_TO_102_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_99_TO_103_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_DTO_1;
import static com.epam.esm.DTOConstants.OrderDTOConstants.getOrderDTOToCreate;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.DateConstants.ISO_8601_DATE_PATTERN;
import static com.epam.esm.DateConstants.ISO_8601_DATE_REGEX;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.epam.esm.data.GiftCertificateRepository;
import com.epam.esm.data.OrderRepository;
import com.epam.esm.data.TagJPARepository;
import com.epam.esm.data.TagRepository;
import com.epam.esm.data.UserRepository;
import com.epam.esm.data.impl.GiftCertificateRepositoryImpl;
import com.epam.esm.data.impl.OrderRepositoryImpl;
import com.epam.esm.data.impl.TagRepositoryImpl;
import com.epam.esm.data.impl.UserRepositoryImpl;
import com.epam.esm.dto.OrderDTO;
import com.epam.esm.dto.OrderFilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchOrderException;
import com.epam.esm.service.OrderService;
import jakarta.persistence.EntityManager;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;

@DataJpaTest
class OrderServiceImplIntegrationTest {

    @Autowired
    OrderService orderService;

    @Test
    void readOrder() {
        assertEquals(ORDER_DTO_1, orderService.read(1L, 1L));
    }

    @Test
    void readWrongUserOrder() {
        assertThrows(NoSuchOrderException.class, () -> orderService.read(42L, 1L));
    }

    @Test
    @DirtiesContext
    void createOrder() {
        var actual = orderService.create(getOrderDTOToCreate());
        assertAll(() -> assertEquals(CREATED_ORDER_DTO.getId(), actual.getId()),
                  () -> assertEquals(CREATED_ORDER_DTO.getUserId(), actual.getUserId()),
                  () -> assertEquals(CREATED_ORDER_DTO.getCertificateId(), actual.getCertificateId()),
                  () -> assertThat(actual.getOrderDate().format(DateTimeFormatter.ofPattern(ISO_8601_DATE_PATTERN)),
                                   matchesPattern(ISO_8601_DATE_REGEX)),
                  () -> assertEquals(CREATED_ORDER_DTO.getOrderCost(), actual.getOrderCost()));
    }

    @ParameterizedTest
    @MethodSource("provideOrderPageParamsAndResult")
    void getOrdersList(OrderFilterParams orderFilterParams, List<OrderDTO> orderDTOList) {
        assertEquals(orderDTOList, orderService.list(orderFilterParams));
    }

    @ParameterizedTest
    @MethodSource("provideOrderPageParamsAndResultAndPaging")
    void getPagingParams(OrderFilterParams orderFilterParams, List<OrderDTO> orderDTOList,
                         PagingParamsDTO pagingParamsDTO) {
        assertEquals(pagingParamsDTO,
                     orderService.getPagingParams(orderFilterParams,
                                                  orderDTOList.get(orderDTOList.size() - 1).getId(),
                                                  orderDTOList.get(0).getId()));
    }

    private static Stream<Arguments> provideOrderPageParamsAndResult() {
        return Stream.of(Arguments.of(ORDER_NEXT_PAGE_FILTER_PARAMS,
                                      ORDER_98_TO_102_DTO_LIST),
                         Arguments.of(ORDER_FIRST_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_97_DTO_LIST),
                         Arguments.of(ORDER_LAST_PAGE_FILTER_PARAMS,
                                      ORDER_99_TO_103_DTO_LIST),
                         Arguments.of(ORDER_PREV_PAGE_FILTER_PARAMS,
                                      ORDER_94_TO_98_DTO_LIST),
                         Arguments.of(ORDER_EMPTY_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_103_DTO_LIST));
    }

    private static Stream<Arguments> provideOrderPageParamsAndResultAndPaging() {
        return Stream.of(Arguments.of(ORDER_NEXT_PAGE_FILTER_PARAMS,
                                      ORDER_98_TO_102_DTO_LIST,
                                      ORDER_NEXT_PAGING),
                         Arguments.of(ORDER_FIRST_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_97_DTO_LIST,
                                      ORDER_FIRST_PAGING),
                         Arguments.of(ORDER_LAST_PAGE_FILTER_PARAMS,
                                      ORDER_99_TO_103_DTO_LIST,
                                      ORDER_LAST_PAGING),
                         Arguments.of(ORDER_PREV_PAGE_FILTER_PARAMS,
                                      ORDER_94_TO_98_DTO_LIST,
                                      ORDER_PREV_PAGING),
                         Arguments.of(ORDER_EMPTY_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_103_DTO_LIST,
                                      EMPTY_PAGING));
    }

    @TestConfiguration
    static class OrderServiceTestContextConfiguration {

        @Bean
        public OrderRepository orderRepository(EntityManager entityManager) {
            return new OrderRepositoryImpl(entityManager);
        }

        @Bean
        public GiftCertificateRepository giftCertRepo(TagRepository tagRepository, EntityManager entityManager) {
            return new GiftCertificateRepositoryImpl(tagRepository, entityManager);
        }

        @Bean
        public TagRepository tagRepo(EntityManager entityManager, TagJPARepository tagJPARepository) {
            return new TagRepositoryImpl(entityManager, tagJPARepository);
        }

        @Bean
        OrderService orderService(ModelMapper modelMapper, OrderRepository orderRepository,
                                  UserRepository userRepository, GiftCertificateRepository giftCertificateRepository) {
            return new OrderServiceImpl(modelMapper, orderRepository, userRepository, giftCertificateRepository);
        }

        @Bean
        UserRepository userRepository(EntityManager entityManager) {
            return new UserRepositoryImpl(entityManager);
        }

    }

}
