package com.epam.esm.service.impl;

import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_1_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_1_TO_5_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_41_TO_45_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_46_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_6_TO_10_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_DTO_1;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_DTO_51;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_DTO_TO_SAVE;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_1_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_1_TO_5_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_41_TO_45_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_46_TO_50_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_6_TO_10_ENTITY_LIST;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_ENTITY_1;
import static com.epam.esm.EntityConstants.UserEntityConstants.USER_ENTITY_51;
import static com.epam.esm.EntityConstants.UserEntityConstants.getUserToSave;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

import com.epam.esm.data.UserRepository;
import com.epam.esm.domain.User;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.UserDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.service.UserService;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {

    @Autowired
    UserService userService;
    @MockBean
    UserRepository userRepository;

    @Test
    void readUser() {
        given(userRepository.read(1)).willReturn(USER_ENTITY_1);
        assertEquals(USER_DTO_1, userService.read(1));
    }

    @ParameterizedTest
    @MethodSource("provideUserPageParamsAndResult")
    void getUsersList(FilterParams filterParams, List<UserDTO> userDTOList, List<User> userList) {
        given(userRepository.list(filterParams)).willReturn(userList);
        assertEquals(userDTOList, userService.list(filterParams));

    }

    @ParameterizedTest
    @MethodSource("provideUserPageParamsAndResultAndPaging")
    void getPagingParams(FilterParams filterParams, List<UserDTO> userDTOList, PagingParamsDTO pagingParamsDTO) {
        given(userRepository.getNextAfterId(filterParams, userDTOList.get(userDTOList.size() - 1).getId())).willReturn(
            pagingParamsDTO.getNextAfterId());
        given(userRepository.getPrevAfterId(filterParams, userDTOList.get(0).getId())).willReturn(
            pagingParamsDTO.getPrevAfterId());
        given(userRepository.getLastAfterId(filterParams)).willReturn(
            pagingParamsDTO.getLastAfterId());
        assertEquals(pagingParamsDTO,
                     userService.getPagingParams(filterParams,
                                                 userDTOList.get(userDTOList.size() - 1).getId(),
                                                 userDTOList.get(0).getId()));
    }

    @Test
    void createUser() {
        given(userRepository.create(getUserToSave())).willReturn(USER_ENTITY_51);
        assertEquals(USER_DTO_51, userService.create(USER_DTO_TO_SAVE));
    }

    private static Stream<Arguments> provideUserPageParamsAndResult() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS,
                                      USER_6_TO_10_DTO_LIST,
                                      USER_6_TO_10_ENTITY_LIST),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS,
                                      USER_1_TO_5_DTO_LIST,
                                      USER_1_TO_5_ENTITY_LIST),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS,
                                      USER_46_TO_50_DTO_LIST,
                                      USER_46_TO_50_ENTITY_LIST),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS,
                                      USER_41_TO_45_DTO_LIST,
                                      USER_41_TO_45_ENTITY_LIST),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS,
                                      USER_1_TO_50_DTO_LIST,
                                      USER_1_TO_50_ENTITY_LIST));
    }

    private static Stream<Arguments> provideUserPageParamsAndResultAndPaging() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS,
                                      USER_6_TO_10_DTO_LIST,
                                      NEXT_PAGING),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS,
                                      USER_1_TO_5_DTO_LIST,
                                      FIRST_PAGING),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS,
                                      USER_46_TO_50_DTO_LIST,
                                      LAST_PAGING),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS,
                                      USER_41_TO_45_DTO_LIST,
                                      PREV_PAGING),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS,
                                      USER_1_TO_50_DTO_LIST,
                                      EMPTY_PAGING));
    }

    @TestConfiguration
    static class UserServiceTestContextConfiguration {

        @Bean
        UserService userService(ModelMapper modelMapper, UserRepository userRepository) {
            return new UserServiceImpl(modelMapper, userRepository);
        }

        @Bean
        ModelMapper modelMapper() {
            return new ModelMapper();
        }
    }
}