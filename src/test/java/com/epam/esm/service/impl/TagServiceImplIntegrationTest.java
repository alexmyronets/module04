package com.epam.esm.service.impl;


import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.TagDTOConstants.SAVED_TAG_DTO;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_1_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_1_TO_5_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_41_TO_55_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_46_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_6_TO_10_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_1;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_3;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_TO_SAVE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.epam.esm.data.TagJPARepository;
import com.epam.esm.data.TagRepository;
import com.epam.esm.data.impl.TagRepositoryImpl;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.TagDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchTagException;
import com.epam.esm.service.TagService;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;

@DataJpaTest
class TagServiceImplIntegrationTest {

    @Autowired
    TagService tagService;

    @Test
    @DirtiesContext
    void createTag() {
        assertEquals(SAVED_TAG_DTO, tagService.create(TAG_DTO_TO_SAVE));
    }

    @Test
    void readTag() {
        assertEquals(TAG_DTO_1, tagService.read(1));
    }

    @Test
    @DirtiesContext
    void deleteTag() {
        tagService.delete(9);
        assertThrows(NoSuchTagException.class, () ->  tagService.read(9));
    }

    @Test
    void getMostUsedTagByMostSpentUserOrders() {
        assertEquals(TAG_DTO_3, tagService.readMostUsedTagByMostSpendUser());
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResult")
    void getAllTags(FilterParams filterParams, List<TagDTO> tagDTOList) {
        assertEquals(tagDTOList, tagService.list(filterParams));
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResultAndPaging")
    void getPagingParams(FilterParams filterParams, List<TagDTO> tagDTOList, PagingParamsDTO pagingParamsDTO) {

        assertEquals(pagingParamsDTO,
                     tagService.getPagingParams(filterParams,
                                                tagDTOList.get(tagDTOList.size() - 1).getId(),
                                                tagDTOList.get(0).getId()));
    }

    private static Stream<Arguments> provideTagPageParamsAndResult() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS,
                                      TAG_6_TO_10_DTO_LIST),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_5_DTO_LIST),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS,
                                      TAG_46_TO_50_DTO_LIST),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS,
                                      TAG_41_TO_55_DTO_LIST),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_50_DTO_LIST));
    }

    private static Stream<Arguments> provideTagPageParamsAndResultAndPaging() {
        return Stream.of(Arguments.of(NEXT_PAGE_FILTER_PARAMS,
                                      TAG_6_TO_10_DTO_LIST,
                                      NEXT_PAGING),
                         Arguments.of(FIRST_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_5_DTO_LIST,
                                      FIRST_PAGING),
                         Arguments.of(LAST_PAGE_FILTER_PARAMS,
                                      TAG_46_TO_50_DTO_LIST,
                                      LAST_PAGING),
                         Arguments.of(PREV_PAGE_FILTER_PARAMS,
                                      TAG_41_TO_55_DTO_LIST,
                                      PREV_PAGING),
                         Arguments.of(EMPTY_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_50_DTO_LIST,
                                      EMPTY_PAGING));
    }

    @TestConfiguration
    static class TagServiceTestContextConfiguration {

        @Bean
        public TagRepository tagRepo(EntityManager entityManager, TagJPARepository tagJPARepository) {
            return new TagRepositoryImpl(entityManager, tagJPARepository);
        }

        @Bean
        TagService tagService(@Qualifier("tagRepo") TagRepository tagRepository, ModelMapper modelMapper) {
            return new TagServiceImpl(modelMapper, tagRepository);
        }

    }

}