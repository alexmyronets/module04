package com.epam.esm.web;

import static com.epam.esm.utils.TestUtils.getBaseUrl;
import static com.epam.esm.utils.TestUtils.getJsonString;
import static com.epam.esm.utils.TestUtils.getUpdateBaseURL;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureMockMvc
class TagControllerIntegrationTest {

    private static final String JSON_PATH = "json/tag/";

    @Autowired
    MockMvc mockMvc;

    @Test
    @DirtiesContext
    @WithMockUser(roles = "ADMIN")
    void postValidTagByAdmin_thenCreated() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidTagRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidTagResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(post("/api/tags").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser
    void postValidTagByUser_thenForbidden() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidTagRequest.json", getClass());
        String responseBodyJson = getJsonString("json/security/403ForbiddenResponse.json", getClass());

        mockMvc.perform(post("/api/tags").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isForbidden())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postValidTagNoAuth_thenUnauthorised() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidTagRequest.json", getClass());
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(post("/api/tags").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isUnauthorized())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void postExistingTag_thenConflict() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingTagRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingTagResponse.json", getClass());

        mockMvc.perform(post("/api/tags").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @ParameterizedTest
    @WithMockUser(roles = "ADMIN")
    @MethodSource("provideTagPageParamsAndResult")
    void getTagsListByAdmin_thenOk(String URL, String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @ParameterizedTest
    @WithMockUser
    @MethodSource("provideTagPageParamsAndResult")
    void getTagsListByUser_thenOk(String URL, String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getTagsListNoAuth_thenUnauthorised() throws Exception {
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(get("/api/tags"))
               .andExpect(status().isUnauthorized())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @ParameterizedTest
    @MethodSource("provideAuthorizedRoles")
    void getExistingTagAuthorized_thenOk(String role) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingTagResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get("/api/tags/1").with(user("user").roles(role)))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingTagNoAuth_thenUnauthorised() throws Exception {
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(get("/api/tags/1"))
               .andExpect(status().isUnauthorized())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser
    void getNonExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingTagResponse.json", getClass());

        mockMvc.perform(get("/api/tags/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DirtiesContext
    void deleteNotUsedTagByAdmin_thenNoContent() throws Exception {
        mockMvc.perform(delete("/api/tags/9")).andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser
    void deleteNotUsedTagByUser_thenForbidden() throws Exception {
        String responseBodyJson = getJsonString("json/security/403ForbiddenResponse.json", getClass());

        mockMvc.perform(delete("/api/tags/9"))
               .andExpect(status().isForbidden())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteNotUsedTagNoAuth_thenUnauthorised() throws Exception {
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(delete("/api/tags/9"))
               .andExpect(status().isUnauthorized())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteUsedTag_thenConflict() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[DELETE]UsedTagResponse.json", getClass());

        mockMvc.perform(delete("/api/tags/5"))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteNotExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[DELETE]NonExistingTagResponse.json", getClass());

        mockMvc.perform(delete("/api/tags/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @ParameterizedTest
    @MethodSource("provideAuthorizedRoles")
    void getMostUsedTagByMostSpentUserOrders_thenOk(String role) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]MostUsedTagByMostSpentUserOrders.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get("/api/tags/most_used_tag_by_most_spent_user").with(user("user").roles(role)))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));

    }

    @Test
    void getMostUsedTagByMostSpentUserOrdersNoAuth_thenUnauthorised() throws Exception {
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(get("/api/tags/most_used_tag_by_most_spent_user"))
               .andExpect(status().isUnauthorized())
               .andExpect(content().json(responseBodyJson));
    }

    private static Stream<Arguments> provideTagPageParamsAndResult() {
        return Stream.of(Arguments.of("/api/tags?page.size=5&page.afterId=5&page.direction=FORWARD",
                                      "[GET]NextPageResponse.json"),
                         Arguments.of("/api/tags?page.size=5", "[GET]FirstPageResponse.json"),
                         Arguments.of("/api/tags?page.size=5&page.afterId=45&page.direction=FORWARD",
                                      "[GET]LastPageResponse.json"),
                         Arguments.of("/api/tags?page.size=5&page.afterId=46&page.direction=BACK",
                                      "[GET]PreviousPageResponse.json"),
                         Arguments.of("/api/tags", "[GET]NoPageParamsResponse.json"));
    }

    private static Stream<Arguments> provideAuthorizedRoles() {
        return Stream.of(Arguments.of("ADMIN"), Arguments.of("USER"));
    }

}