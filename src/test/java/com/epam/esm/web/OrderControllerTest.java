package com.epam.esm.web;

import static com.epam.esm.DTOConstants.OrderDTOConstants.CREATED_ORDER_DTO;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_93_TO_103_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_93_TO_97_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_94_TO_98_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_98_TO_102_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_99_TO_103_DTO_LIST;
import static com.epam.esm.DTOConstants.OrderDTOConstants.ORDER_DTO_1;
import static com.epam.esm.DTOConstants.OrderDTOConstants.getOrderDTOToCreate;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.ORDER_PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ORDER_PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.utils.TestUtils.getBaseUrl;
import static com.epam.esm.utils.TestUtils.getJsonString;
import static com.epam.esm.utils.TestUtils.getUpdateBaseURL;
import static org.mockito.Mockito.when;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.dto.OrderDTO;
import com.epam.esm.dto.OrderFilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchOrderException;
import com.epam.esm.service.OrderService;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(OrderController.class)
@AutoConfigureMockMvc(addFilters = false)
class OrderControllerTest {

    private static final String JSON_PATH = "json/order/";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    OrderService orderService;

    @ParameterizedTest
    @MethodSource("provideOrdersPageParamsAndResult")
    void getOrdersListByUser_thenOk(String URL, OrderFilterParams filterParams,
                                    List<OrderDTO> orderDTOlist, PagingParamsDTO pagingParams,
                                    String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        orderDTOlist.forEach(RepresentationModel::removeLinks);
        when(orderService.list(filterParams)).thenReturn(orderDTOlist);
        when(orderService.getPagingParams(filterParams,
                                          orderDTOlist.get(orderDTOlist.size() - 1).getId(),
                                          orderDTOlist.get(0).getId())).thenReturn(pagingParams);

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingOrderByUser_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingOrderResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        when(orderService.read(1L, 1L)).thenReturn(ORDER_DTO_1);

        mockMvc.perform(get("/api/users/1/orders/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingOrderByUser_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingOrderResponse.json", getClass());

        when(orderService.read(1L, 442L)).thenThrow(new NoSuchOrderException(
            "Requested resource not found (id = 442)"));

        mockMvc.perform(get("/api/users/1/orders/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postValidOrder_thenCreated() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidOrderRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidOrderResponse.json", getClass());

        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        when(orderService.create(getOrderDTOToCreate())).thenReturn(CREATED_ORDER_DTO);

        mockMvc.perform(post("/api/users/1/orders").content(requestBodyJson)
                                                   .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidJsonFormat_thenBadRequest() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]InvalidJsonFormatOrderRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]InvalidJsonFormatOrderResponse.json", getClass());

        mockMvc.perform(post("/api/users/1/orders").content(requestBodyJson)
                                                   .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidOrder_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]InvalidDataOrderRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]InvalidDataOrderResponse.json", getClass());

        mockMvc.perform(post("/api/users/1/orders").content(requestBodyJson)
                                                   .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    private static Stream<Arguments> provideOrdersPageParamsAndResult() {
        return Stream.of(Arguments.of("/api/users/17/orders?page.size=5&page.afterId=97&page.direction=FORWARD",
                                      ORDER_NEXT_PAGE_FILTER_PARAMS,
                                      ORDER_98_TO_102_DTO_LIST,
                                      ORDER_NEXT_PAGING,
                                      "[GET]UserOrdersNextPageResponse.json"),
                         Arguments.of("/api/users/17/orders?page.size=5",
                                      ORDER_FIRST_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_97_DTO_LIST,
                                      ORDER_FIRST_PAGING,
                                      "[GET]UserOrdersFirstPageResponse.json"),
                         Arguments.of("/api/users/17/orders?page.size=5&page.afterId=98&page.direction=FORWARD",
                                      ORDER_LAST_PAGE_FILTER_PARAMS,
                                      ORDER_99_TO_103_DTO_LIST,
                                      ORDER_LAST_PAGING,
                                      "[GET]UserOrdersLastPageResponse.json"),
                         Arguments.of("/api/users/17/orders?page.size=5&page.afterId=99&page.direction=BACK",
                                      ORDER_PREV_PAGE_FILTER_PARAMS,
                                      ORDER_94_TO_98_DTO_LIST,
                                      ORDER_PREV_PAGING,
                                      "[GET]UserOrdersPreviousPageResponse.json"),
                         Arguments.of("/api/users/17/orders",
                                      ORDER_EMPTY_PAGE_FILTER_PARAMS,
                                      ORDER_93_TO_103_DTO_LIST,
                                      EMPTY_PAGING,
                                      "[GET]UserOrdersNoPageParamsResponse.json"));
    }
}
