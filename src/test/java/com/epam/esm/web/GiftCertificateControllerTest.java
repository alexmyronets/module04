package com.epam.esm.web;

import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_10_12_17_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_14_16_17_20_24_46_47_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_1_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_1_TO_5_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_3_13_23_30_TO_39_43_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_41_TO_45_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_46_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_6_TO_10_DTO_LIST;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_1;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_16;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_LIST_DATE_ASC_ORDER;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_LIST_NAME_DESC_ORDER;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.GIFT_CERT_DTO_PATCHED;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.SAVED_GIFT_CERT_DTO;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.getGiftCertificateDTOToSave;
import static com.epam.esm.DTOConstants.GiftCertificateDTOConstants.getGiftCertificateDtoPatch;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.DATE_ASC_ORDER_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NAME_DESC_ORDER_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.ALL_PARAMETERS_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_LAST_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NO_PARAMETERS_SEARCH_PREV_PAGE_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.SORT_BY_DATE_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.SORT_BY_NAME_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_AND_TEXT_EMPTY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_AND_TEXT_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TAG_ONLY_SEARCH_DTO;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.TEXT_ONLY_SEARCH_DTO;
import static com.epam.esm.utils.TestUtils.getBaseUrl;
import static com.epam.esm.utils.TestUtils.getJsonString;
import static com.epam.esm.utils.TestUtils.getUpdateBaseURL;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.dto.GiftCertificateDTO;
import com.epam.esm.dto.GiftCertificateFilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchGiftCertificateException;
import com.epam.esm.service.GiftCertificateService;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
@WebMvcTest(GiftCertificateController.class)
@AutoConfigureMockMvc(addFilters = false)
class GiftCertificateControllerTest {

    private static final String JSON_PATH = "json/giftCertificate/";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GiftCertificateService giftCertificateService;

    @Test
    void postValidGiftCert_thenCreated() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidGiftCertificateRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidGiftCertificateResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        when(giftCertificateService.create(getGiftCertificateDTOToSave())).thenReturn(SAVED_GIFT_CERT_DTO);

        this.mockMvc.perform(post("/api/certificates").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(HAL_JSON_VALUE))
                    .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidJsonFormat_thenBadRequest() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]InvalidJsonFormatGiftCertificateRequest.json",
                                               getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]InvalidJsonFormatGiftCertificateResponse.json",
                                                getClass());

        mockMvc.perform(post("/api/certificates").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidGiftCert_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]InvalidDataGiftCertificateRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]InvalidDataGiftCertificateResponse.json",
                                                getClass());

        mockMvc.perform(post("/api/certificates").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndResult")
    void getGiftCertificatesList_thenOk(String URL, GiftCertificateFilterParams giftCertificateFilterParams,
                                        List<GiftCertificateDTO> giftCertificateDTOS, PagingParamsDTO pagingParams,
                                        String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        giftCertificateDTOS.forEach(cert -> {
            cert.removeLinks();
            cert.getTags().forEach(RepresentationModel::removeLinks);
        });

        when(giftCertificateService.list(giftCertificateFilterParams)).thenReturn(giftCertificateDTOS);
        if (!giftCertificateDTOS.isEmpty()) {
            when(giftCertificateService.getPagingParams(giftCertificateFilterParams,
                                                        giftCertificateDTOS.get(giftCertificateDTOS.size() - 1).getId(),
                                                        giftCertificateDTOS.get(0).getId())).thenReturn(pagingParams);
        }

        mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(status().isOk()).andExpect(content().contentType(
            HAL_JSON_VALUE)).andExpect(content().json(responseBodyJson));
    }

    @Test
    void getInvalidFilterGiftCertificatesList_thenBadRequestWithErrors() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]InvalidDataSearchResponse.json", getClass());

        mockMvc.perform(get("/api/certificates?tags=ta&searchText=&sortByName=ASC&sortByDate=DESC"))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingGiftCert_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingGiftCertificateResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);

        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        when(giftCertificateService.read(1)).thenReturn(GIFT_CERT_DTO_1);

        mockMvc.perform(get("/api/certificates/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingGiftCert_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingGiftCertificateResponse.json", getClass());
        when(giftCertificateService.read(442)).thenThrow(new NoSuchGiftCertificateException(
            "Requested resource not found (id = 442)"));

        mockMvc.perform(get("/api/certificates/442")).andExpect(status().isNotFound()).andExpect(content().contentType(
            MediaType.APPLICATION_JSON)).andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchValidGiftCert_thenOk() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[PATCH]ValidGiftCertificateRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[PATCH]ValidGiftCertificateResponse.json", getClass());

        when(giftCertificateService.update(getGiftCertificateDtoPatch(), 1)).thenReturn(
            GIFT_CERT_DTO_PATCHED.removeLinks());

        mockMvc.perform(patch("/api/certificates/1").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchNonExistingGiftCert_thenNotFound() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[PATCH]NonExistingGiftCertificateRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[PATCH]NonExistingGiftCertificateResponse.json",
                                                getClass());

        when(giftCertificateService.update(getGiftCertificateDtoPatch(),
                                           442)).thenThrow(new NoSuchGiftCertificateException(
            "Requested resource not found (id = 442)"));

        mockMvc.perform(patch("/api/certificates/442").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchInvalidJsonFormat_thenBadRequest() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[PATCH]InvalidJsonFormatGiftCertificateRequest.json",
                                               getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[PATCH]InvalidJsonFormatGiftCertificateResponse.json",
                                                getClass());

        mockMvc.perform(patch("/api/certificates/1").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchInvalidGiftCert_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[PATCH]InvalidDataGiftCertificateRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[PATCH]InvalidDataGiftCertificateResponse.json",
                                                getClass());

        mockMvc.perform(patch("/api/certificates/1").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteExistingGiftCert_thenNoContent() throws Exception {

        mockMvc.perform(delete("/api/certificates/1")).andExpect(status().isNoContent());
        verify(giftCertificateService).delete(1);
    }

    @Test
    void deleteNonExistingGiftCert_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[DELETE]NonExistingGiftCertificateResponse.json",
                                                getClass());

        doThrow(new NoSuchGiftCertificateException("Requested resource not found (id = 442)")).when(
            giftCertificateService).delete(442);

        mockMvc.perform(delete("/api/certificates/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void unExpectedExceptionTest() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[PATCH]ValidGiftCertificateRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[ALL]InternalServerErrorResponse.json", getClass());

        when(giftCertificateService.update(getGiftCertificateDtoPatch(),
                                           500)).thenThrow(new RuntimeException());

        mockMvc.perform(patch("/api/certificates/500").content(requestBodyJson).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isInternalServerError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));

    }

    private static Stream<Arguments> provideGiftCertificateSearchParamsAndResult() {
        return Stream.of(Arguments.of("/api/certificates?tags=tag2,tag3&searchText=1&sortByName=ASC&sortByDate=DESC",
                                      ALL_PARAMETERS_SEARCH_DTO,
                                      GIFT_CERT_10_12_17_DTO_LIST,
                                      EMPTY_PAGING,
                                      "[GET]AllParametersSearchResponse.json"),
                         Arguments.of("/api/certificates?page.size=5&page.afterId=5&page.direction=FORWARD",
                                      NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO,
                                      GIFT_CERT_6_TO_10_DTO_LIST,
                                      NEXT_PAGING,
                                      "[GET]NoParamsSearchNextPageResponse.json"),
                         Arguments.of("/api/certificates?page.size=5",
                                      NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO,
                                      GIFT_CERT_1_TO_5_DTO_LIST,
                                      FIRST_PAGING,
                                      "[GET]NoParamsSearchFirstPageResponse.json"),
                         Arguments.of("/api/certificates?page.size=5&page.afterId=45&page.direction=FORWARD",
                                      NO_PARAMETERS_SEARCH_LAST_PAGE_DTO,
                                      GIFT_CERT_46_TO_50_DTO_LIST,
                                      LAST_PAGING,
                                      "[GET]NoParamsSearchLastPageResponse.json"),
                         Arguments.of("/api/certificates?page.size=5&page.afterId=46&page.direction=BACK",
                                      NO_PARAMETERS_SEARCH_PREV_PAGE_DTO,
                                      GIFT_CERT_41_TO_45_DTO_LIST,
                                      PREV_PAGING,
                                      "[GET]NoParamsSearchPreviousPageResponse.json"),
                         Arguments.of("/api/certificates?tags=tag1",
                                      TAG_ONLY_SEARCH_DTO,
                                      GIFT_CERT_14_16_17_20_24_46_47_DTO_LIST,
                                      EMPTY_PAGING,
                                      "[GET]TagOnlySearchResponse.json"),
                         Arguments.of("/api/certificates?searchText=3",
                                      TEXT_ONLY_SEARCH_DTO,
                                      GIFT_CERT_3_13_23_30_TO_39_43_DTO_LIST,
                                      EMPTY_PAGING,
                                      "[GET]TextOnlySearchResponse.json"),
                         Arguments.of("/api/certificates?sortByName=DESC&page.size=5",
                                      SORT_BY_NAME_ONLY_SEARCH_DTO,
                                      GIFT_CERT_DTO_LIST_NAME_DESC_ORDER,
                                      NAME_DESC_ORDER_PAGING,
                                      "[GET]SortByNameOnlySearchResponse.json"),
                         Arguments.of("/api/certificates?sortByDate=ASC&page.size=5",
                                      SORT_BY_DATE_ONLY_SEARCH_DTO,
                                      GIFT_CERT_DTO_LIST_DATE_ASC_ORDER,
                                      DATE_ASC_ORDER_PAGING,
                                      "[GET]SortByDateOnlySearchResponse.json"),
                         Arguments.of("/api/certificates?tags=tag7&searchText=1",
                                      TAG_AND_TEXT_SEARCH_DTO,
                                      List.of(GIFT_CERT_DTO_16),
                                      EMPTY_PAGING,
                                      "[GET]TagAndTextSearchResponse.json"),
                         Arguments.of("/api/certificates?tags=tag7&searchText=5",
                                      TAG_AND_TEXT_EMPTY_SEARCH_DTO,
                                      List.of(),
                                      EMPTY_PAGING,
                                      "[GET]EmptyResultResponse.json"),
                         Arguments.of("/api/certificates",
                                      EMPTY_SEARCH_DTO,
                                      GIFT_CERT_1_TO_50_DTO_LIST,
                                      EMPTY_PAGING,
                                      "[GET]NoParamsSearchResponse.json"));
    }
}