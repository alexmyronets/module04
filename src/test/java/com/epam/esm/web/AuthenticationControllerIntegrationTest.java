package com.epam.esm.web;

import static com.epam.esm.utils.TestUtils.getJsonString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.utils.IsValidTokenMatcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureMockMvc
class AuthenticationControllerIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private IsValidTokenMatcher tokenMatcher;

    @Test
    void getTokenValidCredentials_thenOk() throws Exception {

        mockMvc.perform(get("/api/login").with(httpBasic("user1", "password1")))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.access_token").value(tokenMatcher))
               .andExpect(jsonPath("$.refresh_token").value(tokenMatcher));

    }

    @Test
    void getTokenInvalidCredentials_thenOk() throws Exception {
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(get("/api/login").with(httpBasic("user1", "1")))
               .andExpect(status().isUnauthorized())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getGoogleAuthCodeGrantURL() throws Exception {
        String responseBodyJson = getJsonString("json/security/[GET]AuthorizationCodeGrantGoogleURLResponse.json",
                                                getClass());

        mockMvc.perform(get("/api/login/oauth/google"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andDo(result -> {
                   String actual = result.getResponse()
                                         .getContentAsString()
                                         .replaceAll(
                                             "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}",
                                             "");
                   assertEquals(responseBodyJson, actual);
               });
    }

    @Test
    void getGoogleImplicitGrantURL() throws Exception {
        String responseBodyJson = getJsonString("json/security/[GET]ImplicitGrantGoogleURLResponse.json",
                                                getClass());

        mockMvc.perform(get("/api/login/oauth/google/implicit"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andDo(result -> {
                   String actual = result.getResponse()
                                         .getContentAsString()
                                         .replaceAll(
                                             "nonce=[a-z0-9]{20,40}",
                                             "");
                   assertEquals(responseBodyJson, actual);
               });
    }

}