package com.epam.esm.web;

import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.UserDTOConstants.EXISTING_USER_DTO_TO_SAVE;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_1_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_1_TO_5_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_41_TO_45_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_46_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_6_TO_10_DTO_LIST;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_DTO_1;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_DTO_51;
import static com.epam.esm.DTOConstants.UserDTOConstants.USER_DTO_TO_SAVE;
import static com.epam.esm.EntityConstants.AuthenticationConstants.AUTH_CANDIDATE;
import static com.epam.esm.EntityConstants.AuthenticationConstants.AUTH_TRUSTED;
import static com.epam.esm.EntityConstants.AuthenticationConstants.JWT_TOKEN;
import static com.epam.esm.utils.TestUtils.getBaseUrl;
import static com.epam.esm.utils.TestUtils.getJsonString;
import static com.epam.esm.utils.TestUtils.getUpdateBaseURL;
import static org.mockito.Mockito.when;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.UserDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchUserException;
import com.epam.esm.exception.UserAlreadyExistsException;
import com.epam.esm.service.SecurityService;
import com.epam.esm.service.UserService;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(UserController.class)
@AutoConfigureMockMvc(addFilters = false)
class UserControllerTest {

    private static final String JSON_PATH = "json/user/";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @MockBean
    SecurityService securityService;

    @MockBean
    PasswordEncoder passwordEncoder;

    @MockBean
    AuthenticationManager authenticationManager;

    @ParameterizedTest
    @MethodSource("provideUserPageParamsAndResult")
    void getUsersList_thenOk(String URL, FilterParams filterParams, List<UserDTO> userDTOlist, PagingParamsDTO pagingParams, String responseJsonFile)
        throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        userDTOlist.forEach(RepresentationModel::removeLinks);

        when(userService.list(filterParams)).thenReturn(userDTOlist);
        when(userService.getPagingParams(filterParams,
                                         userDTOlist.get(userDTOlist.size() - 1).getId(),
                                         userDTOlist.get(0).getId())).thenReturn(pagingParams);

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingUser_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingUserResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        when(userService.read(1)).thenReturn(USER_DTO_1);

        mockMvc.perform(get("/api/users/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingUser_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingUserResponse.json", getClass());

        when(userService.read(442)).thenThrow(new NoSuchUserException("Requested resource not found (id = 442)"));

        mockMvc.perform(get("/api/users/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postValidUser_thenCreated() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidUserRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidUserResponse.json", getClass());

        when(passwordEncoder.encode("Password51")).thenReturn(
            "$2a$10$dBuqwRrcWTgLYWAG9GrlYeqr.JRXNQzgOGVxWHZ4bXoFeXqNR47tq");
        when(userService.create(USER_DTO_TO_SAVE)).thenReturn(USER_DTO_51);
        when(authenticationManager.authenticate(AUTH_CANDIDATE)).thenReturn(AUTH_TRUSTED);
        when(securityService.generateJwtToken(AUTH_TRUSTED)).thenReturn(JWT_TOKEN);

        mockMvc.perform(post("/api/users").content(requestBodyJson).contentType(APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postValidExistingUser_thenConflict() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingUserRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingUserResponse.json", getClass());

        when(passwordEncoder.encode("Password1")).thenReturn(
            "$2a$10$dBuqwRrcWTgLYWAG9GrlYeqr.JRXNQzgOGVxWHZ4bXoFeXqNR47tq");
        when(userService.create(EXISTING_USER_DTO_TO_SAVE)).thenThrow(new UserAlreadyExistsException(
            "User (name = user11) already exists"));

        mockMvc.perform(post("/api/users").content(requestBodyJson).contentType(APPLICATION_JSON))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidJsonFormat_thenBadRequest() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]InvalidJsonFormatUserRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]InvalidJsonFormatUserResponse.json", getClass());

        mockMvc.perform(post("/api/users").content(requestBodyJson)
                                          .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidUser_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]InvalidDataUserRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]InvalidDataUserResponse.json", getClass());

        mockMvc.perform(post("/api/users").content(requestBodyJson)
                                          .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }


    private static Stream<Arguments> provideUserPageParamsAndResult() {
        return Stream.of(Arguments.of("/api/users?page.size=5&page.afterId=5&page.direction=FORWARD",
                                      NEXT_PAGE_FILTER_PARAMS,
                                      USER_6_TO_10_DTO_LIST,
                                      NEXT_PAGING,
                                      "[GET]NextPageResponse.json"),
                         Arguments.of("/api/users?page.size=5",
                                      FIRST_PAGE_FILTER_PARAMS,
                                      USER_1_TO_5_DTO_LIST,
                                      FIRST_PAGING,
                                      "[GET]FirstPageResponse.json"),
                         Arguments.of("/api/users?page.size=5&page.afterId=45&page.direction=FORWARD",
                                      LAST_PAGE_FILTER_PARAMS,
                                      USER_46_TO_50_DTO_LIST,
                                      LAST_PAGING,
                                      "[GET]LastPageResponse.json"),
                         Arguments.of("/api/users?page.size=5&page.afterId=46&page.direction=BACK",
                                      PREV_PAGE_FILTER_PARAMS,
                                      USER_41_TO_45_DTO_LIST,
                                      PREV_PAGING,
                                      "[GET]PreviousPageResponse.json"),
                         Arguments.of("/api/users",
                                      EMPTY_PAGE_FILTER_PARAMS,
                                      USER_1_TO_50_DTO_LIST,
                                      EMPTY_PAGING,
                                      "[GET]NoPageParamsResponse.json"));
    }


}
