package com.epam.esm.web;

import static com.epam.esm.utils.TestUtils.getBaseUrl;
import static com.epam.esm.utils.TestUtils.getJsonString;
import static com.epam.esm.utils.TestUtils.getUpdateBaseURL;
import static org.hamcrest.Matchers.matchesPattern;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.DateConstants;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureMockMvc
class GiftCertificateControllerIntegrationTest {

    private static final String JSON_PATH = "json/giftCertificate/";

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DirtiesContext
    @WithMockUser(roles = "ADMIN")
    void postValidGiftCert_thenCreated() throws Exception {
        String requestBodyJson = getJsonString("json/giftCertificate/[POST]ValidGiftCertificateRequest.json",
                                               getClass());
        String baseUrl = getBaseUrl(this.mockMvc);

        this.mockMvc.perform(post("/api/certificates").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(HAL_JSON_VALUE))
                    .andExpect(jsonPath("$.id").value("51"))
                    .andExpect(jsonPath("$.name").value("cert51Name"))
                    .andExpect(jsonPath("$.description").value("cert51Description"))
                    .andExpect(jsonPath("$.price").value("50.0"))
                    .andExpect(jsonPath("$.duration").value("90"))
                    .andExpect(jsonPath("$.createDate", matchesPattern(DateConstants.ISO_8601_DATE_REGEX)))
                    .andExpect(jsonPath("$.lastUpdateDate", matchesPattern(DateConstants.ISO_8601_DATE_REGEX)))
                    .andExpect(jsonPath("$._links.self.href").value(baseUrl + "/api/certificates/51"))
                    .andExpect(jsonPath("$.tags[0].id").value("51"))
                    .andExpect(jsonPath("$.tags[0].name").value("tag51"))
                    .andExpect(jsonPath("$.tags[0]._links.self.href").value(baseUrl + "/api/tags/51"))
                    .andExpect(jsonPath("$.tags[1].id").value("42"))
                    .andExpect(jsonPath("$.tags[1].name").value("tag42"))
                    .andExpect(jsonPath("$.tags[1]._links.self.href").value(baseUrl + "/api/tags/42"));
    }

    @Test
    void postGiftCertNoAuth_thenUnauthorised() throws Exception {
        String requestBodyJson = getJsonString("json/giftCertificate/[POST]ValidGiftCertificateRequest.json",
                                               getClass());
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        this.mockMvc.perform(post("/api/certificates").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isUnauthorized())
                    .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser
    void postGiftCertNoPrivileges_thenForbidden() throws Exception {
        String requestBodyJson = getJsonString("json/giftCertificate/[POST]ValidGiftCertificateRequest.json",
                                               getClass());
        String responseBodyJson = getJsonString("json/security/403ForbiddenResponse.json", getClass());

        this.mockMvc.perform(post("/api/certificates").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isForbidden())
                    .andExpect(content().json(responseBodyJson));
    }

    @ParameterizedTest
    @MethodSource("provideGiftCertificateSearchParamsAndResult")
    void getGiftCertificatesList_thenOk(String URL, String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingGiftCert_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingGiftCertificateResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(MockMvcRequestBuilders.get("/api/certificates/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingGiftCert_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingGiftCertificateResponse.json", getClass());

        mockMvc.perform(get("/api/certificates/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @DirtiesContext
    @WithMockUser(roles = "ADMIN")
    void patchValidGiftCert_thenOk() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[PATCH]ValidGiftCertificateRequest.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);

        mockMvc.perform(patch("/api/certificates/1").content(requestBodyJson).contentType(APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(jsonPath("$.id").value("1"))
               .andExpect(jsonPath("$.name").value("cert1NamePatched"))
               .andExpect(jsonPath("$.description").value("cert1DescriptionPatched"))
               .andExpect(jsonPath("$.price").value("500.0"))
               .andExpect(jsonPath("$.duration").value("120"))
               .andExpect(jsonPath("$.createDate").value("2022-12-30T15:07:30.965"))
               .andExpect(jsonPath("$.lastUpdateDate", matchesPattern(DateConstants.ISO_8601_DATE_REGEX)))
               .andExpect(jsonPath("$._links.self.href").value(baseUrl + "/api/certificates/1"))
               .andExpect(jsonPath("$.tags[0].id").value("51"))
               .andExpect(jsonPath("$.tags[0].name").value("tag51"))
               .andExpect(jsonPath("$.tags[0]._links.self.href").value(baseUrl + "/api/tags/51"));
    }

    @Test
    void patchGiftCertNoAuth_thenUnauthorised() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[PATCH]ValidGiftCertificateRequest.json", getClass());
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(patch("/api/certificates/1").content(requestBodyJson).contentType(APPLICATION_JSON))
               .andExpect(status().isUnauthorized())
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser
    void patchGiftCertNoPrivileges_thenForbidden() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[PATCH]ValidGiftCertificateRequest.json", getClass());
        String responseBodyJson = getJsonString("json/security/403ForbiddenResponse.json", getClass());

        mockMvc.perform(patch("/api/certificates/1").content(requestBodyJson).contentType(APPLICATION_JSON))
               .andExpect(status().isForbidden())
               .andExpect(content().json(responseBodyJson));
    }


    @Test
    @WithMockUser(roles = "ADMIN")
    void patchNonExistingGiftCert_thenNotFound() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[PATCH]NonExistingGiftCertificateRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[PATCH]NonExistingGiftCertificateResponse.json",
                                                getClass());

        mockMvc.perform(patch("/api/certificates/442").content(requestBodyJson).contentType(APPLICATION_JSON))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @DirtiesContext
    @WithMockUser(roles = "ADMIN")
    void deleteExistingGiftCert_thenNoContent() throws Exception {
        mockMvc.perform(delete("/api/certificates/25")).andExpect(status().isNoContent());
    }

    @Test
    void deleteGiftCertNoAuth_thenUnauthorised() throws Exception {
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(delete("/api/certificates/25"))
               .andExpect(status().isUnauthorized())
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @DirtiesContext
    @WithMockUser
    void deleteGiftCert_thenNoContent() throws Exception {
        String responseBodyJson = getJsonString("json/security/403ForbiddenResponse.json", getClass());

        mockMvc.perform(delete("/api/certificates/25"))
               .andExpect(status().isForbidden())
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteNonExistingGiftCert_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[DELETE]NonExistingGiftCertificateResponse.json",
                                                getClass());

        mockMvc.perform(delete("/api/certificates/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteUsedGiftCertificate_thenConflict() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[DELETE]UsedGiftCertificateResponse.json", getClass());

        mockMvc.perform(delete("/api/certificates/7"))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    private static Stream<Arguments> provideGiftCertificateSearchParamsAndResult() {
        return Stream.of(Arguments.of("/api/certificates?tags=tag2,tag3&searchText=1&sortByName=ASC&sortByDate=DESC",
                                      "[GET]AllParametersSearchResponse.json"),
                         Arguments.of("/api/certificates?page.size=5&page.afterId=5&page.direction=FORWARD",
                                      "[GET]NoParamsSearchNextPageResponse.json"),
                         Arguments.of("/api/certificates?page.size=5", "[GET]NoParamsSearchFirstPageResponse.json"),
                         Arguments.of("/api/certificates?page.size=5&page.afterId=45&page.direction=FORWARD",
                                      "[GET]NoParamsSearchLastPageResponse.json"),
                         Arguments.of("/api/certificates?page.size=5&page.afterId=46&page.direction=BACK",
                                      "[GET]NoParamsSearchPreviousPageResponse.json"),
                         Arguments.of("/api/certificates?tags=tag1", "[GET]TagOnlySearchResponse.json"),
                         Arguments.of("/api/certificates?searchText=3", "[GET]TextOnlySearchResponse.json"),
                         Arguments.of("/api/certificates?sortByName=DESC&page.size=5",
                                      "[GET]SortByNameOnlySearchResponse.json"),
                         Arguments.of("/api/certificates?sortByDate=ASC&page.size=5",
                                      "[GET]SortByDateOnlySearchResponse.json"),
                         Arguments.of("/api/certificates?tags=tag7&searchText=1", "[GET]TagAndTextSearchResponse.json"),
                         Arguments.of("/api/certificates?tags=tag7&searchText=5", "[GET]EmptyResultResponse.json"),
                         Arguments.of("/api/certificates", "[GET]NoParamsSearchResponse.json"));
    }
}
