package com.epam.esm.web;

import static com.epam.esm.utils.TestUtils.getBaseUrl;
import static com.epam.esm.utils.TestUtils.getJsonString;
import static com.epam.esm.utils.TestUtils.getUpdateBaseURL;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.service.SecurityService;
import com.epam.esm.utils.IsValidTokenMatcher;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.KeyPair;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {

    private static final String JSON_PATH = "json/user/";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private KeyPair keyPair;

    @Autowired
    private IsValidTokenMatcher tokenMatcher;

    @ParameterizedTest
    @MethodSource("provideUserPageParamsAndResult")
    @WithMockUser(roles = "ADMIN")
    void getUsersListByAdmin_thenOk(String URL, String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser
    void getUsersListByUser_thenForbidden() throws Exception {
        String responseBodyJson = getJsonString("json/security/403ForbiddenResponse.json", getClass());

        mockMvc.perform(get("/api/users"))
               .andExpect(status().isForbidden())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getUsersListNoAuth_thenUnauthorised() throws Exception {
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(get("/api/users"))
               .andExpect(status().isUnauthorized())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getExistingUserByAdmin_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingUserResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);

        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get("/api/users/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithUserDetails("user1")
    void getExistingUserByOwnerUser_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingUserResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);

        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get("/api/users/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingUserByOwnerUserWithTokenAuth_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingUserResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        String token = Jwts.builder()
                           .setIssuer("https://esm.epam.com")
                           .setSubject("user1")
                           .claim("authorities", List.of(new SimpleGrantedAuthority("ROLE_USER")))
                           .setIssuedAt(new Date())
                           .setExpiration(new Date((new Date()).getTime() + 60000))
                           .signWith(keyPair.getPrivate(), SignatureAlgorithm.RS256)
                           .compact();

        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        mockMvc.perform(get("/api/users/1").header("Authorization", "Bearer " + token))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithUserDetails("user2")
    void getExistingUserByOtherUser_thenForbidden() throws Exception {
        String responseBodyJson = getJsonString("json/security/403ForbiddenResponse.json", getClass());

        mockMvc.perform(get("/api/users/1"))
               .andExpect(status().isForbidden())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingUserNoAuth_thenUnauthorised() throws Exception {
        String responseBodyJson = getJsonString("json/security/401UnauthorizedResponse.json", getClass());

        mockMvc.perform(get("/api/users/1"))
               .andExpect(status().isUnauthorized())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getNonExistingUser_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingUserResponse.json", getClass());

        mockMvc.perform(get("/api/users/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    @DirtiesContext
    void postValidUser_thenCreated() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidUserRequest.json", getClass());

        mockMvc.perform(post("/api/users").content(requestBodyJson).contentType(APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.access_token").value(tokenMatcher));
    }

    @Test
    void postValidExistingUser_thenConflict() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingUserRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingUserResponse.json", getClass());

        mockMvc.perform(post("/api/users").content(requestBodyJson).contentType(APPLICATION_JSON))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    private static Stream<Arguments> provideUserPageParamsAndResult() {
        return Stream.of(Arguments.of("/api/users?page.size=5&page.afterId=5&page.direction=FORWARD",
                                      "[GET]NextPageResponse.json"),
                         Arguments.of("/api/users?page.size=5", "[GET]FirstPageResponse.json"),
                         Arguments.of("/api/users?page.size=5&page.afterId=45&page.direction=FORWARD",
                                      "[GET]LastPageResponse.json"),
                         Arguments.of("/api/users?page.size=5&page.afterId=46&page.direction=BACK",
                                      "[GET]PreviousPageResponse.json"),
                         Arguments.of("/api/users", "[GET]NoPageParamsResponse.json"));
    }

}
