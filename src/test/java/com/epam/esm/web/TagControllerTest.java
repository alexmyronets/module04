package com.epam.esm.web;

import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.EMPTY_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.FIRST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.LAST_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.NEXT_PAGING;
import static com.epam.esm.DTOConstants.PagingParamsDTOConstants.PREV_PAGING;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.EMPTY_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.FIRST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.LAST_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.NEXT_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.SearchParametersDTOConstants.PREV_PAGE_FILTER_PARAMS;
import static com.epam.esm.DTOConstants.TagDTOConstants.SAVED_TAG_DTO;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_1_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_1_TO_5_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_41_TO_55_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_46_TO_50_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_6_TO_10_DTO_LIST;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_1;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_3;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_EXISTS_TO_CREATE;
import static com.epam.esm.DTOConstants.TagDTOConstants.TAG_DTO_TO_SAVE;
import static com.epam.esm.utils.TestUtils.getBaseUrl;
import static com.epam.esm.utils.TestUtils.getJsonString;
import static com.epam.esm.utils.TestUtils.getUpdateBaseURL;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.TagDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchTagException;
import com.epam.esm.exception.TagAlreadyExistsException;
import com.epam.esm.exception.TagIsInUseException;
import com.epam.esm.service.TagService;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(TagController.class)
@AutoConfigureMockMvc(addFilters = false)
class TagControllerTest {

    private static final String JSON_PATH = "json/tag/";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TagService tagService;

    @Test
    void postValidTag_thenCreated() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidTagRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidTagResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        when(tagService.create(TAG_DTO_TO_SAVE)).thenReturn(SAVED_TAG_DTO);

        mockMvc.perform(post("/api/tags").content(requestBodyJson)
                                         .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidJsonFormat_thenBadRequest() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]InvalidJsonFormatTagRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]InvalidJsonFormatTagResponse.json", getClass());

        mockMvc.perform(post("/api/tags").content(requestBodyJson)
                                         .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidTag_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]InvalidDataTagRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]InvalidDataTagResponse.json", getClass());

        mockMvc.perform(post("/api/tags").content(requestBodyJson)
                                         .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postExistingTag_thenConflict() throws Exception {
        String requestBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingTagRequest.json", getClass());
        String responseBodyJson = getJsonString(JSON_PATH + "[POST]ValidExistingTagResponse.json", getClass());

        when(tagService.create(TAG_DTO_EXISTS_TO_CREATE)).thenThrow(new TagAlreadyExistsException(
            "Tag (name = tag5) is already exists"));

        mockMvc.perform(post("/api/tags").content(requestBodyJson)
                                         .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingTag_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]ExistingTagResponse.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        when(tagService.read(1)).thenReturn(TAG_DTO_1.removeLinks());

        mockMvc.perform(get("/api/tags/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @ParameterizedTest
    @MethodSource("provideTagPageParamsAndResult")
    void getTagsList_thenOk(String URL, FilterParams filterParams,
                            List<TagDTO> tagDTOlist, PagingParamsDTO pagingParams,
                            String responseJsonFile) throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + responseJsonFile, getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        tagDTOlist.forEach(RepresentationModel::removeLinks);

        when(tagService.list(filterParams)).thenReturn(tagDTOlist);
        when(tagService.getPagingParams(filterParams,
                                        tagDTOlist.get(tagDTOlist.size() - 1).getId(),
                                        tagDTOlist.get(0).getId())).thenReturn(pagingParams);

        mockMvc.perform(get(URL))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]NonExistingTagResponse.json", getClass());

        when(tagService.read(442)).thenThrow(new NoSuchTagException("Requested resource not found (id = 442)"));

        mockMvc.perform(get("/api/tags/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteNotUsedTag_thenNoContent() throws Exception {
        mockMvc.perform(delete("/api/tags/11"))
               .andExpect(status().isNoContent());
        verify(tagService, times(1)).delete(11);
    }

    @Test
    void deleteUsedTag_thenConflict() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[DELETE]UsedTagResponse.json", getClass());

        doThrow(new TagIsInUseException("Tag (id = 5) is in use by 6 gift certificates")).when(tagService)
                                                                                         .delete(5);

        mockMvc.perform(delete("/api/tags/5"))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteNotExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[DELETE]NonExistingTagResponse.json", getClass());

        doThrow(new NoSuchTagException("Requested resource not found (id = 442)")).when(tagService)
                                                                                  .delete(442);

        mockMvc.perform(delete("/api/tags/442"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getMostUsedTagByMostSpentUserOrders_thenOk() throws Exception {
        String responseBodyJson = getJsonString(JSON_PATH + "[GET]MostUsedTagByMostSpentUserOrders.json", getClass());
        String baseUrl = getBaseUrl(this.mockMvc);
        if (!baseUrl.equals("http://localhost:8080")) {
            responseBodyJson = getUpdateBaseURL(baseUrl, responseBodyJson);
        }

        when(tagService.readMostUsedTagByMostSpendUser()).thenReturn(TAG_DTO_3);

        mockMvc.perform(get("/api/tags/most_used_tag_by_most_spent_user"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(HAL_JSON_VALUE)).andExpect(content().json(responseBodyJson));

    }

    private static Stream<Arguments> provideTagPageParamsAndResult() {
        return Stream.of(Arguments.of("/api/tags?page.size=5&page.afterId=5&page.direction=FORWARD",
                                      NEXT_PAGE_FILTER_PARAMS,
                                      TAG_6_TO_10_DTO_LIST,
                                      NEXT_PAGING,
                                      "[GET]NextPageResponse.json"),
                         Arguments.of("/api/tags?page.size=5",
                                      FIRST_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_5_DTO_LIST,
                                      FIRST_PAGING,
                                      "[GET]FirstPageResponse.json"),
                         Arguments.of("/api/tags?page.size=5&page.afterId=45&page.direction=FORWARD",
                                      LAST_PAGE_FILTER_PARAMS,
                                      TAG_46_TO_50_DTO_LIST,
                                      LAST_PAGING,
                                      "[GET]LastPageResponse.json"),
                         Arguments.of("/api/tags?page.size=5&page.afterId=46&page.direction=BACK",
                                      PREV_PAGE_FILTER_PARAMS,
                                      TAG_41_TO_55_DTO_LIST,
                                      PREV_PAGING,
                                      "[GET]PreviousPageResponse.json"),
                         Arguments.of("/api/tags",
                                      EMPTY_PAGE_FILTER_PARAMS,
                                      TAG_1_TO_50_DTO_LIST,
                                      EMPTY_PAGING,
                                      "[GET]NoPageParamsResponse.json"));
    }

}