package com.epam.esm.DTOConstants;

import static com.epam.esm.DTOConstants.PageDTOConstants.FIRST_PAGE_DTO;
import static com.epam.esm.DTOConstants.PageDTOConstants.LAST_PAGE_DTO;
import static com.epam.esm.DTOConstants.PageDTOConstants.NEXT_PAGE_DTO;
import static com.epam.esm.DTOConstants.PageDTOConstants.ORDER_FIRST_PAGE_DTO;
import static com.epam.esm.DTOConstants.PageDTOConstants.ORDER_LAST_PAGE_DTO;
import static com.epam.esm.DTOConstants.PageDTOConstants.ORDER_NEXT_PAGE_DTO;
import static com.epam.esm.DTOConstants.PageDTOConstants.ORDER_PREV_PAGE_DTO;
import static com.epam.esm.DTOConstants.PageDTOConstants.PREV_PAGE_DTO;

import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.GiftCertificateFilterParams;
import com.epam.esm.dto.OrderFilterParams;
import com.epam.esm.domain.SortOrder;
import com.epam.esm.dto.TagDTO;
import java.util.List;

public abstract class SearchParametersDTOConstants {

    public static final GiftCertificateFilterParams ALL_PARAMETERS_SEARCH_DTO = new GiftCertificateFilterParams(
        List.of(new TagDTO(null, "tag2"), new TagDTO(null, "tag3")),
        "1",
        SortOrder.ASC,
        SortOrder.DESC);
    public static final GiftCertificateFilterParams NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO = new GiftCertificateFilterParams();
    public static final GiftCertificateFilterParams NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO = new GiftCertificateFilterParams();
    public static final GiftCertificateFilterParams NO_PARAMETERS_SEARCH_LAST_PAGE_DTO = new GiftCertificateFilterParams();
    public static final GiftCertificateFilterParams NO_PARAMETERS_SEARCH_PREV_PAGE_DTO = new GiftCertificateFilterParams();

    public static final GiftCertificateFilterParams TAG_ONLY_SEARCH_DTO = new GiftCertificateFilterParams(List.of(new TagDTO(
        null,
        "tag1")), null, null, null);
    public static final GiftCertificateFilterParams TEXT_ONLY_SEARCH_DTO = new GiftCertificateFilterParams(List.of(),
                                                                                                           "3",
                                                                                                           null,
                                                                                                           null);
    public static final GiftCertificateFilterParams SORT_BY_NAME_ONLY_SEARCH_DTO = new GiftCertificateFilterParams(
        List.of(),
        null,
        SortOrder.DESC,
        null);
    public static final GiftCertificateFilterParams SORT_BY_DATE_ONLY_SEARCH_DTO = new GiftCertificateFilterParams(
        List.of(),
        null,
        null,
        SortOrder.ASC);
    public static final GiftCertificateFilterParams TAG_AND_TEXT_SEARCH_DTO = new GiftCertificateFilterParams(List.of(
        new TagDTO(
            null,
            "tag7")), "1", null, null);
    public static final GiftCertificateFilterParams TAG_AND_TEXT_EMPTY_SEARCH_DTO = new GiftCertificateFilterParams(List.of(
        new TagDTO(
            null,
            "tag7")), "5", null, null);
    public static final GiftCertificateFilterParams EMPTY_SEARCH_DTO = new GiftCertificateFilterParams(List.of(),
                                                                                                       null,
                                                                                                       null,
                                                                                                       null);
    public static final FilterParams NEXT_PAGE_FILTER_PARAMS = new FilterParams(NEXT_PAGE_DTO);
    public static final FilterParams FIRST_PAGE_FILTER_PARAMS = new FilterParams(FIRST_PAGE_DTO);
    public static final FilterParams LAST_PAGE_FILTER_PARAMS = new FilterParams(LAST_PAGE_DTO);
    public static final FilterParams PREV_PAGE_FILTER_PARAMS = new FilterParams(PREV_PAGE_DTO);
    public static final FilterParams EMPTY_PAGE_FILTER_PARAMS = new FilterParams();


    public static long USER_ID = 17;
    public static final OrderFilterParams ORDER_NEXT_PAGE_FILTER_PARAMS = new OrderFilterParams(USER_ID);
    public static final OrderFilterParams ORDER_FIRST_PAGE_FILTER_PARAMS = new OrderFilterParams(USER_ID);
    public static final OrderFilterParams ORDER_LAST_PAGE_FILTER_PARAMS = new OrderFilterParams(USER_ID);
    public static final OrderFilterParams ORDER_PREV_PAGE_FILTER_PARAMS = new OrderFilterParams(USER_ID);
    public static final OrderFilterParams ORDER_EMPTY_PAGE_FILTER_PARAMS = new OrderFilterParams(USER_ID);

    static {
        NO_PARAMETERS_SEARCH_NEXT_PAGE_DTO.setPage(NEXT_PAGE_DTO);
        NO_PARAMETERS_SEARCH_FIRST_PAGE_DTO.setPage(FIRST_PAGE_DTO);
        NO_PARAMETERS_SEARCH_LAST_PAGE_DTO.setPage(LAST_PAGE_DTO);
        NO_PARAMETERS_SEARCH_PREV_PAGE_DTO.setPage(PREV_PAGE_DTO);
        SORT_BY_NAME_ONLY_SEARCH_DTO.setPage(FIRST_PAGE_DTO);
        SORT_BY_DATE_ONLY_SEARCH_DTO.setPage(FIRST_PAGE_DTO);

        ORDER_NEXT_PAGE_FILTER_PARAMS.setPage(ORDER_NEXT_PAGE_DTO);
        ORDER_FIRST_PAGE_FILTER_PARAMS.setPage(ORDER_FIRST_PAGE_DTO);
        ORDER_LAST_PAGE_FILTER_PARAMS.setPage(ORDER_LAST_PAGE_DTO);
        ORDER_PREV_PAGE_FILTER_PARAMS.setPage(ORDER_PREV_PAGE_DTO);
    }

    private SearchParametersDTOConstants() {

    }


}
