package com.epam.esm.DTOConstants;

import com.epam.esm.dto.paging.PageDTO;
import com.epam.esm.dto.paging.PageDirection;

public abstract class PageDTOConstants {

    public static final PageDTO NEXT_PAGE_DTO = new PageDTO((byte) 5, 5L, PageDirection.FORWARD);
    public static final PageDTO FIRST_PAGE_DTO = new PageDTO((byte) 5, null, PageDirection.FORWARD);
    public static final PageDTO LAST_PAGE_DTO = new PageDTO((byte) 5, 45L, PageDirection.FORWARD);
    public static final PageDTO PREV_PAGE_DTO = new PageDTO((byte) 5, 46L, PageDirection.BACK);

    public static final PageDTO ORDER_NEXT_PAGE_DTO = new PageDTO((byte) 5, 97L, PageDirection.FORWARD);
    public static final PageDTO ORDER_FIRST_PAGE_DTO = new PageDTO((byte) 5, null, PageDirection.FORWARD);
    public static final PageDTO ORDER_LAST_PAGE_DTO = new PageDTO((byte) 5, 98L, PageDirection.FORWARD);
    public static final PageDTO ORDER_PREV_PAGE_DTO = new PageDTO((byte) 5, 99L, PageDirection.BACK);

    private PageDTOConstants() {
    }
}
