package com.epam.esm.EntityConstants;

import java.util.List;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public abstract class AuthenticationConstants {

    public static final Authentication AUTH_CANDIDATE
        = new UsernamePasswordAuthenticationToken("user51",
                                                  "Password51");
    public static final Authentication AUTH_TRUSTED
        = new UsernamePasswordAuthenticationToken("user51",
                                                  null,
                                                  List.of(new SimpleGrantedAuthority(
                                                      "ROLE_USER")));

    public static final String JWT_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwczovL2VzbS5lcGFtLmNvbSIsInN1YiI6InVzZXI1MSIsImF1dGhvcml0aWVzIjpbIlJPTEVfVVNFUiJdLCJpYXQiOjE2ODE4MDUyNjAsImV4cCI6MTY4MTgwNTg2MH0.kJcBcagXN8R2BvHIf02MItsHFW6DYXr47RjWuSFHnXTM7u9S7hAr2XyOHoahKrOeXbrre6eXQUqWfnFDTb1QBttjh-5mr8v-mI5rzIQqyfND3lWF3uSi-_8e3jnPI-0FoauRuF8FPBRoZ1rBX92nCgezW5GntiT9P7TVkASuCdmOBmc5JLGWx55vS0o65alIBnAAdO7xVCgDv0x90YTYHVJLnaFTnQaXR2MLFQVfay6lSaYrNSVZLBnyh94-yM3P0Q-YLiJT3HeYsoGNvgGzs-yFtJmKPLp3Pg_3ePaE3q_-IJR-PfbQFBgAhLCSOet7Mu85Vx4y4NrEMkWVqh-BGQ";


    private AuthenticationConstants() {
    }
}
