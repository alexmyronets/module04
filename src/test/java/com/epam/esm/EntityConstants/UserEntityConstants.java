package com.epam.esm.EntityConstants;


import com.epam.esm.domain.Role;
import com.epam.esm.domain.User;
import com.epam.esm.domain.JwtProvider;
import java.util.List;

public abstract class UserEntityConstants {

    public static final User USER_ENTITY_1 = new User(1L, "user1", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_2 = new User(2L, "user2", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_3 = new User(3L, "user3", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_4 = new User(4L, "user4", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_5 = new User(5L, "user5", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_6 = new User(6L, "user6", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_7 = new User(7L, "user7", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_8 = new User(8L, "user8", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_9 = new User(9L, "user9", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_10 = new User(10L, "user10", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_11 = new User(11L, "user11", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_12 = new User(12L, "user12", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_13 = new User(13L, "user13", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_14 = new User(14L, "user14", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_15 = new User(15L, "user15", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_16 = new User(16L, "user16", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_17 = new User(17L, "user17", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_18 = new User(18L, "user18", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_19 = new User(19L, "user19", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_20 = new User(20L, "user20", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_21 = new User(21L, "user21", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_22 = new User(22L, "user22", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_23 = new User(23L, "user23", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_24 = new User(24L, "user24", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_25 = new User(25L, "user25", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_26 = new User(26L, "user26", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_27 = new User(27L, "user27", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_28 = new User(28L, "user28", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_29 = new User(29L, "user29", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_30 = new User(30L, "user30", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_31 = new User(31L, "user31", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_32 = new User(32L, "user32", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_33 = new User(33L, "user33", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_34 = new User(34L, "user34", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_35 = new User(35L, "user35", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_36 = new User(36L, "user36", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_37 = new User(37L, "user37", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_38 = new User(38L, "user38", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_39 = new User(39L, "user39", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_40 = new User(40L, "user40", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_41 = new User(41L, "user41", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_42 = new User(42L, "user42", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_43 = new User(43L, "user43", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_44 = new User(44L, "user44", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_45 = new User(45L, "user45", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_46 = new User(46L, "user46", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_47 = new User(47L, "user47", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_48 = new User(48L, "user48", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_49 = new User(49L, "user49", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_50 = new User(50L, "user50", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User USER_ENTITY_51 = new User(51L, "user51", null, Role.USER, JwtProvider.LOCAL, null);
    public static final User EXISTING_USER_TO_SAVE = new User(null,
                                                              "user11",
                                                              "$2a$10$dBuqwRrcWTgLYWAG9GrlYeqr.JRXNQzgOGVxWHZ4bXoFeXqNR47tq",
                                                              null,
                                                              JwtProvider.LOCAL,
                                                              null);

    public static List<User> USER_6_TO_10_ENTITY_LIST
        = List.of(USER_ENTITY_6,
                  USER_ENTITY_7,
                  USER_ENTITY_8,
                  USER_ENTITY_9,
                  USER_ENTITY_10);

    public static List<User> USER_1_TO_5_ENTITY_LIST
        = List.of(USER_ENTITY_1,
                  USER_ENTITY_2,
                  USER_ENTITY_3,
                  USER_ENTITY_4,
                  USER_ENTITY_5);

    public static List<User> USER_46_TO_50_ENTITY_LIST
        = List.of(USER_ENTITY_46,
                  USER_ENTITY_47,
                  USER_ENTITY_48,
                  USER_ENTITY_49,
                  USER_ENTITY_50);

    public static List<User> USER_41_TO_45_ENTITY_LIST
        = List.of(USER_ENTITY_41,
                  USER_ENTITY_42,
                  USER_ENTITY_43,
                  USER_ENTITY_44,
                  USER_ENTITY_45);

    public static List<User> USER_1_TO_50_ENTITY_LIST
        = List.of(USER_ENTITY_1,
                  USER_ENTITY_2,
                  USER_ENTITY_3,
                  USER_ENTITY_4,
                  USER_ENTITY_5,
                  USER_ENTITY_6,
                  USER_ENTITY_7,
                  USER_ENTITY_8,
                  USER_ENTITY_9,
                  USER_ENTITY_10,
                  USER_ENTITY_11,
                  USER_ENTITY_12,
                  USER_ENTITY_13,
                  USER_ENTITY_14,
                  USER_ENTITY_15,
                  USER_ENTITY_16,
                  USER_ENTITY_17,
                  USER_ENTITY_18,
                  USER_ENTITY_19,
                  USER_ENTITY_20,
                  USER_ENTITY_21,
                  USER_ENTITY_22,
                  USER_ENTITY_23,
                  USER_ENTITY_24,
                  USER_ENTITY_25,
                  USER_ENTITY_26,
                  USER_ENTITY_27,
                  USER_ENTITY_28,
                  USER_ENTITY_29,
                  USER_ENTITY_30,
                  USER_ENTITY_31,
                  USER_ENTITY_32,
                  USER_ENTITY_33,
                  USER_ENTITY_34,
                  USER_ENTITY_35,
                  USER_ENTITY_36,
                  USER_ENTITY_37,
                  USER_ENTITY_38,
                  USER_ENTITY_39,
                  USER_ENTITY_40,
                  USER_ENTITY_41,
                  USER_ENTITY_42,
                  USER_ENTITY_43,
                  USER_ENTITY_44,
                  USER_ENTITY_45,
                  USER_ENTITY_46,
                  USER_ENTITY_47,
                  USER_ENTITY_48,
                  USER_ENTITY_49,
                  USER_ENTITY_50);

    public static User getUserToSave() {
        return new User(null,
                        "user51",
                        "$2a$10$dBuqwRrcWTgLYWAG9GrlYeqr.JRXNQzgOGVxWHZ4bXoFeXqNR47tq",
                        null,
                        JwtProvider.LOCAL,
                        null);
    }

    private UserEntityConstants() {
    }
}
