package com.epam.esm.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;
import java.io.IOException;
import java.security.KeyPair;
import java.util.Objects;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public abstract class TestUtils {

    private TestUtils() {
    }

    public static String getJsonString(String path, Class<?> clazz) throws IOException {
        return new String(Objects.requireNonNull(clazz
                                                     .getClassLoader()
                                                     .getResourceAsStream(path))
                                 .readAllBytes());
    }

    public static String getUpdateBaseURL(String updateURL, String jsonBody) {
        return jsonBody.replaceAll("http://localhost:8080", updateURL);
    }

    public static String getBaseUrl(MockMvc mockMvc) {
        return MockMvcRequestBuilders.post("")
                                     .buildRequest(mockMvc.getDispatcherServlet()
                                                          .getServletContext())
                                     .getRequestURL()
                                     .toString();
    }

    public static boolean isTokenValid(KeyPair keyPair, String jwt) {
        try {
            Claims claims = Jwts.parserBuilder()
                                .setSigningKey(keyPair.getPublic())
                                .build()
                                .parseClaimsJws(jwt)
                                .getBody();
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
