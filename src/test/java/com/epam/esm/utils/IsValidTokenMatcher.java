package com.epam.esm.utils;

import static com.epam.esm.utils.TestUtils.isTokenValid;

import java.security.KeyPair;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.springframework.stereotype.Component;

@Component
public class IsValidTokenMatcher extends BaseMatcher<String> {

    private final KeyPair keyPair;

    public IsValidTokenMatcher(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    @Override
    public boolean matches(Object item) {
        if (item instanceof String accessToken) {
            return isTokenValid(keyPair, accessToken);
        }
        return false;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a valid access token");
    }
}
