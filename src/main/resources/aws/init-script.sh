#!/bin/bash
sudo yum update -y
sudo yum install -y java-17-amazon-corretto-headless
sudo aws s3 cp s3://module05/module04.jar /home/ec2-user/
sudo chown ec2-user:ec2-user /home/ec2-user/module04.jar

# Create the systemd service file
sudo bash -c "cat > /etc/systemd/system/module05.service" << 'EOF'
[Unit]
Description=Module05
After=network.target

[Service]
User=ec2-user
WorkingDirectory=/home/ec2-user
ExecStart=/usr/bin/java -jar /home/ec2-user/module04.jar
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

# Reload the systemd configuration, enable, and start the service
sudo systemctl daemon-reload
sudo systemctl enable module05.service
sudo systemctl start module05.service
