CREATE TABLE IF NOT EXISTS gift_certificate
(
id BIGSERIAL PRIMARY KEY,
name VARCHAR NOT NULL,
description VARCHAR NOT NULL,
price DECIMAL(10,2) NOT NULL,
duration SMALLINT NOT NULL,
create_date TIMESTAMP NOT NULL,
last_update_date TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS tag
(
id BIGSERIAL PRIMARY KEY,
name VARCHAR UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS gift_certificate_tag
(
gift_certificate_id BIGINT REFERENCES gift_certificate (id) ON DELETE CASCADE,
tag_id BIGINT REFERENCES tag (id) ON DELETE RESTRICT,
CONSTRAINT gift_certificate_tag_pkey PRIMARY KEY (gift_certificate_id, tag_id)
);

CREATE TABLE IF NOT EXISTS users (
	id BIGSERIAL PRIMARY KEY,
	username VARCHAR (50) UNIQUE NOT NULL,
	password VARCHAR,
	roles VARCHAR NOT NULL,
	provider VARCHAR NOT NULL,
	provider_id VARCHAR,
	CONSTRAINT oauth_user_id UNIQUE (provider, provider_id),
	CHECK (
        (CASE WHEN password IS NOT NULL THEN 1 ELSE 0 END) +
        (CASE WHEN provider_id IS NOT NULL THEN 1 ELSE 0 END)
        = 1
    )
);

CREATE TABLE IF NOT EXISTS orders (
  id BIGSERIAL PRIMARY KEY,
  user_id BIGINT NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  gift_certificate_id BIGINT NOT NULL REFERENCES gift_certificate (id) ON DELETE RESTRICT,
  order_date TIMESTAMP NOT NULL,
  order_cost DECIMAL(10,2) NOT NULL
)
