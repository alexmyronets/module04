package com.epam.esm;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class Module04Application {

    private static final Logger LOGGER = LogManager.getLogger(Module04Application.class);

    public static void main(String[] args) {
        LOGGER.info("Starting app");
        SpringApplication.run(Module04Application.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Component
    public static class StartupApplicationListener {

        @Value("${app.buildNumber}")
        private String build;

        @Value("${app.version}")
        private String version;

        @EventListener
        public void onApplicationEvent(ContextRefreshedEvent event) {
            LOGGER.info("App version: {}.b{}", version, build);
        }
    }
}
