package com.epam.esm.domain;

public enum SortOrder {
    ASC, DESC
}
