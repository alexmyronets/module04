package com.epam.esm.domain;

public enum Role {
    USER,
    ADMIN
}
