package com.epam.esm.domain;

public enum JwtProvider {
    LOCAL, GOOGLE
}
