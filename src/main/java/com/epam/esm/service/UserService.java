package com.epam.esm.service;

import com.epam.esm.domain.User;
import com.epam.esm.dto.UserDTO;
import com.epam.esm.service.crud.CreateService;
import com.epam.esm.service.crud.ListPagingService;
import com.epam.esm.service.crud.ReadService;

/**
 * This interface consists of methods for mapping between {@link UserDTO} and {@link User} and provides pagination
 * metadata for list operation.
 *
 * @author Oleksandr Myronets.
 */
public interface UserService extends ReadService<UserDTO>, ListPagingService<UserDTO>, CreateService<UserDTO> {

}
