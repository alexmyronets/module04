package com.epam.esm.service;

import com.epam.esm.domain.Order;
import com.epam.esm.dto.OrderDTO;
import com.epam.esm.service.crud.CreateService;
import com.epam.esm.service.crud.ListPagingService;

/**
 * This interface consists of methods for mapping between {@link OrderDTO} and {@link Order} and provides pagination
 * metadata for list operation.
 *
 * @author Oleksandr Myronets.
 */
public interface OrderService extends CreateService<OrderDTO>, ListPagingService<OrderDTO> {

    OrderDTO read(Long userId, Long orderId);

}
