package com.epam.esm.service.authentication;

import com.epam.esm.domain.JwtProvider;
import com.epam.esm.exception.InvalidTokenException;
import com.epam.esm.service.JpaUserDetailsService;
import com.epam.esm.service.SecurityService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class GoogleOAuth2AuthenticationProviderService implements AuthenticationProvider {

    private static final Logger LOGGER = LogManager.getLogger(GoogleOAuth2AuthenticationProviderService.class);

    private final SecurityService securityService;

    private final JpaUserDetailsService jpaUserDetailsService;

    public GoogleOAuth2AuthenticationProviderService(SecurityService securityService,
                                                     JpaUserDetailsService jpaUserDetailsService) {
        this.securityService = securityService;
        this.jpaUserDetailsService = jpaUserDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.info("Authenticating user using Google OAuth2");
        BearerTokenAuthenticationToken bearerTokenAuthenticationToken = (BearerTokenAuthenticationToken) authentication;
        String jwt = bearerTokenAuthenticationToken.getToken();

        String issuer = securityService.getIssuerWithoutValidation(jwt);
        if (!"https://accounts.google.com".equals(issuer)) {
            LOGGER.warn("Issuer is not Google, returning null");
            return null;
        }

        GoogleIdToken token = null;
        try {
            token = securityService.validateGoogleOauthToken(jwt);
            LOGGER.info("Google OAuth token validation successful");
        } catch (InvalidTokenException e) {
            LOGGER.error("Invalid Google OAuth token: {}", e.getMessage());
            throw new BadCredentialsException(e.getMessage(), e);
        }

        UserDetails user = jpaUserDetailsService.loadUserByProviderId(JwtProvider.GOOGLE,
                                                                      token.getPayload().getSubject());
        LOGGER.info("User authenticated successfully using Google OAuth2");
        return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        LOGGER.debug("Checking if GoogleOAuth2AuthenticationProviderService supports authentication class");
        return BearerTokenAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
