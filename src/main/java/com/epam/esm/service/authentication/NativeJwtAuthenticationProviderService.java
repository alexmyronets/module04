package com.epam.esm.service.authentication;

import com.epam.esm.exception.InvalidTokenException;
import com.epam.esm.service.JpaUserDetailsService;
import com.epam.esm.service.SecurityService;
import io.jsonwebtoken.Claims;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class NativeJwtAuthenticationProviderService implements AuthenticationProvider {

    private static final Logger LOGGER = LogManager.getLogger(NativeJwtAuthenticationProviderService.class);

    private final SecurityService securityService;

    private final JpaUserDetailsService jpaUserDetailsService;

    public NativeJwtAuthenticationProviderService(SecurityService securityService,
                                                  JpaUserDetailsService jpaUserDetailsService) {
        this.securityService = securityService;
        this.jpaUserDetailsService = jpaUserDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.info("Authenticating user using Native JWT");
        BearerTokenAuthenticationToken bearerTokenAuthenticationToken = (BearerTokenAuthenticationToken) authentication;
        String jwt = bearerTokenAuthenticationToken.getToken();

        if (!securityService.getIssuerWithoutValidation(jwt).equals("https://esm.epam.com")) {
            LOGGER.warn("Issuer is not ESM, returning null");
            return null;
        }

        Claims claims = null;
        try {
            claims = securityService.getClaims(jwt);
            LOGGER.info("Native JWT token validation successful");
        } catch (InvalidTokenException e) {
            LOGGER.error("Invalid Native JWT token: {}", e.getMessage());
            throw new BadCredentialsException(e.getMessage(), e);
        }

        String userName = claims.getSubject();
        UserDetails user = jpaUserDetailsService.loadUserByUsername(userName);
        LOGGER.info("User authenticated successfully using Native JWT");
        return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return BearerTokenAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
