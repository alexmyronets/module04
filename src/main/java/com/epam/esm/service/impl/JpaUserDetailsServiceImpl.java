package com.epam.esm.service.impl;

import com.epam.esm.data.UserRepository;
import com.epam.esm.domain.CustomUserDetails;
import com.epam.esm.domain.JwtProvider;
import com.epam.esm.domain.User;
import com.epam.esm.exception.NoSuchUserException;
import com.epam.esm.service.JpaUserDetailsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JpaUserDetailsServiceImpl implements JpaUserDetailsService {

    private static final Logger LOGGER = LogManager.getLogger(JpaUserDetailsServiceImpl.class);

    private final UserRepository userRepository;

    public JpaUserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.info("Loading user by username: {}", username);
        User user;
        try {
            user = userRepository.findByUserName(username);
        } catch (NoSuchUserException e) {
            LOGGER.error("User not found during authentication: {}", username, e);
            throw new UsernameNotFoundException("User not found during authentication", e);
        }
        LOGGER.info("User loaded successfully: {}", username);
        return new CustomUserDetails(user);
    }

    @Override
    public UserDetails loadUserByProviderId(JwtProvider provider, String providerId) {
        LOGGER.info("Loading user by providerId: {} - {}", provider, providerId);
        User user;
        try {
            user = userRepository.findByProviderId(provider, providerId);
        } catch (NoSuchUserException e) {
            LOGGER.error("User not found during authentication: {} - {}", provider, providerId, e);
            throw new UsernameNotFoundException("User not found during authentication", e);
        }
        LOGGER.info("User loaded successfully: {} - {}", provider, providerId);
        return new CustomUserDetails(user);
    }
}
