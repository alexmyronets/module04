package com.epam.esm.service.impl;

import com.epam.esm.data.TagRepository;
import com.epam.esm.domain.Tag;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.TagDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.service.TagService;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

/**
 * @author Oleksandr Myronets.
 */
@Service
public class TagServiceImpl implements TagService {

    private static final Logger LOGGER = LogManager.getLogger(TagServiceImpl.class);

    private final ModelMapper modelMapper;

    private final TagRepository tagRepository;

    public TagServiceImpl(ModelMapper modelMapper, TagRepository tagRepository) {
        this.modelMapper = modelMapper;
        this.tagRepository = tagRepository;
    }

    @Override
    public TagDTO create(TagDTO tagDTO) {
        LOGGER.info("Create Tag method");
        LOGGER.debug("Mapping tagDTO: {} to Tag for creation", tagDTO);
        Tag tagToCreate = modelMapper.map(tagDTO, Tag.class);
        Tag createdTag = tagRepository.create(tagToCreate);

        LOGGER.debug("Mapping created Tag: {} to tagDTO", createdTag);
        TagDTO createdTagDTO = modelMapper.map(createdTag, TagDTO.class);
        LOGGER.info("Returning created Tag DTO: {}", createdTagDTO);
        return createdTagDTO;
    }

    @Override
    public TagDTO read(long id) {
        LOGGER.info("Read Tag method");
        Tag tag = tagRepository.read(id);

        LOGGER.debug("Mapping read Tag: {} to tagDTO", tag);
        TagDTO tagDTO = modelMapper.map(tag, TagDTO.class);
        LOGGER.debug("Returning mapped Tag DTO: {}", tagDTO);
        return tagDTO;
    }

    @Override
    public void delete(long id) {
        LOGGER.info("Delete Tag method");
        LOGGER.debug("Deleting Tag with id: {}", id);
        tagRepository.delete(id);
    }

    @Override
    public List<TagDTO> list(FilterParams filterParams) {
        LOGGER.info("List Tag method");
        LOGGER.debug("Getting List of Tags by parameters: {}", filterParams);
        List<Tag> tags = tagRepository.list(filterParams);

        LOGGER.debug("Mapping read Tags list to tagDTO list");
        List<TagDTO> tagDTOS = tags.stream()
                                   .map(tag -> modelMapper.map(tag, TagDTO.class))
                                   .toList();
        LOGGER.info("Mapped Tag DTO List: {}", tagDTOS);
        LOGGER.info("Returning Tag DTO List");
        return tagDTOS;
    }

    @Override
    public PagingParamsDTO getPagingParams(FilterParams filterParams, Long nextCandidate, Long prevCandidate) {
        return getPagingParams(filterParams, nextCandidate, prevCandidate, tagRepository);
    }

    @Override
    public TagDTO readMostUsedTagByMostSpendUser() {
        LOGGER.info("Get most used tag method");
        Tag tag = tagRepository.readMostUsedTagByMostSpendUser();

        LOGGER.debug("Mapping read Tag: {} to tagDTO", tag);
        TagDTO tagDTO = modelMapper.map(tag, TagDTO.class);
        LOGGER.info("Returning most used Tag: {}", tagDTO);
        return tagDTO;
    }
}
