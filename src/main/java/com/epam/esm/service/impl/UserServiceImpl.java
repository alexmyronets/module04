package com.epam.esm.service.impl;

import com.epam.esm.data.UserRepository;
import com.epam.esm.domain.User;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.UserDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.service.UserService;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

/**
 * @author Oleksandr Myronets.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    private final ModelMapper modelMapper;

    private final UserRepository userRepository;

    public UserServiceImpl(ModelMapper modelMapper, UserRepository userRepository) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
    }

    @Override
    public UserDTO read(long id) {
        LOGGER.info("Read User method");
        User user = userRepository.read(id);

        LOGGER.debug("Mapping read User: {} to UserDTO", user);
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        LOGGER.debug("Returning mapped User DTO: {}", userDTO);
        return userDTO;
    }

    @Override
    public List<UserDTO> list(FilterParams filterParams) {
        LOGGER.info("List User method");
        LOGGER.debug("Getting List of Users by parameters: {}", filterParams);
        List<User> users = userRepository.list(filterParams);

        LOGGER.debug("Mapping read Users list to UserDTO list");
        List<UserDTO> userDTOS = users.stream()
                                      .map(user -> modelMapper.map(user, UserDTO.class))
                                      .toList();
        LOGGER.info("Mapped User DTO List: {}", userDTOS);
        LOGGER.info("Returning User DTO List");
        return userDTOS;
    }

    @Override
    public PagingParamsDTO getPagingParams(FilterParams filterParams, Long nextCandidate, Long prevCandidate) {
        return getPagingParams(filterParams, nextCandidate, prevCandidate, userRepository);
    }

    @Override
    public UserDTO create(UserDTO userDTO) {

        User user = modelMapper.map(userDTO, User.class);
        User createdUser = userRepository.create(user);
        return modelMapper.map(createdUser, UserDTO.class);
    }
}
