package com.epam.esm.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.epam.esm.exception.InvalidTokenException;
import com.epam.esm.service.SecurityService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class SecurityServiceImpl implements SecurityService {

    private static final Logger LOGGER = LogManager.getLogger(SecurityServiceImpl.class);

    private final KeyPair keyPair;

    @Value("${jwt.expirationMs}")
    private int jwtExpirationMs;

    @Value("${spring.security.oauth2.client.registration.google.client-id}")
    private String clientId;

    public SecurityServiceImpl(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    @Override
    public String generateJwtToken(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        List<String> authorities = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();
        String jwt = Jwts.builder()
                         .setIssuer("https://esm.epam.com")
                         .setSubject(userDetails.getUsername())
                         .claim("authorities", authorities)
                         .setIssuedAt(new Date())
                         .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                         .signWith(keyPair.getPrivate(), SignatureAlgorithm.RS256)
                         .compact();
        LOGGER.info("Generated JWT token for user: {}", userDetails.getUsername());
        return jwt;
    }

    @Override
    public String generateJwtRefreshToken(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        long refreshTokenExpirationMs = 86400000L;
        String refreshToken = Jwts.builder()
                                  .setIssuer("https://esm.epam.com")
                                  .setSubject(userDetails.getUsername())
                                  .claim("type", "refresh_token")
                                  .setIssuedAt(new Date())
                                  .setExpiration(new Date((new Date()).getTime() + refreshTokenExpirationMs))
                                  .signWith(keyPair.getPrivate(), SignatureAlgorithm.RS256)
                                  .compact();
        LOGGER.info("Generated JWT refresh token for user: {}", userDetails.getUsername());
        return refreshToken;
    }

    @Override
    public GoogleIdToken validateGoogleOauthToken(String token) {
        GoogleIdToken result;
        try {
            GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(GoogleNetHttpTransport.newTrustedTransport(),
                                                                               new GsonFactory()).setAudience(
                Collections.singletonList(clientId)).build();

            result = verifier.verify(token);
        } catch (GeneralSecurityException | IOException e) {
            LOGGER.error("Token is invalid: {}", token);
            throw new InvalidTokenException("Token is invalid: " + token);
        }
        if (result == null) {
            LOGGER.error("Token is invalid: {}", token);
            throw new InvalidTokenException("Token is invalid: " + token);
        }
        LOGGER.info("Google OAuth token validated successfully");
        return result;
    }

    @Override
    public String getIssuerWithoutValidation(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            String issuer = jwt.getIssuer();
            LOGGER.info("Token issuer: {}", issuer);
            return issuer;
        } catch (JWTDecodeException e) {
            LOGGER.error("Token is invalid: {}", token);
            throw new InvalidTokenException("Token is invalid: " + token);
        }
    }

    @Override
    public Claims getClaims(String jwt) {
        try {
            Claims claims = Jwts.parserBuilder()
                                .setSigningKey(keyPair.getPublic())
                                .build()
                                .parseClaimsJws(jwt)
                                .getBody();
            LOGGER.info("Claims extracted successfully from JWT token");
            return claims;
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException |
                 IllegalArgumentException e) {
            LOGGER.error("Token is invalid: {}", jwt, e);
            throw new InvalidTokenException("Token is invalid: " + jwt, e);
        }
    }
}
