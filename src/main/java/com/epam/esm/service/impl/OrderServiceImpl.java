package com.epam.esm.service.impl;

import com.epam.esm.data.GiftCertificateRepository;
import com.epam.esm.data.OrderRepository;
import com.epam.esm.data.UserRepository;
import com.epam.esm.domain.Order;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.OrderDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.exception.NoSuchOrderException;
import com.epam.esm.service.OrderService;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

/**
 * @author Oleksandr Myronets.
 */
@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger LOGGER = LogManager.getLogger(OrderServiceImpl.class);

    private final ModelMapper modelMapper;

    private final OrderRepository orderRepository;

    private final UserRepository userRepository;

    private final GiftCertificateRepository giftCertificateRepository;

    public OrderServiceImpl(ModelMapper modelMapper, OrderRepository orderRepository, UserRepository userRepository,
                            GiftCertificateRepository giftCertificateRepository) {
        this.modelMapper = modelMapper;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.giftCertificateRepository = giftCertificateRepository;
    }


    @Override
    public OrderDTO read(Long userId, Long orderId) {
        LOGGER.info("Read Order method");
        Order order = orderRepository.read(orderId);
        if (!order.getUser().getId().equals(userId)) {
            LOGGER.debug("Requested order belongs to different user. Expected user id = {} actual user id = {}",
                         userId,
                         order.getUser().getId());
            LOGGER.debug("Order with id = {} doesn't exist", orderId);
            throw new NoSuchOrderException("Requested resource not found (id = " + orderId + ")");
        }
        LOGGER.debug("Mapping read Order: {} to Order DTO", order);
        OrderDTO orderDTO = modelMapper.map(order, OrderDTO.class);
        LOGGER.debug("Returning mapped Order DTO: {}", orderDTO);
        return orderDTO;
    }

    @Override
    public OrderDTO create(OrderDTO orderDTOToSave) {
        LOGGER.info("Create Gift Certificate method");
        Order orderToSave = new Order();
        orderToSave.setUser(userRepository.read(orderDTOToSave.getUserId()));
        orderToSave.setGiftCertificate(giftCertificateRepository.read(orderDTOToSave.getCertificateId()));
        orderToSave.setOrderCost(orderToSave.getGiftCertificate().getPrice());
        Order savedOrder = orderRepository.create(orderToSave);
        LOGGER.debug("Added order: {} for user: {}", orderDTOToSave, orderToSave.getUser().getId());
        LOGGER.debug("Mapping created Order: {} to Order DTO", savedOrder);
        OrderDTO orderDTO = modelMapper.map(savedOrder, OrderDTO.class);
        LOGGER.debug("Returning mapped Order DTO: {}", orderDTO);
        return orderDTO;
    }

    @Override
    public List<OrderDTO> list(FilterParams filterParams) {
        LOGGER.info("List Order method");
        LOGGER.debug("Getting List of Orders by parameters: {}", filterParams);
        List<Order> orders = orderRepository.list(filterParams);

        LOGGER.debug("Mapping read Tags list to tagDTO list");
        List<OrderDTO> orderDTOS = orders.stream().map(order -> modelMapper.map(order, OrderDTO.class)).toList();
        LOGGER.info("Mapped Order DTO List: {}", orderDTOS);
        LOGGER.info("Returning Order DTO List");
        return orderDTOS;
    }

    @Override
    public PagingParamsDTO getPagingParams(FilterParams filterParams, Long nextCandidate, Long prevCandidate) {
        return getPagingParams(filterParams, nextCandidate, prevCandidate, orderRepository);
    }
}
