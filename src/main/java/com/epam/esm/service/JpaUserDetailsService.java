package com.epam.esm.service;

import com.epam.esm.domain.JwtProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface JpaUserDetailsService extends UserDetailsService {

    UserDetails loadUserByProviderId(JwtProvider provider, String providerId);
}
