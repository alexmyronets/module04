package com.epam.esm.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import io.jsonwebtoken.Claims;
import org.springframework.security.core.Authentication;

public interface SecurityService {

    String generateJwtToken(Authentication authentication);

    String generateJwtRefreshToken(Authentication authentication);

    GoogleIdToken validateGoogleOauthToken(String token);

    String getIssuerWithoutValidation(String token);

    Claims getClaims(String jwt);
}
