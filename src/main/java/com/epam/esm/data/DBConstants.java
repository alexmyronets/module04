package com.epam.esm.data;

public abstract class DBConstants {

    //SQL SELECT statements

    //Do not remove used for creation of native query
    public static final String SQL_QUERY_MOST_USED_TAG_BY_MOST_SPENT_USER = """
        SELECT *
        FROM   tag
               JOIN (SELECT tag_id,
                            Count (gift_certificate_tag.gift_certificate_id) AS
                            cert_count
                     FROM   gift_certificate_tag
                            JOIN orders
                              ON gift_certificate_tag.gift_certificate_id =
                                 orders.gift_certificate_id
                            JOIN (SELECT user_id,
                                         Sum (order_cost) AS all_orders_cost
                                  FROM   orders
                                  GROUP  BY user_id
                                  ORDER  BY all_orders_cost DESC
                                  LIMIT  1) AS orders_sum
                              ON orders_sum.user_id = orders.user_id
                     GROUP  BY tag_id
                     ORDER  BY cert_count DESC
                     LIMIT  1) AS result
                 ON result.tag_id = tag.id""";
    public static final String JPQL_QUERY_MOST_USED_TAG_BY_MOST_SPENT_USER = """
        SELECT new Tag(t.id, t.name)
        FROM GiftCertificate gc
        JOIN gc.tags t
        JOIN Order o ON o.giftCertificate = gc
        JOIN o.user u
        WHERE u = (
              SELECT ou
              FROM Order o2
              JOIN o2.user ou
              GROUP BY ou
              HAVING SUM(o2.orderCost) = (
                    SELECT MAX(totalOrderCost)
                    FROM (
                          SELECT SUM(o3.orderCost) AS totalOrderCost
                          FROM Order o3
                          GROUP BY o3.user
                    ) subquery
              )
        )
        GROUP BY t.id, t.name
        ORDER BY COUNT(gc) DESC
        LIMIT 1
        """;

    private DBConstants() {
    }
}
