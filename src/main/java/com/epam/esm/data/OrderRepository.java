package com.epam.esm.data;

import com.epam.esm.data.crud.CreateRepository;
import com.epam.esm.data.crud.ListPagingRepository;
import com.epam.esm.data.crud.ReadRepository;
import com.epam.esm.domain.Order;

/**
 * This interface consists of methods for Order persistence operations
 *
 * @author Oleksandr Myronets.
 */
public interface OrderRepository extends CreateRepository<Order>, ReadRepository<Order>,
    ListPagingRepository<Order> {

}
