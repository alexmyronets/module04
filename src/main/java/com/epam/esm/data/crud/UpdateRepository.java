package com.epam.esm.data.crud;

public interface UpdateRepository<T> {

    T update(T t, long id);

}
