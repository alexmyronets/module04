package com.epam.esm.data.crud;

public interface ReadRepository<T> {

    T read(long id);

}
