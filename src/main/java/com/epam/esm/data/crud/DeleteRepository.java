package com.epam.esm.data.crud;

public interface DeleteRepository {

    void delete(long id);

}
