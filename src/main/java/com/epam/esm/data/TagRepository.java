package com.epam.esm.data;

import com.epam.esm.data.crud.CreateRepository;
import com.epam.esm.data.crud.DeleteRepository;
import com.epam.esm.data.crud.ListPagingRepository;
import com.epam.esm.data.crud.ReadRepository;
import com.epam.esm.domain.Tag;

/**
 * This interface consists of methods for Tag persistence operations
 *
 * @author Oleksandr Myronets.
 */
public interface TagRepository extends CreateRepository<Tag>, ReadRepository<Tag>, DeleteRepository,
    ListPagingRepository<Tag> {


    /**
     * @return Most used Tag among orders of the most spent user
     */
    Tag readMostUsedTagByMostSpendUser();
}
