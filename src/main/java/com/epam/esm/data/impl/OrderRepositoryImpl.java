package com.epam.esm.data.impl;

import com.epam.esm.data.OrderRepository;
import com.epam.esm.domain.Order;
import com.epam.esm.domain.User;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.OrderFilterParams;
import com.epam.esm.dto.paging.PageDirection;
import com.epam.esm.exception.NoSuchOrderException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

    private static final Logger LOGGER = LogManager.getLogger(OrderRepositoryImpl.class);

    private final EntityManager entityManager;

    public OrderRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    @Transactional
    @PreAuthorize("hasRole('ADMIN') || #order.user.username.equals(authentication.name)")
    public Order create(Order order) {
        LOGGER.info("Saving order {} into DB", order);
        entityManager.persist(order);
        LOGGER.info("Returning just created Order");
        return order;
    }

    @Override
    @PostAuthorize("hasRole('ADMIN') || returnObject.user.username.equals(authentication.name)")
    public Order read(long id) {
        LOGGER.info("Querying Order by id = {} from DB", id);
        Order order = entityManager.find(Order.class, id);
        if (order == null) {
            LOGGER.debug("Order with id = {} doesn't exist", id);
            throw new NoSuchOrderException("Requested resource not found (id = " + id + ")");
        }
        entityManager.detach(order);
        order.getUser().setPassword(null);
        LOGGER.info("Returning requested Order with id = {}", id);
        return order;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN') || principal.getUser.getId.equals(#filterParams.getUserId)")
    public List<Order> list(FilterParams filterParams) {
        LOGGER.info("Querying and returning List of tags from DB");
        OrderFilterParams orderFilterParams = (OrderFilterParams) filterParams;
        LOGGER.info("Querying and returning List of all orders for user: {} from DB", orderFilterParams.getUserId());
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        Root<Order> order = query.from(Order.class);
        Join<Order, User> user = order.join("user");
        Predicate userCriteria = cb.equal(user.get("id"), orderFilterParams.getUserId());

        query.select(order);
        if (filterParams.getPage().getAfterId() != null) {
            LOGGER.debug("Pagination criteria is present, need to consider in query");
            var orders = getOrderList(filterParams, cb, query, order, userCriteria);
            orders.forEach(o -> {
                entityManager.detach(o);
                o.getUser().setPassword(null);
            });
            return orders;
        }
        LOGGER.debug("Pagination criteria is absent, querying first page");
        LOGGER.info("Returning List of Order");
        var orders = entityManager.createQuery(query.where(userCriteria).orderBy(cb.asc(order)))
                                  .setMaxResults(filterParams.getPage().getSize())
                                  .getResultList();
        orders.forEach(o -> {
            entityManager.detach(o);
            o.getUser().setPassword(null);
        });
        return orders;
    }


    @Override
    @PreAuthorize("hasRole('ADMIN') || principal.getUser.getId.equals(#filterParams.getUserId)")
    public Long getNextAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is next page with given filter params: {}", filterParams);
        OrderFilterParams orderFilterParams = (OrderFilterParams) filterParams;
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        Root<Order> order = query.from(Order.class);
        Join<Order, User> user = order.join("user");
        Predicate userCriteria = cb.equal(user.get("id"), orderFilterParams.getUserId());

        query.select(order).where(cb.and(userCriteria, cb.gt(order.get("id"), candidate))).orderBy(cb.asc(order));
        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Next page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Next page doesn't exist");
        return -1L;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN') || principal.getUser.getId.equals(#filterParams.getUserId)")
    public Long getPrevAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is previous page with given filter params: {}", filterParams);
        OrderFilterParams orderFilterParams = (OrderFilterParams) filterParams;
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        Root<Order> order = query.from(Order.class);
        Join<Order, User> user = order.join("user");
        Predicate userCriteria = cb.equal(user.get("id"), orderFilterParams.getUserId());

        query.select(order).where(cb.and(userCriteria, cb.lt(order.get("id"), candidate))).orderBy(cb.desc(order));
        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Previous page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Previous page doesn't exist");
        return -1L;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN') || principal.getUser.getId.equals(#filterParams.getUserId)")
    public Long getLastAfterId(FilterParams filterParams) {
        LOGGER.info("Check whether there are more than one page with given filter params: {}", filterParams);
        OrderFilterParams orderFilterParams = (OrderFilterParams) filterParams;
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        Root<Order> order = query.from(Order.class);
        Join<Order, User> user = order.join("user");
        Predicate userCriteria = cb.equal(user.get("id"), orderFilterParams.getUserId());

        query.select(order).where(userCriteria).orderBy(cb.desc(order));
        List<Order> orders = entityManager.createQuery(query)
                                          .setMaxResults(filterParams.getPage().getSize() + 1)
                                          .setFirstResult(filterParams.getPage().getSize())
                                          .getResultList();
        if (!orders.isEmpty()) {
            long afterId = orders.get(0).getId();
            LOGGER.info("There are more than one page, last page after id: {}", afterId);
            return afterId;
        }
        LOGGER.info("There is no more than one page");
        return -1L;
    }

    private List<Order> getOrderList(FilterParams filterParams, CriteriaBuilder cb, CriteriaQuery<Order> query,
                                     Root<Order> order, Predicate userCriteria) {
        LOGGER.debug("Retrieving List of Orders from DB by paging criteria");
        if (filterParams.getPage().getDirection() == PageDirection.FORWARD) {
            query.where(cb.and(userCriteria, cb.gt(order.get("id"), filterParams.getPage().getAfterId())))
                 .orderBy(cb.asc(order));
            return entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize()).getResultList();
        }
        query.where(cb.and(userCriteria, cb.lt(order.get("id"), filterParams.getPage().getAfterId()))).orderBy(cb.desc(
            order));
        List<Order> orders = entityManager.createQuery(query)
                                          .setMaxResults(filterParams.getPage().getSize())
                                          .getResultList();
        LOGGER.info("Page direction is BACK reversing result");
        Collections.reverse(orders);
        return orders;
    }
}
