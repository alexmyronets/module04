package com.epam.esm.data.impl;

import com.epam.esm.data.UserRepository;
import com.epam.esm.domain.JwtProvider;
import com.epam.esm.domain.Role;
import com.epam.esm.domain.User;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.paging.PageDirection;
import com.epam.esm.exception.NoSuchUserException;
import com.epam.esm.exception.UserAlreadyExistsException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import jakarta.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final Logger LOGGER = LogManager.getLogger(UserRepositoryImpl.class);

    private final EntityManager entityManager;

    public UserRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN') || principal.getUser.getId.equals(#id)")
    public User read(long id) {
        LOGGER.info("Querying User by id = {} from DB", id);
        User user = entityManager.find(User.class, id);
        if (user == null) {
            LOGGER.debug("User with id = {} doesn't exist", id);
            throw new NoSuchUserException("Requested resource not found (id = " + id + ")");
        }
        entityManager.detach(user);
        user.setPassword(null);
        LOGGER.info("Returning requested User with id = {}", id);
        return user;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> list(FilterParams filterParams) {
        LOGGER.info("Querying and returning List of all users from DB");
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);

        query.select(user);

        if (filterParams.getPage().getAfterId() != null) {
            LOGGER.debug("Pagination criteria is present, need to consider in query");
            var users = getUserList(filterParams, cb, query, user);
            users.forEach(u -> u.setPassword(null));
            return users;
        }
        LOGGER.debug("Pagination criteria is absent, querying first page");
        query.orderBy(cb.asc(user));
        LOGGER.info("Returning List of Users");
        var users = entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize()).getResultList();
        users.forEach(u -> u.setPassword(null));
        return users;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Long getNextAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is next page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);

        query.select(user).where(cb.gt(user.get("id"), candidate)).orderBy(cb.asc(user));

        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Next page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Next page doesn't exist");
        return -1L;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Long getPrevAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is previous page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);

        query.select(user).where(cb.lt(user.get("id"), candidate)).orderBy(cb.desc(user));

        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Previous page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Previous page doesn't exist");
        return -1L;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Long getLastAfterId(FilterParams filterParams) {
        LOGGER.info("Check whether there are more than one page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);

        query.select(user).orderBy(cb.desc(user));
        List<User> users = entityManager.createQuery(query)
                                        .setFirstResult(filterParams.getPage().getSize())
                                        .setMaxResults(filterParams.getPage().getSize() + 1)
                                        .getResultList();
        if (!users.isEmpty()) {
            long afterId = users.get(0).getId();
            LOGGER.info("There are more than one page, last page after id: {}", afterId);
            return afterId;
        }
        LOGGER.info("There is no more than one page");
        return -1L;
    }

    @Override
    @Transactional
    public User create(User user) {
        LOGGER.info("Creating a new User with username = {}", user.getUsername());
        if (isUserExists(user.getUsername())) {
            LOGGER.debug("User with username = {} already exists", user.getUsername());
            throw new UserAlreadyExistsException("User (name = " + user.getUsername() + ") already exists");
        }
        if (user.getProviderId() != null && isUserExists(user.getAuthProvider(), user.getProviderId())) {
            LOGGER.debug("User with provider: {} and providerId: {} already exists",
                         user.getAuthProvider(),
                         user.getProviderId());
            throw new UserAlreadyExistsException(
                "User (provider = " + user.getAuthProvider() + ", providerId = " + user.getProviderId()
                    + ") already exists");
        }

        LOGGER.info("Assigning role 'USER' to the new User");
        user.setRole(Role.USER);
        LOGGER.info("Persisting the new User to the database");
        entityManager.persist(user);
        entityManager.detach(user);
        LOGGER.info("Removing password from the returned User object");
        user.setPassword(null);
        LOGGER.info("New User with username = {} created successfully", user.getUsername());
        return user;
    }

    @Override
    public User findByUserName(String username) {
        LOGGER.info("Searching for User by username = {}", username);
        String jpql = "SELECT u FROM User u WHERE u.username = :username";
        TypedQuery<User> query = entityManager.createQuery(jpql, User.class);
        query.setParameter("username", username);
        List<User> users = query.getResultList();
        if (users.isEmpty()) {
            LOGGER.debug("User with username = {} doesn't exist", username);
            throw new NoSuchUserException("Requested resource not found (username = " + username + ")");
        }
        User user = users.get(0);
        LOGGER.info("Found User with username = {}", username);
        return user;
    }

    @Override
    public User findByProviderId(JwtProvider provider, String providerId) {
        LOGGER.info("Searching for User by provider = {} and providerId = {}", provider::name, () ->  providerId);
        String jpql = "SELECT u FROM User u WHERE u.authProvider = :provider AND u.providerId = :providerId";
        TypedQuery<User> query = entityManager.createQuery(jpql, User.class);
        query.setParameter("provider", provider);
        query.setParameter("providerId", providerId);
        List<User> users = query.getResultList();
        if (users.isEmpty()) {
            LOGGER.debug("User with provider = {}, providerId = {} doesn't exist", provider::name, () ->  providerId);
            throw new NoSuchUserException("Requested resource not found (providerId = " + providerId + ")");
        }
        User user = users.get(0);
        LOGGER.info("Found User with provider = {} and providerId = {}", provider::name, () ->  providerId);
        return user;
    }

    private List<User> getUserList(FilterParams filterParams, CriteriaBuilder cb, CriteriaQuery<User> query,
                                   Root<User> user) {
        LOGGER.debug("Retrieving List of Users from DB by paging criteria");
        if (filterParams.getPage().getDirection() == PageDirection.FORWARD) {
            query.where(cb.gt(user.get("id"), filterParams.getPage().getAfterId())).orderBy(cb.asc(user.get("id")));
            return entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize()).getResultList();
        }
        query.where(cb.lt(user.get("id"), filterParams.getPage().getAfterId())).orderBy(cb.desc(user.get("id")));
        List<User> users = entityManager.createQuery(query)
                                        .setMaxResults(filterParams.getPage().getSize())
                                        .getResultList();
        LOGGER.info("Page direction is BACK reversing result");
        Collections.reverse(users);
        return users;
    }

    private boolean isUserExists(String username) {
        LOGGER.info("Checking if User with username = {} exists", username);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> userRoot = query.from(User.class);

        query.select(userRoot).where(cb.equal(userRoot.get("username"), username));

        List<User> users = entityManager.createQuery(query).getResultList();
        boolean exists = !users.isEmpty();
        LOGGER.info("User with username = {} exists: {}", username, exists);
        return exists;
    }

    private boolean isUserExists(JwtProvider provider, String providerId) {
        LOGGER.info("Checking if User with provider = {} and providerId = {} exists",
                    provider::name,
                    () -> providerId);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);

        query.select(user).where(cb.and(cb.equal(user.get("authProvider"), provider),
                                        cb.equal(user.get("providerId"), providerId)));
        List<User> users = entityManager.createQuery(query).getResultList();
        boolean exists = !users.isEmpty();
        LOGGER.info("User with provider = {} and providerId = {} exists: {}",
                    provider::name,
                    () -> providerId,
                    () -> exists);
        return exists;
    }
}
