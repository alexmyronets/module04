package com.epam.esm.data.impl;

import com.epam.esm.data.GiftCertificateRepository;
import com.epam.esm.data.TagRepository;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.domain.SortOrder;
import com.epam.esm.domain.Tag;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.GiftCertificateFilterParams;
import com.epam.esm.dto.TagDTO;
import com.epam.esm.dto.paging.PageDirection;
import com.epam.esm.exception.GiftCertificateInUseException;
import com.epam.esm.exception.NoSuchGiftCertificateException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class GiftCertificateRepositoryImpl implements GiftCertificateRepository {

    private static final Logger LOGGER = LogManager.getLogger(GiftCertificateRepositoryImpl.class);

    private final TagRepository tagRepository;
    private final EntityManager entityManager;

    public GiftCertificateRepositoryImpl(TagRepository tagRepository,
                                         EntityManager entityManager) {
        this.tagRepository = tagRepository;
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public GiftCertificate create(GiftCertificate giftCert) {
        LOGGER.info("Saving Gift Certificate {} into DB", giftCert);
        createGiftCertificateTags(giftCert.getTags());
        entityManager.persist(giftCert);
        LOGGER.info("Returning just created Gift Certificate");
        return giftCert;
    }

    @Override
    public GiftCertificate read(long id) {
        LOGGER.info("Querying Gift Certificate by id = {} from DB", id);
        GiftCertificate giftCertificate = entityManager.find(GiftCertificate.class, id);
        if (giftCertificate == null) {
            LOGGER.info("Gift Certificate with id = {} doesn't exist", id);
            throw new NoSuchGiftCertificateException("Requested resource not found (id = " + id + ")");
        }
        LOGGER.info("Returning requested Gift Certificate with id = {}", id);

        return giftCertificate;
    }

    @Override
    @Transactional
    public GiftCertificate update(GiftCertificate patch, long id) {
        LOGGER.info("Updating DB record of Gift Certificate with id = {}", id);
        GiftCertificate current = read(id);

        setPatchValues(current, patch);
        LOGGER.info("Returning just created Gift Certificate");
        return current;
    }

    @Override
    @Transactional
    public void delete(long id) {
        LOGGER.info("Removing Gift Certificate with id = {} from DB", id);
        GiftCertificate giftCertificate = read(id);
        int orderCount = countOrdersByGiftCertificate(giftCertificate);
        if (orderCount > 0) {
            LOGGER.info("Gift Certificate with id = {} cannot be deleted, because it in use by {} Orders",
                        id,
                        orderCount);
            throw new GiftCertificateInUseException(
                "Gift Certificate (id = " + id + ") is in use by " + orderCount + " orders");
        }
        entityManager.remove(giftCertificate);
        LOGGER.info("Gift Certificate with id = {} removed from DB", id);
    }

    @Override
    public List<GiftCertificate> list(FilterParams filterParams) {
        GiftCertificateFilterParams filterParamsDTO = (GiftCertificateFilterParams) filterParams;
        LOGGER.info("Querying DB for certificates by search parameters: {}", filterParamsDTO);
        List<GiftCertificate> giftCerts = entityManager.createQuery(composeSearchQuery(filterParamsDTO)).setMaxResults(
            filterParams.getPage().getSize()).getResultList();
        if (filterParams.getPage().getDirection() == PageDirection.BACK) {
            Collections.reverse(giftCerts);
        }
        LOGGER.info("Returning List of Gift Certificates found by search parameters");
        return giftCerts;
    }

    @Override
    public Long getNextAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is next page with given filter params: {}", filterParams);
        GiftCertificateFilterParams filterParamsDTO = (GiftCertificateFilterParams) filterParams;
        GiftCertificateFilterParams nextPageFilterParams = new GiftCertificateFilterParams(filterParamsDTO);
        nextPageFilterParams.getPage()
                            .setAfterId(candidate);
        nextPageFilterParams.getPage()
                            .setDirection(PageDirection.FORWARD);
        nextPageFilterParams.getPage()
                            .setSize((byte) 1);

        List<GiftCertificate> giftCerts = entityManager.createQuery(composeSearchQuery(nextPageFilterParams))
                                                       .setMaxResults(1)
                                                       .getResultList();
        if (!giftCerts.isEmpty()) {
            LOGGER.info("Next page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Next page doesn't exist");
        return -1L;
    }

    @Override
    public Long getPrevAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is previous page with given filter params: {}", filterParams);
        GiftCertificateFilterParams filterParamsDTO = (GiftCertificateFilterParams) filterParams;
        GiftCertificateFilterParams prevPageFilterParams = new GiftCertificateFilterParams(filterParamsDTO);

        prevPageFilterParams.getPage()
                            .setAfterId(candidate);
        prevPageFilterParams.getPage()
                            .setDirection(PageDirection.BACK);
        prevPageFilterParams.getPage()
                            .setSize((byte) 1);

        List<GiftCertificate> giftCerts = entityManager.createQuery(composeSearchQuery(prevPageFilterParams))
                                                       .setMaxResults(1)
                                                       .getResultList();
        if (!giftCerts.isEmpty()) {
            LOGGER.info("Previous page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Previous page doesn't exist");
        return -1L;
    }

    @Override
    public Long getLastAfterId(FilterParams filterParams) {
        LOGGER.info("Check whether there are more than one page with given filter params: {}", filterParams);
        GiftCertificateFilterParams filterParamsDTO = (GiftCertificateFilterParams) filterParams;
        GiftCertificateFilterParams lastPageFilterParams = new GiftCertificateFilterParams(filterParamsDTO);

        lastPageFilterParams.getPage()
                            .setSize((byte) (filterParamsDTO.getPage()
                                                            .getSize() + 1));
        CriteriaQuery<GiftCertificate> query = composeSearchQuery(lastPageFilterParams);
        query.orderBy(reverseQueryOrder(query.getOrderList()));

        List<GiftCertificate> giftCerts = entityManager.createQuery(query).setMaxResults(
            filterParamsDTO.getPage().getSize() + 1).setFirstResult(filterParams.getPage().getSize()).getResultList();
        if (!giftCerts.isEmpty()) {
            long afterId = giftCerts.get(0).getId();
            LOGGER.info("There are more than one page, last page after id: {}", afterId);
            return afterId;
        }
        LOGGER.info("There is no more than one page");
        return -1L;
    }

    private void setPatchValues(GiftCertificate current, GiftCertificate patch) {
        LOGGER.debug("Setting new values from the patch");
        if (patch.getName() != null) {
            LOGGER.debug("New name: {}", patch::getName);
            current.setName(patch.getName());
        }
        if (patch.getDescription() != null) {
            LOGGER.debug("New description: {}", patch::getDescription);
            current.setDescription(patch.getDescription());
        }
        if (patch.getPrice() != null) {
            LOGGER.debug("New price: {}", patch::getPrice);
            current.setPrice(patch.getPrice());
        }
        if (patch.getDuration() != null) {
            LOGGER.debug("New duration: {}", patch::getDuration);
            current.setDuration(patch.getDuration());
        }
        if (patch.getTags() != null) {
            createGiftCertificateTags(patch.getTags());
            LOGGER.debug("New tags: {}", patch::getTags);
            current.setTags(patch.getTags());
        }
    }

    private void createGiftCertificateTags(Set<Tag> tags) {
        LOGGER.info("Saving new Tags for Gift Certificate into DB");
        tags.forEach(tag -> {
            if (tag.getId() == null) {
                LOGGER.debug("Tag with name = {} need to be saved in DB", tag::getName);
                tagRepository.create(tag);
            }
        });
    }

    private CriteriaQuery<GiftCertificate> composeSearchQuery(GiftCertificateFilterParams searchParams) {
        LOGGER.debug("Composing search query by search params: {}", searchParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<GiftCertificate> query = cb.createQuery(GiftCertificate.class);
        Root<GiftCertificate> giftCertificate = query.from(GiftCertificate.class);
        Join<GiftCertificate, Tag> tag = giftCertificate.join("tags");
        List<Predicate> searchPredicates = new ArrayList<>();
        List<Order> orders = getSearchSortOrder(cb, giftCertificate, searchParams);

        if (!searchParams.getTags().isEmpty()) {
            LOGGER.debug("Adding tags to search predicates");
            searchPredicates.add(getTagSearchPredicate(cb, tag, searchParams.getTags()));
        }

        if (searchParams.getSearchText() != null) {
            LOGGER.debug("Adding search text to search predicates");
            searchPredicates.add(getTextSearchPredicate(cb, giftCertificate, searchParams.getSearchText()));
        }

        if (searchParams.getPage().getAfterId() != null) {
            LOGGER.debug("Adding pagination conditions to search predicate");
            searchPredicates.add(getPagingSearchPredicate(cb, giftCertificate, searchParams));
        }
        LOGGER.debug("Returning composed criteria query");
        return query.select(giftCertificate).distinct(true).where(cb.and(searchPredicates.toArray(Predicate[]::new)))
                    .orderBy(orders.toArray(Order[]::new));
    }

    private Predicate getTagSearchPredicate(CriteriaBuilder cb, Join<GiftCertificate, Tag> tag,
                                            List<TagDTO> tags) {
        LOGGER.debug("Composing tags search predicate");
        Predicate tagPredicate = cb.or(tags
                                           .stream()
                                           .map(t -> cb.equal(tag.get("name"), t.getName()))
                                           .toArray(Predicate[]::new));
        LOGGER.debug("Composed tags search predicate: {}", tagPredicate);
        return tagPredicate;
    }

    private Predicate getTextSearchPredicate(CriteriaBuilder cb, Root<GiftCertificate> giftCertificate,
                                             String searchText) {
        LOGGER.debug("Composing text search predicate");
        Predicate textPredicate = cb.or(cb.like(cb.lower(giftCertificate.get("name")), "%" + searchText.toLowerCase() + "%"),
                                        cb.like(cb.lower(giftCertificate.get("description")), "%" + searchText.toLowerCase() + "%"));
        LOGGER.debug("Composed text search predicate: {}", textPredicate);
        return textPredicate;

    }

    private Predicate getPagingSearchPredicate(CriteriaBuilder cb, Root<GiftCertificate> giftCertificate,
                                               GiftCertificateFilterParams searchParams) {
        LOGGER.debug("Composing pagination predicate");
        List<Predicate> searchPredicates = new ArrayList<>();
        GiftCertificate cursor = read(searchParams.getPage().getAfterId());
        Predicate pagingPredicate;

        if (searchParams.getSortByName() != null && searchParams.getSortByDate() != null) {
            LOGGER.debug("Composing pagination predicate by 3 sorting parameters: name, date, id");
            searchPredicates.add(getComparePredicate(cb,
                                                     giftCertificate.get("name"),
                                                     searchParams.getSortByName(),
                                                     searchParams.getPage().getDirection(),
                                                     cursor.getName()));
            searchPredicates.add(cb.and(cb.equal(giftCertificate.get("name"), cursor.getName()),
                                        getComparePredicate(cb,
                                                            giftCertificate.get("createDate"),
                                                            searchParams.getSortByDate(),
                                                            searchParams.getPage().getDirection(),
                                                            cursor.getCreateDate())));
            searchPredicates.add(cb.and(cb.equal(giftCertificate.get("name"), cursor.getName()),
                                        cb.equal(giftCertificate.get("createDate"), cursor.getCreateDate()),
                                        getComparePredicate(cb,
                                                            giftCertificate.get("id"),
                                                            SortOrder.ASC,
                                                            searchParams.getPage()
                                                                        .getDirection(),
                                                            cursor.getId())));
            pagingPredicate = cb.or(searchPredicates.toArray(Predicate[]::new));
            LOGGER.debug("Composed pagination predicate: {}", pagingPredicate);
            return pagingPredicate;
        }

        if (searchParams.getSortByName() != null) {
            LOGGER.debug("Composing pagination predicate by 2 sorting parameters: name, id");
            searchPredicates.add(getComparePredicate(cb,
                                                     giftCertificate.get("name"),
                                                     searchParams.getSortByName(),
                                                     searchParams.getPage().getDirection(),
                                                     cursor.getName()));
            searchPredicates.add(cb.and(cb.equal(giftCertificate.get("name"), cursor.getName()),
                                        getComparePredicate(cb,
                                                            giftCertificate.get("id"),
                                                            SortOrder.ASC,
                                                            searchParams.getPage()
                                                                        .getDirection(),
                                                            cursor.getId())));
            pagingPredicate = cb.or(searchPredicates.toArray(Predicate[]::new));
            LOGGER.debug("Composed pagination predicate: {}", pagingPredicate);
            return pagingPredicate;
        }

        if (searchParams.getSortByDate() != null) {
            LOGGER.debug("Composing pagination predicate by 2 sorting parameters: date, id");
            searchPredicates.add(getComparePredicate(cb,
                                                     giftCertificate.get("createDate"),
                                                     searchParams.getSortByDate(),
                                                     searchParams.getPage().getDirection(),
                                                     cursor.getCreateDate()));
            searchPredicates.add(cb.and(cb.equal(giftCertificate.get("createDate"), cursor.getCreateDate()),
                                        getComparePredicate(cb,
                                                            giftCertificate.get("id"),
                                                            SortOrder.ASC,
                                                            searchParams.getPage()
                                                                        .getDirection(),
                                                            cursor.getId())));
            pagingPredicate = cb.or(searchPredicates.toArray(Predicate[]::new));
            LOGGER.debug("Composed pagination predicate: {}", pagingPredicate);
            return pagingPredicate;
        }
        LOGGER.debug("Composing pagination predicate by 1 sorting parameter: id");
        pagingPredicate = getComparePredicate(cb,
                                              giftCertificate.get("id"),
                                              SortOrder.ASC,
                                              searchParams.getPage()
                                                          .getDirection(),
                                              cursor.getId());
        LOGGER.debug("Composed pagination predicate: {}", pagingPredicate);
        return pagingPredicate;
    }


    private List<Order> reverseQueryOrder(List<Order> orders) {
        LOGGER.debug("Reversing sort orders: {}", orders);
        return orders.stream().map(Order::reverse).toList();
    }

    private List<Order> getSearchSortOrder(CriteriaBuilder cb, Root<GiftCertificate> giftCertificate,
                                           GiftCertificateFilterParams searchParams) {
        LOGGER.debug("Composing sorting order for query");
        List<Order> orders = new ArrayList<>();

        if (searchParams.getSortByName() != null) {
            LOGGER.debug("Adding sort order for Gift Certificate name to search query: {}",
                         searchParams::getSortByName);
            orders.add(this.<String>getOrder(cb, giftCertificate.get("name"), searchParams.getSortByName()));
        }

        if (searchParams.getSortByDate() != null) {
            LOGGER.debug("Adding sort order for Gift Certificate creation date to search query: {}",
                         searchParams::getSortByDate);
            orders.add(this.<LocalDateTime>getOrder(cb,
                                                    giftCertificate.get("createDate"),
                                                    searchParams.getSortByDate()));
        }

        if (searchParams.getPage().getDirection() == PageDirection.FORWARD) {
            LOGGER.debug("Adding sort order for Gift Certificate Id to search query: ASC)");
            orders.add(cb.asc(giftCertificate.get("id")));

        } else {
            LOGGER.debug("Adding sort order for Gift Certificate Id to search query: DESC)");
            orders.add(cb.desc(giftCertificate.get("id")));
        }
        LOGGER.debug("Composed sorting order for query: {}", orders);
        return orders;
    }

    private <T extends Comparable<T>> Predicate getComparePredicate(CriteriaBuilder cb, Expression<T> expression,
                                                                    SortOrder order, PageDirection direction, T t) {
        LOGGER.debug("Composing comparing predicate for order: {} and page direction: {}", order, direction);
        if ((order == SortOrder.ASC && direction == PageDirection.FORWARD) || (order == SortOrder.DESC
            && direction == PageDirection.BACK)) {
            LOGGER.debug("Comparing predicate is greater than");
            return cb.greaterThan(expression, t);
        }
        LOGGER.debug("Comparing predicate is less than");
        return cb.lessThan(expression, t);
    }

    private <T extends Comparable<? super T>> Order getOrder(CriteriaBuilder cb, Expression<T> expression,
                                                             SortOrder order) {
        LOGGER.debug("Calculating query order based on sort order field value: {}", order);
        if (order == SortOrder.ASC) {
            LOGGER.debug("Query order is ASC");
            return cb.asc(expression);
        }
        LOGGER.debug("Query order is DESC");
        return cb.desc(expression);
    }

    private int countOrdersByGiftCertificate(GiftCertificate giftCertificate) {
        LOGGER.debug("Checking whether Gift Certificate with id: {} is in use by one or more orders",
                     giftCertificate.getId());
        //JPQL way
        TypedQuery<Long> query = entityManager.createQuery(
            "SELECT COUNT(o) FROM Order o JOIN GiftCertificate g ON o.giftCertificate = g WHERE g = :giftCertificate",
            Long.class);
        query.setParameter("giftCertificate", giftCertificate);
        return Math.toIntExact(query.getSingleResult());
    }
}
