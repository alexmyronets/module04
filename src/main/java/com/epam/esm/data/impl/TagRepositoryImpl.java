package com.epam.esm.data.impl;

import com.epam.esm.data.TagJPARepository;
import com.epam.esm.data.TagRepository;
import com.epam.esm.domain.Tag;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.paging.PageDirection;
import com.epam.esm.exception.NoSuchTagException;
import com.epam.esm.exception.TagAlreadyExistsException;
import com.epam.esm.exception.TagIsInUseException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class TagRepositoryImpl implements TagRepository {

    protected static final Logger LOGGER = LogManager.getLogger(TagRepositoryImpl.class);

    private final EntityManager entityManager;

    private final TagJPARepository tagJPARepository;

    public TagRepositoryImpl(EntityManager entityManager, TagJPARepository tagJPARepository) {
        this.entityManager = entityManager;
        this.tagJPARepository = tagJPARepository;
    }

    @Override
    @Transactional
    public Tag create(Tag tag) {
        LOGGER.info("Saving tag {} into DB", tag);
        List<Tag> existingTags = tagJPARepository.findByName(tag.getName());

        if (!existingTags.isEmpty()) {
            LOGGER.debug("Tag with name {}, already exists", tag::getName);
            throw new TagAlreadyExistsException("Tag (name = " + tag.getName() + ") is already exists");
        }

        LOGGER.info("Returning just created Tag");
        return tagJPARepository.save(tag);
    }

    @Override
    public Tag read(long id) {
        LOGGER.info("Querying Tag by id = {} from DB", id);
        Tag tag = tagJPARepository.findById(id).orElse(null);

        if (tag == null) {
            LOGGER.debug("Tag with id = {} doesn't exist", id);
            throw new NoSuchTagException("Requested resource not found (id = " + id + ")");
        }
        LOGGER.info("Returning requested Tag with id: {}", id);
        return tag;
    }

    @Override
    @Transactional
    public void delete(long id) {
        LOGGER.info("Removing Tag with id = {} from DB", id);
        Tag tag = read(id);
        int certCount = (int) tagJPARepository.countGiftCertificatesByTagId(id);
        if (certCount > 0) {
            LOGGER.debug("Tag with id = {} cannot be deleted, because it in use by {} Gift Certificates",
                         id,
                         certCount);
            throw new TagIsInUseException("Tag (id = " + id + ") is in use by " + certCount + " gift certificates");
        }
        tagJPARepository.delete(tag);
        LOGGER.info("Tag with id = {} removed from DB", id);
    }

    @Override
    public List<Tag> list(FilterParams filterParams) {
        LOGGER.info("Querying and returning List of tags from DB");
        TypedQuery<Tag> typedQuery;
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        Root<Tag> tag = query.from(Tag.class);

        query.select(tag);

        if (filterParams.getPage().getAfterId() != null) {
            LOGGER.debug("Pagination criteria is present, need to consider in query");
            return getTagList(filterParams, cb, query, tag);
        }
        LOGGER.debug("Pagination criteria is absent, querying first page");
        query.orderBy(cb.asc(tag));
        typedQuery = entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize());
        LOGGER.info("Returning List of Tags");
        return typedQuery.getResultList();
    }


    @Override
    public Long getNextAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is next page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        Root<Tag> tag = query.from(Tag.class);

        query.select(tag).where(cb.gt(tag.get("id"), candidate)).orderBy(cb.asc(tag));

        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Next page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Next page doesn't exist");
        return -1L;
    }

    @Override
    public Long getPrevAfterId(FilterParams filterParams, Long candidate) {
        LOGGER.info("Check whether there is previous page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        Root<Tag> tag = query.from(Tag.class);

        query.select(tag);
        query.where(cb.lt(tag.get("id"), candidate));
        query.orderBy(cb.desc(tag));

        if (!entityManager.createQuery(query).setMaxResults(1).getResultList().isEmpty()) {
            LOGGER.info("Previous page is exist, after id: {}", candidate);
            return candidate;
        }
        LOGGER.info("Previous page doesn't exist");
        return -1L;
    }

    @Override
    public Long getLastAfterId(FilterParams filterParams) {
        LOGGER.info("Check whether there are more than one page with given filter params: {}", filterParams);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        Root<Tag> tag = query.from(Tag.class);

        query.select(tag);
        query.orderBy(cb.desc(tag));

        List<Tag> tags = entityManager.createQuery(query)
                                      .setFirstResult(filterParams.getPage().getSize())
                                      .setMaxResults(filterParams.getPage().getSize() + 1)
                                      .getResultList();

        if (!tags.isEmpty()) {
            long afterId = tags.get(0).getId();
            LOGGER.info("There are more than one page, last page after id: {}", afterId);
            return afterId;
        }
        LOGGER.info("There is no more than one page");
        return -1L;
    }

    @Override
    public Tag readMostUsedTagByMostSpendUser() {
        LOGGER.info("Querying most used Tag by most spent user from DB");
        Tag tag = tagJPARepository.findMostUsedTagByMostSpentUser();

        if (tag == null) {
            LOGGER.debug("Most used Tag by most spent user doesn't exist.");
            throw new NoSuchTagException("Requested resource not found");
        }
        LOGGER.info("Returning most used Tag by most spent user");
        return tag;
    }

    private List<Tag> getTagList(FilterParams filterParams, CriteriaBuilder cb, CriteriaQuery<Tag> query,
                                 Root<Tag> tag) {
        LOGGER.debug("Retrieving List of Tags from DB by paging criteria");
        TypedQuery<Tag> typedQuery;
        if (filterParams.getPage().getDirection() == PageDirection.FORWARD) {
            query.where(cb.gt(tag.get("id"), filterParams.getPage().getAfterId()));
            query.orderBy(cb.asc(tag.get("id")));
            typedQuery = entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize());
            return typedQuery.getResultList();
        }

        query.where(cb.lt(tag.get("id"), filterParams.getPage().getAfterId()));
        query.orderBy(cb.desc(tag.get("id")));
        typedQuery = entityManager.createQuery(query).setMaxResults(filterParams.getPage().getSize());
        List<Tag> result = typedQuery.getResultList();
        LOGGER.info("Page direction is BACK reversing result");
        Collections.reverse(result);
        return result;
    }
}
