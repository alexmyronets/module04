package com.epam.esm.data;

import com.epam.esm.data.crud.CreateRepository;
import com.epam.esm.data.crud.DeleteRepository;
import com.epam.esm.data.crud.ListPagingRepository;
import com.epam.esm.data.crud.ReadRepository;
import com.epam.esm.data.crud.UpdateRepository;
import com.epam.esm.domain.GiftCertificate;

/**
 * This interface consists of methods for Gift Certificate persistence operations
 *
 * @author Oleksandr Myronets.
 */
public interface GiftCertificateRepository extends CreateRepository<GiftCertificate>,
    ReadRepository<GiftCertificate>, UpdateRepository<GiftCertificate>, DeleteRepository,
    ListPagingRepository<GiftCertificate> {

}
