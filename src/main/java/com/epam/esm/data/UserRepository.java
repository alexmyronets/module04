package com.epam.esm.data;

import com.epam.esm.data.crud.CreateRepository;
import com.epam.esm.data.crud.ListPagingRepository;
import com.epam.esm.data.crud.ReadRepository;
import com.epam.esm.domain.JwtProvider;
import com.epam.esm.domain.User;

/**
 * This interface consists of methods for User persistence operations
 *
 * @author Oleksandr Myronets.
 */
public interface UserRepository extends ReadRepository<User>, ListPagingRepository<User>, CreateRepository<User> {

    User findByUserName(String username);

    User findByProviderId(JwtProvider provider, String providerId);

}
