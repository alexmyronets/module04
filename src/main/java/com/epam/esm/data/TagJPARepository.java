package com.epam.esm.data;

import static com.epam.esm.data.DBConstants.JPQL_QUERY_MOST_USED_TAG_BY_MOST_SPENT_USER;

import com.epam.esm.domain.Tag;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface TagJPARepository extends CrudRepository<Tag, Long> {

    List<Tag> findByName(String name);

    @Query("SELECT COUNT(gc) FROM GiftCertificate gc JOIN gc.tags t WHERE t.id = :tagId")
    long countGiftCertificatesByTagId(Long tagId);

    @Query(JPQL_QUERY_MOST_USED_TAG_BY_MOST_SPENT_USER)
    Tag findMostUsedTagByMostSpentUser();

}
