package com.epam.esm.dto;


import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class LoginRequest {

    @NotEmpty(message = "Username cannot be blank")
    @Size(min = 6, max = 16, message = "Username length should be from 6 to 16 characters")
    @Pattern(regexp = "^(?!.*[_.]{2})[a-zA-Z0-9](?:[a-zA-Z0-9._]*[a-zA-Z0-9])?$", message = "Username should consist of latin letters, numbers, periods, and underscores, without starting or ending with periods or underscores, and without having consecutive periods or underscores.")
    private String username;
    @NotEmpty(message = "Password cannot be blank")
    @Size(min = 8, max = 16, message = "password length should be from 8 to 16 characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$)[a-zA-Z0-9]*$", message = "Password should consist of latin letters, numbers and should have at least one uppercase letter, one digit.")
    private String password;
}
