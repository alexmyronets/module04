package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.Min;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Relation(collectionRelation = "orders", itemRelation = "order")
public class OrderDTO extends RepresentationModel<OrderDTO> implements Serializable {

    public static final long serialVersionUID = 4103135541026975762L;

    @JsonIgnore
    private Long id;

    @JsonIgnore
    private Long userId;

    @Min(value = 1, message = "Certificate Id cannot be less than 1")
    private Long certificateId;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime orderDate;

    private BigDecimal orderCost;

}
