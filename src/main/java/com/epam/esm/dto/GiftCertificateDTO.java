package com.epam.esm.dto;

import com.epam.esm.validation.PatchInfo;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.validation.groups.Default;
import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Relation(collectionRelation = "giftCertificates", itemRelation = "giftCertificate")
public class GiftCertificateDTO extends RepresentationModel<GiftCertificateDTO> implements Serializable {

    @Serial
    private static final long serialVersionUID = -310717954223597437L;

    private Long id;
    @NotNull(message = "Gift certificate name is mandatory")
    @Size(min = 3, max = 16, message = "Name cannot be shorter than 3 characters and longer than 16 characters", groups = {
        Default.class,
        PatchInfo.class})
    private String name;

    @NotNull(message = "Description is mandatory")
    @Size(min = 5, max = 150, message = "Description cannot be shorter than 5 characters and longer than 150 characters", groups = {
        Default.class,
        PatchInfo.class})
    private String description;

    @NotNull(message = "Price is mandatory")
    @Min(value = 1, message = "Minimum price is 1", groups = {Default.class, PatchInfo.class})
    private BigDecimal price;

    @NotNull(message = "Duration is mandatory")
    @Min(value = 1, message = "Minimum duration is 1", groups = {Default.class, PatchInfo.class})
    private Short duration;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime createDate;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime lastUpdateDate;

    @NotNull(message = "At least one tag should be specified")
    private Set<@Valid TagDTO> tags;
}
