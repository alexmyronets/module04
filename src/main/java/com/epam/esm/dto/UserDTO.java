package com.epam.esm.dto;

import com.epam.esm.domain.JwtProvider;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Relation(collectionRelation = "users", itemRelation = "user")
public class UserDTO extends RepresentationModel<UserDTO> {

    public static final long serialVersionUID = 8695676004080724102L;

    private Long id;

    private String username;

    private JwtProvider authProvider;

    private String providerId;

}
