package com.epam.esm.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class OauthRegisterRequest {

    @NotEmpty(message = "Username cannot be blank")
    @Size(min = 6, max = 16, message = "Username length should be from 6 to 16 characters")
    @Pattern(regexp = "^(?!.*[_.]{2})[^_.].*[^_.]$", message = "Username should consist of letters, numbers, periods, and underscores, without starting or ending with periods or underscores, and without having consecutive periods or underscores.")
    private String username;

    @NotEmpty(message = "id_token cannot be blank")
    private String idToken;
}
