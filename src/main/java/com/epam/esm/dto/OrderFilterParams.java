package com.epam.esm.dto;

import java.io.Serial;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class OrderFilterParams extends FilterParams implements Serializable {

    @Serial
    private static final long serialVersionUID = 6553107020506131781L;

    private Long userId;

}
