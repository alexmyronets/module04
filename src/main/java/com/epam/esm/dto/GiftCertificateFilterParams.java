package com.epam.esm.dto;

import com.epam.esm.domain.SortOrder;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class GiftCertificateFilterParams extends FilterParams implements Serializable {

    @Serial
    private static final long serialVersionUID = 6650179392535547921L;

    @Valid
    private List<@Valid TagDTO> tags = new ArrayList<>();

    @Size(min = 1, message = "Search text cannot be blank")
    private String searchText;
    private SortOrder sortByName;
    private SortOrder sortByDate;

    public GiftCertificateFilterParams(GiftCertificateFilterParams filterParams) {
        this(filterParams.getTags(),
             filterParams.getSearchText(),
             filterParams.getSortByName(),
             filterParams.getSortByDate());
    }
}
