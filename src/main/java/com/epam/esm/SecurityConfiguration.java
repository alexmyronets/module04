package com.epam.esm;

import com.epam.esm.domain.Role;
import com.epam.esm.service.authentication.GoogleOAuth2AuthenticationProviderService;
import com.epam.esm.service.authentication.NativeJwtAuthenticationProviderService;
import com.epam.esm.web.handler.JwtAccessDeniedHandler;
import com.epam.esm.web.handler.JwtAuthenticationEntryPoint;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.KeyPair;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.resource.web.authentication.BearerTokenAuthenticationFilter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableMethodSecurity
@EnableWebSecurity
@Configuration
public class SecurityConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, AuthenticationManager authenticationManager, JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint, JwtAccessDeniedHandler jwtAccessDeniedHandler)
        throws Exception {

        BearerTokenAuthenticationFilter bearerTokenAuthenticationFilter = new BearerTokenAuthenticationFilter(
            authenticationManager);
        bearerTokenAuthenticationFilter.setAuthenticationEntryPoint(jwtAuthenticationEntryPoint);

        return http.cors()
                   .and()
                   .csrf()
                   .disable()
                   .sessionManagement()
                   .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                   .and()
                   .authorizeHttpRequests()
                   .requestMatchers(HttpMethod.GET, "/**", "/")
                   .hasAnyRole(Role.USER.name(), Role.ADMIN.name())
                   .requestMatchers(HttpMethod.POST, "/api/users/{id}/orders")
                   .hasAnyRole(Role.USER.name(), Role.ADMIN.name())
                   .anyRequest()
                   .hasRole(Role.ADMIN.name())
                   .and()
                   .addFilter(bearerTokenAuthenticationFilter)
                   .exceptionHandling()
                   .authenticationEntryPoint(jwtAuthenticationEntryPoint)
                   .accessDeniedHandler(jwtAccessDeniedHandler)
                   .and()
                   .build();
    }

    @Bean
    @Order(1)
    public SecurityFilterChain loginFilterChain(HttpSecurity http, JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint)
        throws Exception {
        return http.cors()
                   .and()
                   .csrf()
                   .disable()
                   .sessionManagement()
                   .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                   .and()
                   .securityMatcher("/api/login")
                   .authorizeHttpRequests()
                   .anyRequest()
                   .authenticated()
                   .and()
                   .httpBasic()
                   .authenticationEntryPoint(jwtAuthenticationEntryPoint)
                   .and()
                   .build();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return web -> web.ignoring()
                         .requestMatchers(HttpMethod.GET, "/api/certificates/**")
                         .requestMatchers("/api/login/oauth/**",
                                          "/api/oauth2/callback/**",
                                          "/api/login/refresh-token/**")
                         .requestMatchers(HttpMethod.POST, "/api/users/**");
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("http://localhost:3000"));
        configuration.setAllowedMethods(List.of("*"));
        configuration.setAllowedHeaders(List.of("*"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }

    @Bean
    public AuthenticationManager authenticationManager(HttpSecurity http, PasswordEncoder passwordEncoder, UserDetailsService userDetailsService, GoogleOAuth2AuthenticationProviderService googleOAuth2AuthenticationProviderService, NativeJwtAuthenticationProviderService nativeJwtAuthenticationProviderService)
        throws Exception {
        return http.getSharedObject(AuthenticationManagerBuilder.class)
                   .userDetailsService(userDetailsService)
                   .passwordEncoder(passwordEncoder)
                   .and()
                   .authenticationProvider(googleOAuth2AuthenticationProviderService)
                   .authenticationProvider(nativeJwtAuthenticationProviderService)
                   .build();
    }


    @Bean
    public KeyPair rsaKeyPair() {
        return Keys.keyPairFor(SignatureAlgorithm.RS256);
    }
}
