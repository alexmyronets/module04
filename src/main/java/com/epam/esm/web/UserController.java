package com.epam.esm.web;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.epam.esm.domain.JwtProvider;
import com.epam.esm.domain.User;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.LoginRequest;
import com.epam.esm.dto.OauthRegisterRequest;
import com.epam.esm.dto.OrderFilterParams;
import com.epam.esm.dto.UserDTO;
import com.epam.esm.dto.UserRegistrationDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.hateoas.PagedModel;
import com.epam.esm.service.SecurityService;
import com.epam.esm.service.UserService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.common.collect.Iterables;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import java.net.URI;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class provides RESTful API endpoints for managing {@link User} resources. The endpoints support CRUD operations
 * and pagination of the user resources.
 *
 * @author Oleksandr Myronets
 */
@RestController
@RequestMapping("/api/users")
@Validated
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);

    private final UserService userService;

    private final SecurityService securityService;

    private final PasswordEncoder passwordEncoder;

    private final AuthenticationManager authenticationManager;

    /**
     * Constructs User Controller
     *
     * @param userService           {@link UserService}
     * @param securityService
     * @param passwordEncoder
     * @param authenticationManager
     */
    public UserController(UserService userService, SecurityService securityService, PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.securityService = securityService;
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
    }

    /**
     * Returns a list of {@link UserDTO} instances filtered by the provided {@link FilterParams}.
     *
     * @param filterParams the filter parameters to be applied on the user list
     * @return a {@link CollectionModel} of {@link UserDTO} instances
     */
    @GetMapping(produces = HAL_JSON_VALUE)
    public CollectionModel<UserDTO> getUsersList(@Valid FilterParams filterParams) {
        LOGGER.info("GET request for the list of users");
        List<UserDTO> users = userService.list(filterParams);
        return getUsersWithHATEOAS(filterParams, users);
    }

    /**
     * Performs read operation for User.
     *
     * @param id id of User to read.
     * @return {@link UserDTO} in JSON format.
     */
    @GetMapping(value = "/{id}", produces = HAL_JSON_VALUE)
    public UserDTO getUser(@PathVariable @Min(value = 1, message = "Id should be greater than 0") Long id) {
        LOGGER.info("GET request for user with id = {}", id);
        UserDTO userDTO = userService.read(id);
        addHATEOASLinks(userDTO);
        return userDTO;
    }

    /**
     * Registers a new user with the provided {@link LoginRequest} details.
     *
     * @param loginRequest the login request containing the user's credentials
     * @return a {@link ResponseEntity} containing the created user's JWT access token
     */
    @PostMapping
    public ResponseEntity<Map<String, String>> registerUser(@RequestBody @Validated LoginRequest loginRequest) {
        LOGGER.info("POST request to register a new user with username: {}", loginRequest.getUsername());
        UserRegistrationDTO user = new UserRegistrationDTO();

        user.setUsername(loginRequest.getUsername());
        user.setPassword(passwordEncoder.encode(loginRequest.getPassword()));
        user.setAuthProvider(JwtProvider.LOCAL);
        UserDTO createdUser = userService.create(user);
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
            createdUser.getUsername(),
            loginRequest.getPassword()));

        String jwt = securityService.generateJwtToken(authentication);
        LOGGER.info("User registered successfully. Sending JWT access token.");
        return new ResponseEntity<>(Map.of("access_token", jwt), HttpStatus.CREATED);
    }

    /**
     * Registers a new user using Google OAuth with the provided {@link OauthRegisterRequest} details.
     *
     * @param registerRequest the registration request containing the Google OAuth token
     */
    @PostMapping("/oauth/google")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerGoogleOauthUser(@RequestBody @Validated OauthRegisterRequest registerRequest) {
        LOGGER.info("POST request to register a new user using Google OAuth with username: {}",
                    registerRequest.getUsername());
        GoogleIdToken token = securityService.validateGoogleOauthToken(registerRequest.getIdToken());
        UserDTO user = new UserDTO();

        user.setUsername(registerRequest.getUsername());
        user.setAuthProvider(JwtProvider.GOOGLE);
        user.setProviderId(token.getPayload().getSubject());
        LOGGER.info("User registered successfully using Google OAuth.");
        userService.create(user);
    }

    private CollectionModel<UserDTO> getUsersWithHATEOAS(FilterParams filterParams, List<UserDTO> users) {
        LOGGER.info("Adding HATEOAS links for List of Users");
        URI baseURI = getBaseURI(filterParams);
        if (!users.isEmpty()) {
            PagingParamsDTO pagingParamsDTO = userService.getPagingParams(filterParams,
                                                                          Iterables.getLast(users).getId(),
                                                                          users.get(0).getId());
            users.forEach(UserController::addHATEOASLinks);
            LOGGER.info("Return List of Users with added HATEOAS links");
            return PagedModel.of(users, baseURI, filterParams.getPage(), pagingParamsDTO);
        }
        LOGGER.debug("List is empty, need to add only self link");
        LOGGER.info("Return empty List with self link");
        return PagedModel.empty(baseURI, filterParams.getPage());
    }

    private static void addHATEOASLinks(UserDTO user) {
        LOGGER.info("Adding HATEOAS links for each User");
        Link selfLink = linkTo(methodOn(UserController.class).getUser(user.getId())).withSelfRel();
        Link ordersLink = linkTo(methodOn(OrderController.class).getOrdersList(user.getId(),
                                                                               new OrderFilterParams())).withRel(
            "orders");
        user.add(selfLink);
        user.add(ordersLink);
    }

    private static URI getBaseURI(FilterParams filterParams) {
        LOGGER.info("Composing and returning base URI for request");
        return linkTo(methodOn(UserController.class).getUsersList(filterParams)).toUri();
    }
}
