package com.epam.esm.web;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.epam.esm.exception.InvalidTokenException;
import com.epam.esm.service.SecurityService;
import com.fasterxml.jackson.databind.JsonNode;
import io.jsonwebtoken.Claims;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api/login")
@CrossOrigin("http://localhost:3000")
public class AuthenticationController {

    private static final Logger LOGGER = LogManager.getLogger(AuthenticationController.class);

    private final SecurityService securityService;

    private final UserDetailsService userDetailsService;

    @Value("${spring.security.oauth2.client.registration.google.client-id}")
    private String clientId;

    @Value("${spring.security.oauth2.client.provider.google.authorization-uri}")
    private String authorizationUri;

    @Value("${spring.security.oauth2.client.registration.google.redirect-uri}")
    private String redirectUri;

    public AuthenticationController(SecurityService securityService, UserDetailsService userDetailsService) {
        this.securityService = securityService;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping
    public ResponseEntity<Map<String, String>> authenticateUser(Authentication authentication) {
        LOGGER.info("Generating Native JWT token for Basic authenticated user");
        String accessToken = securityService.generateJwtToken(authentication);
        LOGGER.info("Access token generated successfully");
        String refreshToken = securityService.generateJwtRefreshToken(authentication);
        LOGGER.info("Refresh token generated successfully");

        Map<String, String> tokens = new HashMap<>();
        tokens.put("access_token", accessToken);
        tokens.put("refresh_token", refreshToken);

        return ResponseEntity.ok(tokens);
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<Map<String, String>> refreshToken(@RequestBody JsonNode refreshTokenJson) {
        LOGGER.info("Refresh token request");
        String refreshToken = refreshTokenJson.get("refreshToken").asText();
        Claims claims = securityService.getClaims(refreshToken);
        if (!"refresh_token".equals(claims.get("type", String.class))) {
            LOGGER.error("Token is invalid: {}", refreshToken);
            throw new InvalidTokenException("Token is invalid: " + refreshToken);
        }
        String username = claims.getSubject();
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
                                                                                                     null,
                                                                                                     userDetails.getAuthorities());
        LOGGER.info("Generating new pair of tokens");
        return authenticateUser(authentication);
    }

    @GetMapping(value = "/oauth/google", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, String>> getGoogleAuthorizationUrl() {
        LOGGER.info("Generating Google Authorization URL");
        String state = UUID.randomUUID().toString();
        String url = UriComponentsBuilder.fromHttpUrl(authorizationUri)
                                         .queryParam("client_id", clientId)
                                         .queryParam("response_type", "code")
                                         .queryParam("scope", "openid")
                                         .queryParam("redirect_uri", redirectUri)
                                         .queryParam("state", state)
                                         .toUriString();
        LOGGER.info("Google Authorization URL generated");
        return ResponseEntity.ok(Map.of("Authorization URL", url));
    }

    @GetMapping(value = "/oauth/google/implicit", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, String>> getGoogleImplicitAuthorizationUrl() {
        LOGGER.info("Generating Google Implicit Authorization URL");
        String nonce = generateNonce();
        String url = UriComponentsBuilder.fromHttpUrl(authorizationUri)
                                         .queryParam("client_id", clientId)
                                         .queryParam("response_type", "token id_token")
                                         .queryParam("scope", "openid")
                                         .queryParam("redirect_uri", redirectUri)
                                         .queryParam("nonce", nonce)
                                         .toUriString();
        LOGGER.info("Google Implicit Authorization URL generated");
        return ResponseEntity.ok(Map.of("Authorization URL", url));
    }

    private String generateNonce() {
        LOGGER.debug("Generating nonce for Google Implicit Authorization URL");
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }

}
