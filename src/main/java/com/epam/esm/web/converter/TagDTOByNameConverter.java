package com.epam.esm.web.converter;

import com.epam.esm.dto.TagDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TagDTOByNameConverter implements Converter<String, TagDTO> {


    @Override
    public TagDTO convert(String name) {
        return new TagDTO(null, name);
    }
}
