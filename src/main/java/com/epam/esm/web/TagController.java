package com.epam.esm.web;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.epam.esm.domain.Tag;
import com.epam.esm.dto.FilterParams;
import com.epam.esm.dto.TagDTO;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.hateoas.PagedModel;
import com.epam.esm.service.TagService;
import com.google.common.collect.Iterables;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import java.net.URI;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class consists of methods for managing requests for CRD operation of {@link Tag}.
 *
 * @author Oleksandr Myronets
 */
@RestController
@RequestMapping(path = "/api/tags")
@Validated
public class TagController {

    private static final Logger LOGGER = LogManager.getLogger(TagController.class);

    private final TagService tagService;

    /**
     * Constructs tag Controller
     *
     * @param tagService {@link TagService}
     */
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    /**
     * Performs create operation for Tag.
     *
     * @param tagDTO {@link TagDTO} to create.
     * @return created {@link TagDTO} in JSON format.
     */
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = HAL_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public TagDTO postTag(@Valid @RequestBody TagDTO tagDTO) {
        LOGGER.info("POST request for tag: {}", tagDTO);
        TagDTO createdTag = tagService.create(tagDTO);
        addHATEOASLinks(createdTag);
        return createdTag;
    }

    /**
     * Performs list operation for Tag.
     *
     * @param filterParams parameters of filter
     * @return List of {@link TagDTO} in JSON format.
     */
    @GetMapping(produces = HAL_JSON_VALUE)
    public CollectionModel<TagDTO> getTagsList(@Valid FilterParams filterParams) {
        LOGGER.info("GET request for the list of all tags");
        List<TagDTO> tags = tagService.list(filterParams);
        return getTagsWithHATEOAS(filterParams, tags);
    }

    /**
     * Performs read operation for Tag.
     *
     * @param id of Tag to read.
     * @return {@link TagDTO} in JSON format.
     */
    @GetMapping(value = "/{id}", produces = HAL_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public TagDTO getTag(@PathVariable @Min(value = 1, message = "Id should be greater than 0") Long id) {
        LOGGER.info("GET request for tag with id = {}", id);
        TagDTO tag = tagService.read(id);
        addHATEOASLinks(tag);
        return tag;
    }

    /**
     * Performs search for the most widely used Tag among the orders of a user with the highest cost of all orders.
     *
     * @return {@link TagDTO} in JSON format.
     */
    @GetMapping(value = "/most_used_tag_by_most_spent_user", produces = HAL_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public TagDTO getMostUsedTagByMostSpendUser() {
        LOGGER.info("GET request for the most widely used tag of a user with the highest cost of all orders");
        TagDTO tag = tagService.readMostUsedTagByMostSpendUser();
        addHATEOASLinks(tag);
        return tag;
    }

    /**
     * Performs delete operation for Tag.
     *
     * @param id id of Tag to delete.
     */
    @DeleteMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTag(@PathVariable Long id) {
        LOGGER.info("DELETE request for tag with id = {}", id);
        tagService.delete(id);
    }

    private CollectionModel<TagDTO> getTagsWithHATEOAS(FilterParams filterParams, List<TagDTO> tags) {
        LOGGER.info("Adding HATEOAS links for List of Tags");
        URI baseURI = getBaseURI(filterParams);

        if (!tags.isEmpty()) {
            PagingParamsDTO pagingParamsDTO = tagService.getPagingParams(filterParams,
                                                                         Iterables.getLast(tags).getId(),
                                                                         tags.get(0).getId());
            tags.forEach(TagController::addHATEOASLinks);
            LOGGER.info("Return List of Tags with added HATEOAS links");
            return PagedModel.of(tags, baseURI, filterParams.getPage(), pagingParamsDTO);
        }
        LOGGER.debug("List is empty, need to add only self link");
        LOGGER.info("Return empty List with self link");
        return PagedModel.empty(baseURI, filterParams.getPage());
    }

    private static void addHATEOASLinks(TagDTO tag) {
        LOGGER.info("Adding HATEOAS links for each Tag");
        Link selfLink = linkTo(methodOn(TagController.class).getTag(tag.getId())).withSelfRel();
        tag.add(selfLink);
    }

    private static URI getBaseURI(FilterParams filterParams) {
        LOGGER.info("Composing and returning base URI for request");
        return linkTo(methodOn(TagController.class).getTagsList(filterParams)).toUri();
    }
}
