package com.epam.esm.web;

import static org.springframework.hateoas.MediaTypes.HAL_JSON_VALUE;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.epam.esm.dto.GiftCertificateDTO;
import com.epam.esm.dto.GiftCertificateFilterParams;
import com.epam.esm.dto.TagDTO;
import com.epam.esm.hateoas.PagedModel;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.validation.PatchInfo;
import com.google.common.collect.Iterables;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import java.net.URI;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class consists of methods for managing requests for CRUD operation of
 * {@link com.epam.esm.dto.GiftCertificateDTO}.
 *
 * @author Oleksandr Myronets.
 */
@RestController
@RequestMapping(path = "/api/certificates")
@Validated
@CrossOrigin("http://localhost:3000")
public class GiftCertificateController {

    private static final Logger LOGGER = LogManager.getLogger(GiftCertificateController.class);

    private final GiftCertificateService giftCertificateService;

    /**
     * Constructs Gift Certificate Controller
     *
     * @param giftCertificateService {@link GiftCertificateService}
     */
    public GiftCertificateController(GiftCertificateService giftCertificateService) {
        this.giftCertificateService = giftCertificateService;
    }

    /**
     * Performs create operation for Gift Certificate.
     *
     * @param giftCert {@link GiftCertificateDTO} to create.
     * @return created {@link GiftCertificateDTO} in JSON format.
     */
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = HAL_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public GiftCertificateDTO postGiftCertificate(@Valid @RequestBody GiftCertificateDTO giftCert) {
        LOGGER.info("POST request for Gift Certificate: {}", giftCert);
        GiftCertificateDTO createdGiftCert = giftCertificateService.create(giftCert);
        addHATEOASLinks(createdGiftCert);
        return createdGiftCert;
    }

    /**
     * Performs list operation for Gift Certificate.
     *
     * @param filterParams parameters of filter
     * @return List of {@link GiftCertificateDTO} in JSON format.
     */
    @GetMapping(produces = HAL_JSON_VALUE)
    public CollectionModel<GiftCertificateDTO> getGitCertificatesList(@Valid GiftCertificateFilterParams filterParams) {
        LOGGER.info("GET request for the list of gift certificates by parameters: {}", filterParams);
        List<GiftCertificateDTO> certificates = giftCertificateService.list(filterParams);
        return getGiftCertificatesWithHATEOAS(filterParams, certificates);
    }

    /**
     * Performs read operation for Gift Certificate.
     *
     * @param id of Gift Certificate to read.
     * @return {@link GiftCertificateDTO} in JSON format.
     */
    @GetMapping(value = "/{id}", produces = HAL_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GiftCertificateDTO getGiftCertificate(
        @PathVariable @Min(value = 1, message = "Id should be greater than 0") Long id) {
        LOGGER.info("GET request for certificate with id = {}", id);
        GiftCertificateDTO giftCert = giftCertificateService.read(id);
        addHATEOASLinks(giftCert);
        return giftCert;
    }

    /**
     * Performs update operation for Gift Certificate.
     *
     * @param id       of Gift Certificate to update.
     * @param giftCert {@link GiftCertificateDTO} with patch details.
     * @return patched {@link GiftCertificateDTO} in JSON format.
     */
    @PatchMapping(path = "/{id}", consumes = APPLICATION_JSON_VALUE, produces = HAL_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GiftCertificateDTO patchGiftCertificate(@PathVariable Long id,
                                                   @Validated(PatchInfo.class) @RequestBody GiftCertificateDTO giftCert) {
        LOGGER.info("PATCH request for certificate with id = {}, patch details: {}", id, giftCert);
        GiftCertificateDTO patchedGiftCert = giftCertificateService.update(giftCert, id);
        addHATEOASLinks(patchedGiftCert);
        return patchedGiftCert;
    }

    /**
     * Performs delete operation for Gift Certificate.
     *
     * @param id of Gift Certificate to delete.
     */
    @DeleteMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGiftCertificate(@PathVariable Long id) {
        LOGGER.info("DELETE request for certificate with id = {}", id);
        giftCertificateService.delete(id);
    }

    private CollectionModel<GiftCertificateDTO> getGiftCertificatesWithHATEOAS(GiftCertificateFilterParams filterParams,
                                                                               List<GiftCertificateDTO> certificates) {
        LOGGER.info("Adding HATEOAS links for List of Gift Certificates and their Tags");
        URI baseURI = getBaseURI(filterParams);
        if (!certificates.isEmpty()) {
            var pagingParamsDTO = giftCertificateService.getPagingParams(filterParams,
                                                                         Iterables.getLast(certificates).getId(),
                                                                         certificates.get(0).getId());
            certificates.forEach(GiftCertificateController::addHATEOASLinks);

            LOGGER.info("Return List of Gift Certificates with added HATEOAS links");
            return PagedModel.of(certificates, baseURI, filterParams.getPage(), pagingParamsDTO);
        }
        LOGGER.debug("List is empty, need to add only self link");
        LOGGER.info("Return empty List with self link");
        return PagedModel.empty(baseURI, filterParams.getPage());
    }

    private static URI getBaseURI(GiftCertificateFilterParams filterParams) {
        LOGGER.info("Composing base URI for request");
        var uriComponentsBuilder = linkTo(methodOn(GiftCertificateController.class).getGitCertificatesList(filterParams)).toUriComponentsBuilder();
        LOGGER.info("Returning base URI for request");
        return uriComponentsBuilder.queryParams(getQueryParams(filterParams)).build().toUri();
    }

    private static MultiValueMap<String, String> getQueryParams(GiftCertificateFilterParams filterParams) {
        LOGGER.info("Getting request parameters");
        LOGGER.trace("Creating empty Map of request params");
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();

        if (!filterParams.getTags().isEmpty()) {
            LOGGER.debug("Adding tags to request params Map: {}", filterParams.getTags());
            queryParams.addAll("tags", filterParams.getTags().stream().map(TagDTO::getName).toList());
        }

        if (filterParams.getSearchText() != null) {
            LOGGER.debug("Adding search text to request params Map: {}", filterParams.getSearchText());
            queryParams.add("searchText", filterParams.getSearchText());
        }

        if (filterParams.getSortByName() != null) {
            LOGGER.debug("Adding name sort order to request params Map: {}", () -> filterParams.getSortByName().name());
            queryParams.add("sortByName", filterParams.getSortByName().name());
        }

        if (filterParams.getSortByDate() != null) {
            LOGGER.debug("Adding date sort order to request params Map: {}", () -> filterParams.getSortByDate().name());
            queryParams.add("sortByDate", filterParams.getSortByDate().name());
        }
        LOGGER.info("Returning Map of request parameters");
        return queryParams;
    }

    private static void addHATEOASLinks(GiftCertificateDTO giftCert) {
        LOGGER.info("Adding HATEOAS links for each Gift Certificate and tags");
        Link selfLink = linkTo(methodOn(GiftCertificateController.class).getGiftCertificate(giftCert.getId())).withSelfRel();
        giftCert.add(selfLink);
        for (TagDTO tag : giftCert.getTags()) {
            Link tagLink = linkTo(methodOn(TagController.class).getTag(tag.getId())).withSelfRel();
            tag.add(tagLink);
        }
    }
}
