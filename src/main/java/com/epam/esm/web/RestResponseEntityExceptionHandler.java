package com.epam.esm.web;

import com.epam.esm.exception.GiftCertificateInUseException;
import com.epam.esm.exception.InvalidTokenException;
import com.epam.esm.exception.NoSuchGiftCertificateException;
import com.epam.esm.exception.NoSuchOrderException;
import com.epam.esm.exception.NoSuchTagException;
import com.epam.esm.exception.NoSuchUserException;
import com.epam.esm.exception.TagAlreadyExistsException;
import com.epam.esm.exception.TagIsInUseException;
import com.epam.esm.exception.UserAlreadyExistsException;
import jakarta.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2AuthorizationException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger(RestResponseEntityExceptionHandler.class);

    private static final Map<String, String> errorMessages = Map.of("page.direction",
                                                                    "Incorrect page.direction value. Allowed values: BACK, FORWARD",
                                                                    "sortByName",
                                                                    "Incorrect sortByName value. Allowed values are: ASC, DESC",
                                                                    "sortByDate",
                                                                    "Incorrect sortByDate value. Allowed values are: ASC, DESC");

    private static final String ERROR = "Error";

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponse> handleHttpMessageNotReadable(HttpMessageNotReadableException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse("Failed to read request", "40001"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BindException.class)
    public ResponseEntity<ErrorResponse> handleBindException(BindException e) {
        LOGGER.error(ERROR, e);
        Map<String, String> errors = new HashMap<>();
        ValidationErrorResponse validationErrorResponse = new ValidationErrorResponse();
        ResponseEntity<ErrorResponse> responseEntity = new ResponseEntity<>(validationErrorResponse,
                                                                            HttpStatus.BAD_REQUEST);

        validationErrorResponse.setErrorMessage("Validation error");
        validationErrorResponse.setErrorCode("40002");

        e.getBindingResult()
         .getAllErrors()
         .forEach(error -> {
             String fieldName = ((FieldError) error).getField();
             String errorMessage;
             if (errorMessages.containsKey(fieldName)) {
                 errorMessage = errorMessages.get(fieldName);
             } else {
                 errorMessage = error.getDefaultMessage();
             }
             errors.put(fieldName, errorMessage);
         });
        validationErrorResponse.setValidationErrors(errors);
        return responseEntity;
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> handleBindException(ConstraintViolationException e) {
        LOGGER.error(ERROR, e);
        Map<String, String> errors = new HashMap<>();
        ValidationErrorResponse validationErrorResponse = new ValidationErrorResponse();
        ResponseEntity<ErrorResponse> responseEntity = new ResponseEntity<>(validationErrorResponse,
                                                                            HttpStatus.BAD_REQUEST);

        validationErrorResponse.setErrorMessage("Validation error");
        validationErrorResponse.setErrorCode("40003");

        e.getConstraintViolations()
         .forEach(constraintViolation -> {
             String errorMessage = constraintViolation.getMessage();
             errors.put("id", errorMessage);
         });
        validationErrorResponse.setValidationErrors(errors);
        return responseEntity;
    }

    @ExceptionHandler(InvalidTokenException.class)
    public ResponseEntity<ErrorResponse> handleInvalidTokenException(InvalidTokenException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse("Invalid token", "40004"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(OAuth2AuthorizationException.class)
    public ResponseEntity<ErrorResponse> handleOAuth2AuthorizationException(OAuth2AuthorizationException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse("Authorization problems", "40005"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse("Unauthorized", "40101"), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedExceptionException(AccessDeniedException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse("Forbidden", "40301"), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = NoHandlerFoundException.class)
    public ResponseEntity<ErrorResponse> handleNoHandlerFoundException(NoHandlerFoundException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse("Requested resource not found: " + e.getMessage(), "40400"),
                                    HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = NoSuchGiftCertificateException.class)
    public ResponseEntity<ErrorResponse> handleNoSuchGiftCertificateException(NoSuchGiftCertificateException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40401"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = NoSuchTagException.class)
    public ResponseEntity<ErrorResponse> handleNoSuchTagException(NoSuchTagException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40402"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = NoSuchUserException.class)
    public ResponseEntity<ErrorResponse> handleNoSuchTagException(NoSuchUserException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40403"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = NoSuchOrderException.class)
    public ResponseEntity<ErrorResponse> handleNoSuchTagException(NoSuchOrderException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40404"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = TagIsInUseException.class)
    public ResponseEntity<ErrorResponse> handleTagIsInUseException(TagIsInUseException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40901"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = TagAlreadyExistsException.class)
    public ResponseEntity<ErrorResponse> handleTagAlreadyExistsException(TagAlreadyExistsException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40902"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = UserAlreadyExistsException.class)
    public ResponseEntity<ErrorResponse> handleTagAlreadyExistsException(UserAlreadyExistsException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40903"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = GiftCertificateInUseException.class)
    public ResponseEntity<ErrorResponse> handleTagIsInUseException(GiftCertificateInUseException e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40904"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        LOGGER.error(ERROR, e);
        return new ResponseEntity<>(new ErrorResponse("Internal server error", "50001"),
                                    HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Data
    @RequiredArgsConstructor
    @AllArgsConstructor
    private static class ErrorResponse {

        private String errorMessage;
        private String errorCode;

    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    private static class ValidationErrorResponse extends ErrorResponse {

        Map<String, String> validationErrors;
    }
}
