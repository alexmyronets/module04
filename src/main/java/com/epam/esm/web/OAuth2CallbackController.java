package com.epam.esm.web;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/oauth2/callback")
public class OAuth2CallbackController {

    private static final Logger LOGGER = LogManager.getLogger(OAuth2CallbackController.class);

    private final ClientRegistrationRepository clientRegistrationRepository;

    @Value("${spring.security.oauth2.client.registration.google.redirect-uri}")
    private String redirectUri;

    public OAuth2CallbackController(ClientRegistrationRepository clientRegistrationRepository) {
        this.clientRegistrationRepository = clientRegistrationRepository;
    }

    @GetMapping("/google")
    public ResponseEntity<Map<String, Object>> handleGoogleCallback(@RequestParam Map<String, String> params)
        throws GeneralSecurityException, IOException {

        LOGGER.info("Handling Google OAuth2 callback");

        if (params.containsKey("code")) {
            String code = params.get("code");
            LOGGER.debug("Received OAuth2 code: {}", code);
            ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId("google");

            GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(GoogleNetHttpTransport.newTrustedTransport(),
                                                                                        new GsonFactory(),
                                                                                        clientRegistration.getClientId(),
                                                                                        clientRegistration.getClientSecret(),
                                                                                        code,
                                                                                        redirectUri).execute();
            LOGGER.info("Google Token Response received");
            return ResponseEntity.ok(Map.of("id_token", tokenResponse.getIdToken()));
        }
        LOGGER.warn("OAuth2 code not found in callback parameters");
        return ResponseEntity.ok(Map.of());
    }
}
