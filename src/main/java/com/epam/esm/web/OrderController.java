package com.epam.esm.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.epam.esm.domain.Order;
import com.epam.esm.dto.OrderDTO;
import com.epam.esm.dto.OrderFilterParams;
import com.epam.esm.dto.paging.PagingParamsDTO;
import com.epam.esm.hateoas.PagedModel;
import com.epam.esm.service.OrderService;
import com.google.common.collect.Iterables;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import java.net.URI;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class consists of methods for managing requests for CR operation of {@link Order}.
 *
 * @author Oleksandr Myronets
 */
@RestController
@RequestMapping("api/users/{userId}/orders")
@Validated
public class OrderController {

    private static final Logger LOGGER = LogManager.getLogger(OrderController.class);

    private final OrderService orderService;

    /**
     * Constructs Order Controller
     *
     * @param orderService {@link OrderService}
     */
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * Performs list operation for Order.
     *
     * @param userId       id of the user
     * @param filterParams parameters of filter
     * @return List of {@link OrderDTO} in JSON format.
     */
    @GetMapping(produces = MediaTypes.HAL_JSON_VALUE)
    public CollectionModel<OrderDTO> getOrdersList(
        @PathVariable @Min(value = 1, message = "Id should be greater than 0") Long userId,
        OrderFilterParams filterParams) {
        filterParams.setUserId(userId);
        LOGGER.info("GET request for the list of orders for user with id: {}", userId);
        List<OrderDTO> orders = orderService.list(filterParams);
        return getOrdersWithHATEOAS(userId, filterParams, orders);
    }

    /**
     * Performs read operation for User.
     *
     * @param userId  id of the user
     * @param orderId id of the order
     * @return {@link OrderDTO} in JSON format.
     */
    @GetMapping(value = "/{orderId}", produces = MediaTypes.HAL_JSON_VALUE)
    public OrderDTO getOrder(@PathVariable Long userId, @PathVariable Long orderId) {
        LOGGER.info("GET request for order with id = {}, by user id: {}", orderId, userId);
        OrderDTO orderDTO = orderService.read(userId, orderId);
        addHATEOASLinks(orderDTO);
        return orderDTO;
    }

    /**
     * Performs create operation for Order.
     *
     * @param userId      id of the user
     * @param orderToSave {@link OrderDTO} to create
     * @return created {@link OrderDTO}
     */
    @PostMapping(produces = MediaTypes.HAL_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDTO postOrder(@PathVariable Long userId, @Valid @RequestBody OrderDTO orderToSave) {
        orderToSave.setUserId(userId);
        LOGGER.info("POST request for order: {}", orderToSave);
        OrderDTO savedOrder = orderService.create(orderToSave);
        addHATEOASLinks(savedOrder);
        return savedOrder;
    }

    private CollectionModel<OrderDTO> getOrdersWithHATEOAS(Long userId, OrderFilterParams filterParams,
                                                           List<OrderDTO> orders) {
        LOGGER.info("Adding HATEOAS links for List of Orders");
        URI baseURI = getBaseURI(userId, filterParams);
        if (!orders.isEmpty()) {
            PagingParamsDTO pagingParamsDTO = orderService.getPagingParams(filterParams,
                                                                           Iterables.getLast(orders).getId(),
                                                                           orders.get(0).getId());
            orders.forEach(OrderController::addHATEOASLinks);
            LOGGER.info("Return List of Orders with added HATEOAS links");
            return PagedModel.of(orders, baseURI, filterParams.getPage(), pagingParamsDTO);
        }
        LOGGER.debug("List is empty, need to add only self link");
        LOGGER.info("Return empty List with self link");
        return PagedModel.empty(baseURI, filterParams.getPage());
    }

    private static void addHATEOASLinks(OrderDTO order) {
        LOGGER.info("Adding HATEOAS links for each Order");
        Link selfLink = linkTo(methodOn(OrderController.class).getOrder(order.getUserId(),
                                                                        order.getId())).withSelfRel();
        order.add(selfLink);
    }

    private static URI getBaseURI(Long userId, OrderFilterParams filterParams) {
        LOGGER.info("Composing and returning base URI for request");
        return linkTo(methodOn(OrderController.class).getOrdersList(userId, filterParams)).toUri();
    }
}