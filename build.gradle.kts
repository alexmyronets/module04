import java.nio.file.Files
import java.nio.file.Paths

plugins {
    java
    jacoco
    id("org.springframework.boot") version "3.0.1"
    id("io.spring.dependency-management") version "1.1.0"
    id("org.sonarqube") version "4.0.0.2929"
}

jacoco {
    toolVersion = "0.8.8" // Set the JaCoCo version
}

sonarqube {
    properties {
        property("sonar.projectKey", "alexmyronets_module04_AYfUpaTkmoiYzMS9sh7o")
        property("sonar.projectName", "module04")
        property("sonar.token", "squ_f043c5ef631340b09c4d946bee2b230b7a515e9a")
    }
}
val buildNumber = System.getenv("BUILD_NUMBER") ?: "local"

group = "com.epam.esm"
version = "4.0.0.b$buildNumber"

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
    implementation {
        exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.zaxxer:HikariCP")
    implementation("org.postgresql:postgresql")
    implementation("com.google.code.findbugs:jsr305:3.0.2")
    implementation("org.apache.logging.log4j:log4j-core")
    implementation("org.apache.logging.log4j:log4j-api")
    implementation("org.modelmapper:modelmapper:3.1.1")
    implementation("com.h2database:h2")
    implementation("org.springframework.boot:spring-boot-starter-hateoas")
    implementation ("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation ("org.springframework.boot:spring-boot-starter-security")
    implementation ("org.springframework.boot:spring-boot-starter-oauth2-client")
    implementation ("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    implementation("com.google.guava:guava:31.1-jre")
    implementation("com.google.api-client:google-api-client:2.2.0")
    implementation("com.google.oauth-client:google-oauth-client:1.34.1")
    implementation ("io.jsonwebtoken:jjwt-api:0.11.5")
    runtimeOnly("io.jsonwebtoken:jjwt-impl:0.11.5")
    runtimeOnly("io.jsonwebtoken:jjwt-jackson:0.11.5")
    implementation ("com.auth0:java-jwt:3.18.2")
    compileOnly("org.projectlombok:lombok")
    annotationProcessor("org.projectlombok:lombok")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<JavaCompile>().configureEach {
    doFirst {
        val propertiesFile = file("src/main/resources/application.properties")
        val lines = Files.readAllLines(Paths.get(propertiesFile.path)).toMutableList()
        val buildNumberLine = "app.buildNumber=$buildNumber"

        val existingLineIndex = lines.indexOfFirst { it.startsWith("app.buildNumber=") }
        if (existingLineIndex != -1) {
            // Update the existing line
            lines[existingLineIndex] = buildNumberLine
        } else {
            // Add a new line if it doesn't exist
            lines.add(buildNumberLine)
        }

        Files.write(Paths.get(propertiesFile.path), lines)
    }
}

tasks.bootJar {
    this.archiveFileName.set("${archiveBaseName.get()}.${archiveExtension.get()}")
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        csv.required.set(false)
        html.outputLocation.set(layout.buildDirectory.dir("jacocoHtml"))
    }
}

tasks.test {
    finalizedBy("jacocoTestReport")
}








